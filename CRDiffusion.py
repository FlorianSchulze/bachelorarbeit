"""
Create the final plots for the project.
Load the templates, calculate source distribution and interpolation.
"""
import matplotlib.pyplot as plt
import numpy as np
from scipy import interpolate, integrate
import glob
import h5py
import time

np.set_printoptions(threshold=20, linewidth=150)

"""Some global parameters"""
mass_p = 938.272088  # <Mass of the Proton in MeV
light_speed_si = 299792458  # <c in m/s

"""Parameters in normal units"""
p0 = mass_p  # <p0 in MeV/c
b0 = 1.96879  # <b0 in (Mev/c)/Myr
kappa0 = 0.035  # <kappa0 in kpc^2/Myr
# q0 = 1.42993e49 * p0  # <q0 in dimensionless
q0 = 1.42993e49  # <q0 in 1/(MeV/c)
delta_analytic = 0.63  # <delta in kappa in the analytic solution
# delta_analytic = 1.0  # <delta in kappa in the analytic solution
n_analytic = -2.0  # <n in b in the analytic solution
gamma = 2.2  # <gamma in q

"""Variables for making dimensionless"""
tau = p0 / b0  # <tau in Myr, make t dimensionless
lamb = np.sqrt(kappa0 * p0 / b0)  # <lambda in kpc, make r dimensionless

"""dimensions of the Galaxy"""
R = 10.  # <R in kpc
L = 4.  # <L in kpc
h = 0.1  # <h in kpc

"""parameter for energy loss"""
beta_0 = 0.01  # <beta_0 in ionisation loss

"""parameter for ionisation losses"""
n_ion = 1.0  # <density in 1/cm^3, the ionisation rate is independent of density!
f_ion = 1.82e-7  # <factor ionisation losses in eV/s

"""parameter for ionisation rate"""
nu1 = 5. / 3.  # secondary ionisation
nu2 = 1.17  # helium abundance
nu3 = 1.43  # heavy CR and CR electrons


def intensity_phan(ekin):
    """intensity by phan et al in (m^2 s sr MeV)^-1"""
    return 1.882e1 * pow(ekin, 0.129) * pow(1 + ekin / 624.5, -2.829)


def plot_energy_loss():
    """Plot the energy loss b(p)"""
    file_list = glob.glob('CRDiff_energy_loss*.csv')
    if not file_list:
        return -1
    plt.figure()
    for file in file_list:
        log_p, dpdt = np.loadtxt(file, unpack=True, skiprows=2, delimiter=';')
        p = np.exp(log_p) * p0
        dpdt = dpdt * np.exp(log_p) * b0
        dEdt = dpdt / np.sqrt(1.0 + (mass_p / p) ** 2)
        ekin = np.sqrt(p ** 2 + mass_p ** 2) - mass_p
        # print('Ekin: ', ekin)
        index_ekin = np.argmin(np.abs(ekin - 1.0))
        # print('-dE/dt(Ekin=%fMeV) = %f MeV/Myr' % (ekin[index_ekin], -dEdt[index_ekin]))
        label = 'composed'
        # plt.plot(ekin, -dEdt, label=label)
        plt.plot(p, -dpdt, label=label)

        """Fit power law"""

        def energy_loss_analytic(par_p, par_n):
            return b0 * np.power(par_p / p0, par_n)

        n_fit = n_analytic
        # from scipy import optimize
        # out = optimize.curve_fit(energy_loss_analytic, p, dpdt, [n_fit])
        # print(out)
        # n_analytic = out[0][0]
        energy_loss_analytic_val = energy_loss_analytic(p, n_fit)
        # energy_loss_analytic_val = energy_loss_analytic_val / np.sqrt(1.0 + (mass_p / p) ** 2)

        label = r'$b(p) = b_0 (\frac{p}{p0})^n, n = %.2f$' % n_analytic
        # plt.plot(ekin, energy_loss_analytic_val, label=label)
        plt.plot(p, energy_loss_analytic_val, label=label)
    # plt.xlabel('$E_{kin}$ [MeV]')
    plt.xlabel('$p$ [MeV/c]')
    # plt.ylabel('$-\\frac{dE}{dt}$ [$\\frac{MeV}{Myr}$]')
    plt.ylabel('$-\\frac{dp}{dt}$ [$\\frac{MeV/c}{Myr}$]')
    # plt.title('Energy loss rate')
    plt.legend()
    plt.xscale('log')
    plt.yscale('log')
    plt.grid()
    plt.legend()
    plt.tight_layout()
    plt.savefig('CRDiffEnergyLoss.pdf')
    return 0


def plot_diffusion_coefficient():
    """Plot the diffusion coefficient kappa(p)"""
    file_list = glob.glob('CRDiff_diffusion_coefficient*.csv')
    if not file_list:
        return -1
    plt.figure()
    for file in file_list:
        log_p, kappa = np.loadtxt(file, unpack=True, skiprows=2, delimiter=';')
        p = np.exp(log_p) * p0
        kappa = kappa * kappa0
        ekin = np.sqrt(p ** 2 + mass_p ** 2) - mass_p
        index_ekin = np.argmin(np.abs(ekin - 1.0))
        # print('kappa(Ekin=%fMeV) = %f kpc^2/Myr' % (ekin[index_ekin], kappa[index_ekin]))
        label = 'composed'
        # plt.plot(ekin, kappa, label=label)
        plt.plot(p, kappa, label=label)

        """Fit power law"""

        def kappa_analytic(par_p, par_delta):
            return kappa0 * np.power(par_p / p0, par_delta)

        delta_fit = delta_analytic
        # from scipy import optimize
        # out = optimize.curve_fit(kappa_analytic, p, kappa, [delta_fit])
        # print(out)
        # delta_fit = out[0][0]
        kappa_analytic_val = kappa_analytic(p, delta_fit)
        label = r'$\kappa(p) = \kappa_0 (\frac{p}{p_0})^{\delta}, \delta = %.2f$' % delta_fit
        # plt.plot(ekin, kappa_analytic_val, label=label)
        plt.plot(p, kappa_analytic_val, label=label)
    # plt.xlabel('$E_{kin}$ [MeV]')
    plt.xlabel('$p$ [MeV/c]')
    plt.ylabel('$\\kappa$ [$\\frac{kpc^2}{Myr}$]')
    # plt.title('Diffusion coefficient')
    plt.legend()
    plt.xscale('log')
    plt.yscale('log')
    plt.grid()
    plt.legend()
    plt.tight_layout()
    plt.savefig('CRDiffDiffusionCoefficient.pdf')
    return 0


def plot_characteristic_time_scales():
    """Plot the characteristic time scales"""
    file_list_energy_loss = glob.glob('CRDiff_energy_loss*.csv')
    file_list_diffusion = glob.glob('CRDiff_diffusion_coefficient*.csv')
    if not file_list_energy_loss and not file_list_diffusion:
        return -1
    plt.figure()
    for file in file_list_energy_loss:
        log_p, dpdt = np.loadtxt(file, unpack=True, skiprows=2, delimiter=';')
        p = np.exp(log_p) * p0
        ekin = np.sqrt(p ** 2 + mass_p ** 2) - mass_p
        dpdt = dpdt * np.exp(log_p) * b0
        dpdt_analytic = b0 * np.power(p / p0, n_analytic)
        t = p / abs(dpdt)
        t_analytic = p / abs(dpdt_analytic)
        # obj = plt.plot(ekin, t, label='Energy loss time')
        obj = plt.plot(p, t, label='energy loss time')
        # plt.plot(ekin, t_analytic, '--', color=obj[-1].get_color())
        plt.plot(p, t_analytic, '--', color=obj[-1].get_color())
    for file in file_list_diffusion:
        log_p, kappa = np.loadtxt(file, unpack=True, skiprows=2, delimiter=';')
        p = np.exp(log_p) * p0
        ekin = np.sqrt(p ** 2 + mass_p ** 2) - mass_p
        kappa = kappa * kappa0
        kappa_analytic = kappa0 * np.power(p / p0, delta_analytic)
        """loss out of disk"""
        t = h ** 2 / kappa
        t_analytic = h ** 2 / kappa_analytic
        # obj = plt.plot(ekin, t, label='Diffusion out of disk')
        obj = plt.plot(p, t, label='diffusion out of disk')
        # plt.plot(ekin, t_analytic, '--', color=obj[-1].get_color())
        plt.plot(p, t_analytic, '--', color=obj[-1].get_color())
        """loss out of halo"""
        t = L ** 2 / kappa
        t_analytic = L ** 2 / kappa_analytic
        # obj = plt.plot(ekin, t, label='Diffusion out of halo')
        obj = plt.plot(p, t, label='diffusion out of halo')
        # plt.plot(ekin, t_analytic, '--', color=obj[-1].get_color())
        plt.plot(p, t_analytic, '--', color=obj[-1].get_color())
    plt.plot(0, 0, '--', color='grey', label='power law')
    # plt.xlabel('$E_{kin}$ [MeV]')
    plt.xlabel('$p$ [MeV/c]')
    plt.ylabel(r'$\tau_{loss}$ [Myr]')
    # plt.title('Characteristic time scales')
    plt.legend()
    plt.xscale('log')
    plt.yscale('log')
    plt.grid()
    plt.legend()
    plt.tight_layout()
    plt.savefig('CRDiffusionCharacteristicTimes.pdf')
    return 0


def plot_error_one_dimensional_diffusion(variable='z'):
    """Plot the error of the one dimensional Diffusion equation in 'variable' ('z' or 'r')."""
    if variable == 'z':
        file_list = glob.glob('OneDimDiffError_steps_*.csv')
    else:
        file_list = glob.glob('OneDimDiffRError_steps_*.csv')
    if not file_list:
        return -1
    plt.figure()
    for file in file_list:
        steps, du = np.loadtxt(file, unpack=True, skiprows=2, delimiter=';')
        dx = file.split('_')[2][2:]
        dt = file.split('_')[3][2:-4]
        dx = float(dx) * lamb
        dt = float(dt) * tau
        label = r'$\Delta %s$=%.3fkpc, $\Delta t$=%.eMyr' % (variable, dx, dt)
        plt.plot(steps, du, label=label)
    # plt.title('Error of Diffusion - $%s$' % variable)
    plt.xlabel('steps $n$')
    plt.ylabel(r'$\Delta u$')
    loc = 'upper right'
    if variable == 'z':
        loc = 'center left'
    plt.legend(loc=loc)
    plt.xscale('log')
    plt.yscale('log')
    # plt.grid()
    plt.tight_layout()
    plt.savefig('ErrorDiffusion_%s.pdf' % variable)
    return 0


def plot_cr_flux_data(new_figure=True):
    """Plot the CR flux data of Voyager and AMS. Use current figure if new_figure is False."""
    file_list = glob.glob('plots/data/Protons_*_Ekin.csv')
    if not file_list:
        return -1
    if new_figure:
        plt.figure()

    for file in file_list:
        ekin, ekin_lo, ekin_up, y, yerr_lo, yerr_up = np.loadtxt(file, unpack=True, skiprows=3,
                                                                 usecols=(0, 1, 2, 3, 8, 9), delimiter=',')
        label = file.split('_')[1]
        if 'Voyager' in file:
            label = 'Voyager 1'
        if 'AMS' in file:
            label = 'AMS-2'
            max_element = 30
            ekin = ekin[:max_element] * 1e3
            ekin_lo = ekin_lo[:max_element] * 1e3
            ekin_up = ekin_up[:max_element] * 1e3
            y = y[:max_element] * 1e-3
            yerr_lo = yerr_lo[:max_element] * 1e-3
            yerr_up = yerr_up[:max_element] * 1e-3
        plt.errorbar(x=ekin, y=y, xerr=(ekin - ekin_lo, ekin_up - ekin), yerr=(yerr_lo, yerr_up), label=label, fmt='.')

    if new_figure:
        plt.legend()
        plt.xlabel('$E_{kin}$ [MeV]')
        plt.ylabel('$j_0$ [(m$^2$ s sr MeV)$^{-1}$]')
        # plt.title('Experimental CR flux')
        plt.xscale('log')
        plt.yscale('log')
        plt.grid()
        plt.tight_layout()
        plt.savefig('ExpCRFlux.pdf')

    return 0


def plot_ionisation_data(new_figure=True, do_label=True, do_plot_phan=True):
    """Plot the ionisation rate data, and the inisation rate by Phan et al. if do_plot_phan True.
    Use current figure if new_figure is False."""
    file_list = glob.glob('plots/data/ionisation/*.csv')
    if not file_list:
        return -1
    if new_figure:
        plt.figure()

    for file in file_list:
        x, x_lo, x_up, y, yerr = np.loadtxt(file, unpack=True, skiprows=1, delimiter=',')
        if 'Indriolo' in file:
            """x and x_lo are swapped"""
            dummy = x
            x = x_lo
            x_lo = dummy
            """upper errors doens't fit"""
            x_up = x + x - x_lo
        if not isinstance(x, np.ndarray):
            x = np.array([x])
            x_lo = [x_lo]
            x_up = [x_up]
            y = [y]
            yerr = [yerr]
        label = file.split('/')[-1].split('.')[0]
        if not do_label:
            label = None
        fmt = 'xr'
        if 'Caselli' in file:
            fmt = '.b'
        if 'Williams' in file:
            fmt = '^b'
        if 'Maret' in file:
            fmt = '*m'
        if 'Indriolo' in file:
            if 'data_points' in file:
                fmt = 'sk'
            if 'upper_limits' in file:
                fmt = 'vy'
        plt.errorbar(x=x, y=y, xerr=(x - x_lo, x_up - x), yerr=yerr, label=label, fmt=fmt)

    if do_plot_phan:
        val = 1.23196e-17
        label = 'Phan et al. - paper'
        plt.axhline(val, linestyle='--', color='r', label=label)

    if new_figure:
        plt.xlabel(r'$N_{H_2}$ [$cm^{-2}$]')
        plt.ylabel(r'$\zeta (N_{H_2})$ [$s^{-1}$]')
        plt.title('Experimental ionisation rate')
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.grid()
        plt.tight_layout()
        plt.savefig('ExpIonisationRate.pdf')
    return 0


def plot_template(file_appendix='', pos_t=[0.0], pos_p=[0.0], pos_r=[0.0]):
    """
    Plot the .hdf template for different times t, impulse p, distances r.

    :param file_appendix: file identifier for the template,
     the file 'greens_template'+file_appendix_'.hdf5' is used if it exists.
    :param pos_t: The positions in time t to plot, list or array.
    :param pos_p: The positions in impulse p to plot, list or array.
    :param pos_r: The positions in distance r to plot, list or array.
    :return: 0, or -1 if file don't exist
    """
    file_list = glob.glob('greens_template%s.hdf5' % file_appendix)
    if not file_list:
        return -1
    file = h5py.File(file_list[0], 'r')
    t = file['t'][...]
    log_p = file['log_p'][...]
    r = file['r'][...]
    u = file['u'][...]
    log_p = log_p[1:]  # cut of first point
    u = u[:, 1:, :]

    r = r * lamb
    p = np.exp(log_p) * p0
    t = t * tau
    u = u * q0 / (lamb ** 3)

    index_t = []
    for x in pos_t:
        if x == -1:
            index_t += [-1]
        else:
            index_t += [np.abs(t - x).argmin()]
    index_p = []
    for x in pos_p:
        if x == -1:
            index_p += [-1]
        else:
            index_p += [np.abs(p - x).argmin()]
    index_r = []
    for x in pos_r:
        if x == -1:
            index_r += [-1]
        else:
            index_r += [np.abs(r - x).argmin()]

    if len(index_p) and len(index_r):
        plt.figure()
        for i_p in index_p:
            for i_r in index_r:
                plt.plot(t[:], u[:, i_p, i_r], label='p=%.1e MeV, r=%.1f kpc' % (p[i_p], r[i_r]))
        plt.xlabel('t [Myr]')
        plt.ylabel('$\\psi$(t, r, p)')
        plt.title('CR Diffusion Template, t axis')
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.tight_layout()
        plt.savefig('CRDiffTemplate_t%s.pdf' % file_appendix)

    if len(index_t) and len(index_r):
        plt.figure()
        for i_t in index_t:
            for i_r in index_r:
                plt.plot(p, u[i_t, :, i_r], label='t=%.1e Myr, r=%.1f kpc' % (t[i_t], r[i_r]))
        plt.xlabel('p [MeV]')
        plt.ylabel('$\\psi$(t, p, r)')
        plt.title('CR Diffusion Template, p axis')
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.grid()
        plt.ylim(1e35, 1e53)
        plt.tight_layout()
        plt.savefig('CRDiffTemplate_p%s.pdf' % file_appendix)

    if len(index_t) and len(index_p):
        plt.figure()
        for i_t in index_t:
            for i_p in index_p:
                plt.plot(r, u[i_t, i_p, :], label='t=%.1e Myr, p=%.1e MeV' % (t[i_t], p[i_p]))
        plt.xlabel('r [kpc]')
        plt.ylabel('$\\psi$(t, p, r)')
        plt.title('CR Diffusion Template, r axis')
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.tight_layout()
        plt.savefig('CRDiffTemplate_r%s.pdf' % file_appendix)
    return 0


def plot_time_development(file_appendix='', r0_array=[0.1], p0_array=[1, 100, 10000], use_analytic=True,
                          plot_free_greens_function=False):
    """
    Plot the time development of the CR density.

    :param file_appendix: file identifier for the template,
     the file 'greens_template'+file_appendix_'.hdf5' is used if it exists.
    :param r0_array: The distances r_0 to be plotted (array)
    :param p0_array: The impulse p to be plotted (array)
    :param use_analytic: if True analytic calculation is plotted too (only if template file+'_analytic' exists)
    :param plot_free_greens_function: True if free Green's function should be plotted too
    :return: 0, or -1 if file don't exist
    """
    file_list = glob.glob('greens_template%s.hdf5' % file_appendix)
    if file_appendix == '_3':
        file_list = glob.glob('greens_template.hdf5')
    if not file_list:
        return -1
    file = h5py.File(file_list[0], 'r')
    t = file['t'][...]
    log_p = file['log_p'][...]
    r = file['r'][...]
    u = file['u'][...]
    log_p = log_p[1:]  # cut of first point
    u = u[:, 1:, :]

    t = t * tau
    u = u * q0 / (lamb ** 3)

    if use_analytic:
        file_list = glob.glob('greens_template%s_analytic.hdf5' % file_appendix)
        if file_appendix == '_2' or file_appendix == '_2_2':
            file_list = glob.glob('greens_template_analytic.hdf5')
        if file_appendix == '_3':
            file_list = glob.glob('greens_template_3.hdf5')
        if not file_list:
            use_analytic = False
    if use_analytic:
        file = h5py.File(file_list[0], 'r')
        r_analytic = file['r'][...]
        log_p_analytic = file['log_p'][...]
        t_analytic = file['t'][...]
        u_analytic = file['u'][...]

        t_analytic = t_analytic * tau
        u_analytic = u_analytic * q0 / (lamb ** 3)

    plt.figure()
    for r0 in r0_array:
        index_r0 = np.abs(r - r0 / lamb).argmin()
        for p_val in p0_array:
            index_p0 = np.abs(log_p - np.log(p_val / p0)).argmin()
            p = p0 * np.exp(log_p[index_p0])
            y = (p / p0) ** gamma * t ** (3 / 2) * u[:, index_p0, index_r0]
            label = 'r=%.1fkpc, p=%.1eMeV/c' % (r[index_r0] * lamb, p)
            plt.plot(t, y, '-', label=label)

    if use_analytic:
        plt.gca().set_prop_cycle(None)
        label = 'analytic'
        if file_appendix == '_3':
            label = 'finer grid'
        for r0 in r0_array:
            index_r0 = np.abs(r_analytic - r0 / lamb).argmin()
            for p_val in p0_array:
                index_p0 = np.abs(log_p_analytic - np.log(p_val / p0)).argmin()
                p = p0 * np.exp(log_p_analytic[index_p0])
                y = (p / p0) ** gamma * t_analytic ** (3 / 2) * u_analytic[:, index_p0, index_r0]
                plt.plot(t_analytic, y, '--', label=label)
                label = None

    if plot_free_greens_function:
        plt.gca().set_prop_cycle(None)
        label = "free Green`s function"
        for r0 in r0_array:
            index_r0 = np.abs(r - r0 / lamb).argmin()
            for p_val in p0_array:
                index_p = np.abs(log_p - np.log(p_val / p0)).argmin()
                kappa_val = kappa0 * np.exp(delta_analytic * log_p[index_p])
                """psi = e^(- r^2 / (4 t kappa)) / (4 pi t kappa)^(3/2) * q(p)"""
                y = np.exp(-(r[index_r0] * lamb) ** 2 / (4. * t * kappa_val)) / (
                        4. * np.pi * kappa_val) ** (3. / 2.) * q0
                plt.plot(t, y, ':', label=label)
                label = None

    plt.xlabel('$t-t_0$ [Myr]')
    plt.ylabel(r'$(\frac{p}{p_0})^{\Gamma} (\frac{t-t_0}{Myr})^{3/2} \Psi(r,p,t)$ [$kpc^{-3} (MeV/c)^{-1}$]')
    # plt.title('Time development')
    plt.legend()
    # box = plt.gca().get_position()
    # plt.gca().set_position([box.x0, box.y0, box.width * 0.8, box.height])
    # plt.legend(loc='center left', bbox_to_anchor=(1, 0.5))
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(bottom=1e40, top=1e50)
    if plot_free_greens_function:
        plt.ylim(bottom=1e40, top=1e52)
    plt.grid()
    plt.tight_layout()
    plt.savefig('CRDiffTimeDevelopment%s.pdf' % file_appendix)
    return 0


def plot_spectrum(file_appendix='', t0_array=[0.0], r0_array=[0.0], ylim_bottom=1e35, plot_power_law=False):
    """
    Plot the spectrum in one .hdf template for different times t0 and distances r0.

    :param file_appendix: file identifier for the template,
     the file 'greens_template'+file_appendix_'.hdf5' is used if it exists.
    :param t0_array: The times t_0 to be plotted (array)
    :param r0_array: The distances r_0 to be plotted (array)
    :param ylim_bottom: The bottom ylim, spectra below this value are not plotted. 0 if it should be automatic
    :param plot_power_law: if True the power low dependencies of the steady state solution ae plotted too.
    :return: 0, or -1 if file don't exist
    """
    file_list = glob.glob('greens_template%s.hdf5' % file_appendix)
    if not file_list:
        return -1
    file = h5py.File(file_list[0], 'r')
    t = file['t'][...]
    log_p = file['log_p'][...]
    r = file['r'][...]
    u = file['u'][...]
    log_p = log_p[1:]  # cut of first point
    u = u[:, 1:, :]

    r = r * lamb
    p = np.exp(log_p) * p0
    t = t * tau
    u = u * q0 / (lamb ** 3)
    ekin = np.sqrt(p ** 2 + mass_p ** 2) - mass_p

    index_t = []
    for x in t0_array:
        if x == -1:
            index_t += [-1]
        else:
            index_t += [np.abs(t - x).argmin()]
    index_r = []
    for x in r0_array:
        if x == -1:
            index_r += [-1]
        else:
            index_r += [np.abs(r - x).argmin()]

    # x_axis = ekin
    x_axis = p

    plt.figure()
    linestyles = ['-', '--', ':']
    for i, i_r in enumerate(index_r):
        plt.gca().set_prop_cycle(None)
        for i_t in index_t:
            y = u[i_t, :, i_r]
            if i == 0:
                label = 't=%.1e Myr' % t[i_t]
            if len(index_r) == 1:
                label += ', r=%.1f kpc' % r[i_r]
            if np.max(y) > ylim_bottom or True:
                obj = plt.plot(x_axis, y, linestyles[i], label=label)
                label = None
                if plot_power_law:
                    y_power_law_1 = y[0] * np.power(p / p[0], -n_analytic - gamma + 1)
                    # y_power_law_1 = y[0] * np.power(p / p[0], -n_analytic)
                    plt.plot(x_axis, y_power_law_1, '--', color=obj[-1].get_color())
                    y_power_law_2 = y[-1] * np.power(p / p[-1], -gamma - delta_analytic)
                    plt.plot(x_axis, y_power_law_2, '-.', color=obj[-1].get_color())
    if len(index_r) > 1:
        for i, i_r in enumerate(index_r):
            plt.plot(0, 0, linestyles[i], color='gray', label='r=%.1f kpc' % r[i_r])

    # plt.xlabel('$E_{kin}$ [MeV]')
    plt.xlabel('$p$ [MeV/c]')
    plt.ylabel('$\\psi$(p) [$kpc^{-3} (MeV/c)^{-1}$]')
    # plt.title('Spectrum')
    plt.legend(ncol=2)
    plt.xscale('log')
    plt.yscale('log')
    plt.grid()
    # plt.ylim(1e35, 1e53)
    if ylim_bottom > 0:
        plt.ylim(bottom=ylim_bottom)
    plt.ylim(top=2e52)
    plt.tight_layout()
    if plot_power_law:
        plt.savefig('CRDiffSpectrum_power_law%s.pdf' % file_appendix)
    else:
        plt.savefig('CRDiffSpectrum%s.pdf' % file_appendix)
    return 0


def source_distribution(source_count=None, t_start=1e-3, t_end=1e2, r_max=R):
    """
    Calculates the source distribution

    :param source_count: count of SN sources, if None it is calculated put of t_end with 3SN/100yr
    :param t_start: start time in Myr
    :param t_end: end time in Myr
    :param r_max: maximal distance in kpc
    :return: t0, r0 as numpy array
    """
    if source_count is None:
        source_count = int(3 * t_end / 1e-4)
    """time distribution of sources, homogeneous distribution in t"""
    t0 = np.random.uniform(t_start, t_end, source_count)
    """spacial distribution of sources, homogeneous distribution in r"""
    r0 = r_max * np.random.power(2, source_count)
    return t0, r0


def preprocess_u_for_interpolation(u_in, t_in, r_in):
    """
    Preprocess u, t, r so they can be used by interpolate_template

    :param u_in: u like it is saved in the template
    :param t_in: t like it is saved in the template
    :param r_in: r like it is saved in the template
    :return: log_u, log_t, log_r as input for interpolate template
    """
    log_t = np.log(t_in)
    r_in[0] = np.finfo(np.float64).eps  # use minimal value instead of 0, so log doesn't get problems
    log_r = np.log(r_in)
    u_in = np.swapaxes(u_in, 1, 2)  # u[t, r, log_p]
    # u_in = u_in * q0 / (lamb ** 3)
    # u_in[u_in == 0] = np.finfo(np.float64).eps  # use minimal value instead of 0, so log doesn't get problems
    log_u = np.log(u_in)
    log_u[log_u == -np.inf] = np.finfo(np.float64).min  # use minimal value instead of -inf
    return log_u, log_t, log_r


def interpolate_template(log_u, log_t, log_r, t0, r0):
    """
    Interpolate the template log_u in r and t.

    :param log_u: log(u) (u is the template) but with swapped axis, so u is in the form u[t, log_r, log_p]
    :param log_t: logarithmic t axis, log(t)
    :param log_r: logarithmic r axis, log(r)
    :param t0: array of times t_0 of the sources in Myr
    :param r0: array of position r_0 of the sources in kpc
    :return: interpolated spectrum for every source u[i, p]
    """
    log_t0 = np.log(t0 / tau)
    log_r0 = np.log(r0 / lamb)

    # log_t0_interior = log_t0[log_t[0] <= log_t0 <= log_t[-1]]
    # log_r0_interior = log_r0[log_t[0] <= log_t0 <= log_t[-1]]
    log_t0_interior = log_t0[log_t0 <= log_t[-1]]
    log_r0_interior = log_r0[log_t0 <= log_t[-1]]
    log_t0_exterior = log_t0[log_t0 > log_t[-1]]
    log_r0_exterior = log_r0[log_t0 > log_t[-1]]

    u_out = np.empty((0, log_u.shape[2]), dtype=np.float64)
    """Interpolate inside the gird"""
    if log_t0_interior.size > 0:
        points = np.transpose([log_t0_interior, log_r0_interior])
        val = interpolate.interpn((log_t, log_r), log_u, points, method='linear')
        u_out = np.concatenate((u_out, val))

    """Interpolate outside the grid"""
    if log_t0_exterior.size > 0:
        points = np.transpose([np.full(log_t0_exterior.shape, log_t[-1]), log_r0_exterior])
        val = interpolate.interpn((log_t, log_r), log_u, points, method='linear')
        points_m = np.transpose([np.full(log_t0_exterior.shape, log_t[-2]), log_r0_exterior])
        val_m = interpolate.interpn((log_t, log_r), log_u, points_m, method='linear')

        log_t0_exterior_b = np.broadcast_to(np.reshape(log_t0_exterior, (log_t0_exterior.size, 1)), val.shape)
        a = (val - val_m) / (log_t[-1] - log_t[-2])
        val = val + a * (log_t0_exterior_b - log_t[-1])

        u_out = np.concatenate((u_out, val))

    u_out = np.exp(u_out) * q0 / (lamb ** 3)
    return u_out


def plot_interpolate_time_development(file_appendix='', r0_array=[0.1], p0_array=[1, 100, 10000]):
    """
    Plot the time development of the CR density calculated with the interpolation.

    :param file_appendix: file identifier for the template,
     the file 'greens_template'+file_appendix_'.hdf5' is used if it exists.
    :param r0_array: The distances r_0 to be plotted (array)
    :param p0_array: The impulse p to be plotted (array)
    :return: 0, or -1 if file don't exist
    """
    file_list = glob.glob('greens_template%s.hdf5' % file_appendix)
    if not file_list:
        return -1
    file = h5py.File(file_list[0], 'r')
    t = file['t'][...]
    log_p = file['log_p'][...]
    r = file['r'][...]
    u = file['u'][...]
    log_p = log_p[1:]  # cut of first point
    u = u[:, 1:, :]

    log_u, log_t, log_r = preprocess_u_for_interpolation(u, t, r)
    t = t * tau
    u = u * q0 / (lamb ** 3)
    t_interpolate = np.logspace(-3, 3, 500)

    r0_array = np.array(r0_array, ndmin=1)
    u_interpolate = np.zeros((len(t_interpolate), len(r0_array), len(log_p)))
    for i, dt in enumerate(t_interpolate):
        dt_array = np.array([dt] * r0_array.size, ndmin=1)
        u_interpolate[i] = interpolate_template(log_u, log_t, log_r, dt_array, r0_array)

    plt.figure()
    for i, r0 in enumerate(r0_array):
        index_r0 = np.abs(r - r0 / lamb).argmin()
        for p_val in p0_array:
            index_p0 = np.abs(log_p - np.log(p_val / p0)).argmin()
            p = p0 * np.exp(log_p[index_p0])
            y = (p / p0) ** gamma * t ** (3 / 2) * u[:, index_p0, index_r0]
            label = 'r=%.1fkpc, p=%.1eMeV/c' % (r[index_r0] * lamb, p)
            plt.plot(t, y, '.', label=label)
    plt.gca().set_prop_cycle(None)
    label_interpolate = 'interpolated'
    for i, r0 in enumerate(r0_array):
        for p_val in p0_array:
            index_p0 = np.abs(log_p - np.log(p_val / p0)).argmin()
            p = p0 * np.exp(log_p[index_p0])
            y_interpolate = (p / p0) ** gamma * t_interpolate ** (3 / 2) * u_interpolate[:, i, index_p0]
            plt.plot(t_interpolate, y_interpolate, '--', label=label_interpolate)
            label_interpolate = None

    plt.xlabel('$t-t_0$ [Myr]')
    plt.ylabel(r'$(\frac{p}{p_0})^{\Gamma} (\frac{t-t_0}{Myr})^{3/2} \Psi(r,p,t)$ [$kpc^{-3} (MeV/c)^{-1}$]')
    # plt.title('Interpolated time development')
    plt.legend()
    plt.xscale('log')
    plt.yscale('log')
    plt.ylim(bottom=1e40, top=1e50)
    plt.grid()
    plt.tight_layout()
    plt.savefig('CRDiffTimeDevelopmentInterpolated%s.pdf' % file_appendix)
    return 0


def calculate_scaling_factor(ekin, j0):
    """Calculate the scaling factor by comparing the calculated j0 with the AMS data"""
    ekin_ams, y = np.loadtxt('plots/data/Protons_AMS02_Ekin.csv', unpack=True, skiprows=3, usecols=(0, 3),
                             delimiter=',')
    scaling_index = -1
    ekin_ams *= 1e3
    scaling_index_ams = np.argmin(abs(ekin_ams - ekin[scaling_index]))
    j0_ams = y[scaling_index_ams] * 1e-3
    scaling_factor = j0_ams / j0[scaling_index]
    return scaling_factor


def added_spectrum(file_appendix='', t_end=1e2, rescale=False, load_hdf5=False, do_plot=True,
                   do_plot_hist=False, do_plot_strongest=False):
    """
    Calculate and plot the CR spectrum created by a distribution of sources sources.

    :param file_appendix: file identifier for the template,
     the file 'greens_template'+file_appendix_'.hdf5' is used if it exists.
    :param t_end: end time in Myr
    :param rescale: if True the spectrum is rescaled to fit for high energies
    :param load_hdf5: if True the file 'one_spectrum%s.hdf5' is load instead of calculation the distribution
    :param do_plot: if True the plot 'CRDiffAddedSpectrum' is created
    :param do_plot_hist: if True the histogram of source spectrum and cumulative spectrum plot is created
    :param do_plot_strongest: if True the plot with only the strongest sources is created
    :return: ekin, u (kinetic energy, added spectrum), or -1 if file don't exist
    """
    file_list = glob.glob('one_spectrum%s.hdf5' % file_appendix)
    if load_hdf5 and not file_list:
        return -1
    if load_hdf5:
        file = h5py.File(file_list[0], 'r')
        ekin = file['ekin'][...]
        p = file['p'][...]
        u_interpol = file['u'][...]
        t0 = file['t0'][...]
        r0 = file['r0'][...]
    else:
        file_list = glob.glob('greens_template%s.hdf5' % file_appendix)
        if not file_list:
            return -1

        print('Start calculating source distribution.')
        timer_start = time.time()
        t_start = 1e-3
        r_max = R
        t0, r0 = source_distribution(t_start=t_start, t_end=t_end, r_max=r_max)
        timer_end = time.time()
        print('Process finished in %f seconds.' % (timer_end - timer_start))

        """Sort t0, so the indexes in u wont change because of splitting t0"""
        index_sorted = np.argsort(t0)
        t0 = t0[index_sorted]
        r0 = r0[index_sorted]

        file = h5py.File(file_list[0], 'r')
        t_in = np.array(file['t'][...], dtype=np.float64)
        log_p = np.array(file['log_p'][...], dtype=np.float64)
        r_in = np.array(file['r'][...], dtype=np.float64)
        u_in = np.array(file['u'][...], dtype=np.float64)
        log_p = log_p[1:]  # cut of first point
        u_in = u_in[:, 1:, :]

        p = np.exp(log_p) * p0
        ekin = np.sqrt(p ** 2 + mass_p ** 2) - mass_p

        print('Start calculating preprocessing u.')
        timer_start = time.time()
        log_u, log_t, log_r = preprocess_u_for_interpolation(u_in, t_in, r_in)
        timer_end = time.time()
        print('Process finished in %f seconds.' % (timer_end - timer_start))

        print('Start calculating added spectrum.')
        timer_start = time.time()
        u_interpol = interpolate_template(log_u, log_t, log_r, t0, r0)
        timer_end = time.time()
        print('Process finished in %f seconds.' % (timer_end - timer_start))

        """Save the results as .hdf5 file"""
        file = h5py.File('one_spectrum%s.hdf5' % file_appendix, 'w')
        file.create_dataset('ekin', data=ekin)
        file.create_dataset('p', data=p)
        file.create_dataset('u', data=u_interpol)
        file.create_dataset('t0', data=t0)
        file.create_dataset('r0', data=r0)

    u = np.sum(u_interpol, 0)
    beta = (p / mass_p) / np.sqrt(1. + (p / mass_p) ** 2)
    """Calculate j0: j0 = beta / (4 pi) * sqrt(1 + (m c / p)^2) u"""
    j0 = beta / (4. * np.pi) * np.sqrt(1. + (mass_p / p) ** 2) * u * 1.02e-50  # intenesity in (m^2 s sr MeV)^-1
    j0_phan = intensity_phan(ekin)  # intensity by phan et al

    scaling_factor = 1
    if rescale:
        scaling_factor = calculate_scaling_factor(ekin, j0)
        j0 *= scaling_factor
        u *= scaling_factor

    if do_plot:
        fig = plt.figure()
        plt.plot(ekin, j0, label='Added Spectrum')
        plt.plot(ekin, j0_phan, label='Phan et al.')
        plt.xlabel('$E_{kin}$ [MeV]')
        plt.ylabel('$j_0$ [(m$^2$ s sr MeV)$^{-1}$]')
        # plt.title('CR Diffusion added spectrum')
        plot_cr_flux_data(False)
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffAddedSpectrum%s.pdf' % file_appendix)

    if do_plot_hist:
        fig_cum = plt.figure(1, clear=True)
        fig_hist = plt.figure(2, clear=True)
        percent_values = [0.1, 0.5]
        for val_ekin in [1e0, 1e2, 1e4]:
            index_ekin = np.argmin(abs(ekin - val_ekin))
            label = '$E_{kin}$=%.eMeV' % ekin[index_ekin]
            x = u_interpol[:, index_ekin]
            x = np.sort(x)[::-1]
            x_cum = np.cumsum(x)
            x_cum = x_cum / x_cum[-1]
            number = np.linspace(1, x.size, x.size)

            plt.figure(1)
            label_str = label
            for percent in percent_values:
                n = np.argmin(abs(x_cum - percent)) + 1
                label_str += ', %.f%%: %4i' % (percent * 100, n)
            plt.plot(number, x_cum, label=label_str)

            plt.figure(2)
            x = x[x > 0]
            bins = np.logspace(np.log10(np.min(x)), np.log10(np.max(x)), 100)
            plt.hist(x, bins=bins, label=label)
            """Plot distribution for comparision"""
            # mean = np.mean(x)
            # sigma = np.std(x)
            # dist = np.random.normalsize=x.size)
            # dist = np.random.lognormal(size=x.size)
            # dist = dist[dist>0]
            # bins = np.logspace(np.log10(np.min(dist)), np.log10(np.max(dist)), 100)
            # plt.hist(dist, bins=bins)
        plt.figure(1)
        for percent in percent_values:
            plt.axhline(percent, linestyle='--')
        plt.xlabel('source count')
        plt.ylabel(r'$\frac{\psi_{cumulative}}{\psi_{complete}}$')
        # plt.title('CR Diffusion distribution of the Spectrum - cumulative')
        plt.legend(loc='lower right')
        plt.xscale('log')
        plt.yscale('log')
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffSpectrumDistribution%s_cumulative.pdf' % file_appendix)

        plt.figure(2)
        plt.xlabel('$\\psi$ [$kpc^{-3} (MeV/c)^{-1}$]')
        plt.ylabel('count')
        # plt.title('CR Diffusion distribution of the Spectrum - histogram')
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffSpectrumDistribution%s_hist.pdf' % file_appendix)

    if do_plot_strongest:
        fig = plt.figure()
        plt.plot(ekin, j0, label='Complete Spectrum')
        plt.plot(ekin, j0_phan, label='Phan et al.')
        x = np.array(u_interpol)
        index_ekin = np.argmin(abs(ekin - 30))
        index_sorted = np.argsort(x[:, index_ekin])[::-1]
        x = x[index_sorted]
        # plt.gca().set_prop_cycle(None)
        for count in [1, 5, 100]:
            x_cum = np.sum(x[:count], axis=0)
            x_cum = beta / (4. * np.pi) * np.sqrt(1. + (mass_p / p) ** 2) * x_cum * 1.02e-50 * scaling_factor
            obj = plt.plot(ekin, x_cum, ':', label='strongest %i' % count)
            x_cum = np.sum(x[count:], axis=0)
            x_cum = beta / (4. * np.pi) * np.sqrt(1. + (mass_p / p) ** 2) * x_cum * 1.02e-50 * scaling_factor
            plt.plot(ekin, x_cum, '--', color=obj[-1].get_color())
        plt.plot(0, 0, '--', color='grey', label='without strongest')
        plt.xlabel('$E_{kin}$ [MeV]')
        plt.ylabel('$j_0$ [(m$^2$ s sr MeV)$^{-1}$]')
        # plt.title('CR Diffusion added spectrum strongest sources')
        plot_cr_flux_data(False)
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffSpectrumStrongest%s.pdf' % file_appendix)

        """Source position"""
        plt.figure()
        i_max = 1000
        # i_max = 20
        for i in range(i_max):
            # obj = plt.plot(r0[index_sorted[i]], t0[index_sorted[i]], ',', color='black', visible=False)
            obj = plt.plot(r0[index_sorted[i]], t0[index_sorted[i]], ',', visible=False)
            fontsize = 80 * x[i, index_ekin] / x[0, index_ekin]
            # fontsize = 12 - np.log(x[i, index_ekin] / x[0, index_ekin])
            plt.text(r0[index_sorted[i]], t0[index_sorted[i]], str(i + 1), fontsize=fontsize,
                     horizontalalignment='center', verticalalignment='center', color=obj[-1].get_color())
        plt.xlabel('$r_0$ [kpc]')
        plt.ylabel('$t_0$ [Myr]')
        # plt.title('CR Diffusion added spectrum strongest sources')
        # plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffSpectrumStrongestPositions%s.pdf' % file_appendix)
    return ekin, u


def ionrate_energy_loss(ekin, density_n):
    """Energy loss in eV/s"""
    beta = np.sqrt(1. - 1. / (1. + ekin / mass_p))
    return f_ion * density_n * (1.0 + 0.0185 * np.log(beta)) * 2.0 * beta ** 2 / (beta_0 ** 3 + 2.0 * beta ** 3)


def ionrate_CR_density(ekin, interpolator_log_u):
    """CR density N in (cm^3 MeV)^-1"""
    u_val = np.exp(interpolator_log_u(np.log(ekin)))  # Interpolate function
    return (ekin + mass_p) / np.sqrt(ekin ** 2 + 2. * ekin * mass_p) * u_val * 3.4e-65


def ionrate_CR_density_phan(ekin):
    """CR density N with j0 by Phan et al. in (cm^3 MeV)^-1"""
    beta = np.sqrt(1. - 1. / (1. + ekin / mass_p) ** 2)
    return 4. * np.pi / (beta * light_speed_si) * intensity_phan(ekin) * 1e-6


def calculate_ionisation_rate(u, log_ekin, density_n, nH2_array, use_phan=False):
    """
    Calculate the ionisation rate.

    :param u: spectrum u, u[log_ekin]
    :param log_ekin: logarithmic kinetic energy
    :param density_n: the density of the gas, has no effect since it cancels out
    :param nH2_array: the column density nH2, only for the size of array since there is no denpendence on it
    :param use_phan: if True the fitted spectrum by Phan et al. is used instead of u
    :return: ionisatoin_rate (numpy array with same size as nH2_array)
    """
    interpolator_log_u = interpolate.interp1d(log_ekin, np.log(u), kind='linear', assume_sorted=True)

    def integrand(ekin):
        """abs(dE_kin/dt) * N / 32eV"""
        return ionrate_energy_loss(ekin, density_n) * ionrate_CR_density(ekin, interpolator_log_u) / 32

    if use_phan:
        def integrand(ekin):
            """abs(dE_kin/dt) * N / 32eV"""
            return ionrate_energy_loss(ekin, density_n) * ionrate_CR_density_phan(ekin) / 32

    """integrate with scipy"""
    """primary ionisation rate in cm^-3 s-1"""
    out = integrate.quad(integrand, 1, 10e3)  # 1 MeV - 10 GeV
    primary_ionisation = out[0]

    ionisation_rate = np.zeros(nH2_array.shape)
    for i, x in enumerate(nH2_array):
        ionisation_rate[i] = nu1 * nu2 * nu3 * primary_ionisation / density_n

    return ionisation_rate


def plot_ionisation_rate(file_appendix='', t_end=1e2, rescale=False, load_hdf5=False, do_plot=True,
                         do_plot_added_spectrum=False, do_plot_hist=False, do_plot_strongest=False):
    """
    Plot the ionisation rate.

    :param file_appendix: file identifier for the template,
     the file 'greens_template'+file_appendix_'.hdf5' is used if it exists.
    :param t_end: end time in Myr
    :param rescale: if True the spectrum is rescaled to fit for high energies
    :param load_hdf5: if True the file 'one_spectrum%s.hdf5' is load instead of calculation the distribution
    :param do_plot: if True the plot 'CRDiffIonisationRate' is created
    :param do_plot_added_spectrum: if True the plot 'CRDiffAddedSpectrum' is created
    :param do_plot_hist: if True the histogram of source spectrum and cumulative spectrum plot is created
    :param do_plot_strongest: if True the plot with only the strongest sources is created
    :return: 0, or -1 if file don't exist
    """
    if load_hdf5:
        file_list = glob.glob('one_spectrum%s.hdf5' % file_appendix)
        if not file_list:
            return -1
    else:
        file_list = glob.glob('greens_template%s.hdf5' % file_appendix)
        if not file_list:
            return -1

    ekin, u = added_spectrum(file_appendix, t_end, rescale, load_hdf5, do_plot_added_spectrum,
                             do_plot_hist, do_plot_strongest)
    log_ekin = np.log(ekin)  # log(E_kin / MeV)

    nH2_array = np.power(10, np.linspace(20, 23, 10))
    print('Start calculating ionisation rate.')
    timer_start = time.time()
    ionisation_rate = calculate_ionisation_rate(u, log_ekin, n_ion, nH2_array)
    ionisation_rate_phan = calculate_ionisation_rate(u, log_ekin, n_ion, nH2_array, use_phan=True)
    timer_end = time.time()
    print('Process finished in %f seconds.' % (timer_end - timer_start))

    if do_plot:
        plt.figure()
        plt.plot(nH2_array, ionisation_rate, label='Calculated')
        plt.plot(nH2_array, ionisation_rate_phan, label='Phan et al.')
        plt.xlabel(r'$N_{H_2}$ [$cm^{-2}$]')
        plt.ylabel(r'$\xi (N_{H_2})$ [$s^{-1}$]')
        # plt.title('Ionisation rate')
        plot_ionisation_data(False, False, True)
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffIonisationRate%s.pdf' % file_appendix)
    return 0


def stochastic_analysis(file_appendix='', t_end=1e2, sample_size=100, p_quantile=[0.68, 0.95], rescale=False,
                        use_analytic=True, load_hdf5=False, do_plot_spectrum=True, do_plot_ionisation=True):
    """
    Calculate median and quantile of spectrum and ionisation rate for a sample of source distributions

    :param file_appendix: file identifier for the template,
     the file 'greens_template'+file_appendix_'.hdf5' is used if it exists.
    :param t_end: end time in Myr
    :param sample_size: the size of the sample
    :param p_quantile: the quantiles to show
    :param rescale: if True the spectrum is rescaled to fit for high energies
    :param use_analytic: if True analytic calculation is plotted too (only if template file+'_analytic' exists)
    :param load_hdf5: if True the file 'sample_spectrum%s.hdf5' is load instead of calculation the distribution
    :param do_plot_spectrum: if True the plot 'CRDiffStochasticSpectrum' is created
    :param do_plot_ionisation: if True the plot 'CRDiffStochasticIonisationRate' is created
    :return: 0, or -1 if file don't exist
    """
    file_list = glob.glob('sample_spectrum%s.hdf5' % file_appendix)
    if load_hdf5 and not file_list:
        return -1
    if load_hdf5:
        file = h5py.File(file_list[0], 'r')
        ekin = file['ekin'][...]
        u = file['u'][...]

        file_list = glob.glob('sample_spectrum%s_analytic.hdf5' % file_appendix)
        if file_appendix == '_2':
            file_list = glob.glob('sample_spectrum.hdf5')
        if file_appendix == '_analytic_2':
            file_list = glob.glob('sample_spectrum_analytic.hdf5')
        if not file_list:
            use_analytic = False
        if use_analytic:
            file = h5py.File(file_list[0], 'r')
            ekin_analytic = file['ekin'][...]
            u_analytic = file['u'][...]

        p = np.sqrt((ekin + mass_p) ** 2 - mass_p ** 2)
        beta = np.sqrt(1. - 1. / (1. + ekin / mass_p) ** 2)
        if use_analytic:
            p_analytic = np.sqrt((ekin_analytic + mass_p) ** 2 - mass_p ** 2)
            beta_analytic = np.sqrt(1. - 1. / (1. + ekin_analytic / mass_p) ** 2)
    else:
        file_list = glob.glob('greens_template%s.hdf5' % file_appendix)
        if not file_list:
            return -1
        file = h5py.File(file_list[0], 'r')
        t_in = np.array(file['t'][...], dtype=np.float64)
        log_p = np.array(file['log_p'][...], dtype=np.float64)
        r_in = np.array(file['r'][...], dtype=np.float64)
        u_in = np.array(file['u'][...], dtype=np.float64)
        log_p = log_p[1:]  # cut of first point
        u_in = u_in[:, 1:, :]

        p = np.exp(log_p) * p0
        ekin = np.sqrt(p ** 2 + mass_p ** 2) - mass_p
        beta = (p / mass_p) / np.sqrt(1. + (p / mass_p) ** 2)

        u_in, t_in, r_in = preprocess_u_for_interpolation(u_in, t_in, r_in)

        """use also analytic solution"""
        file_list = glob.glob('greens_template_%sanalytic.hdf5' % file_appendix)
        if not file_list:
            use_analytic = False
        if use_analytic:
            file = h5py.File(file_list[0], 'r')
            t_in_analytic = np.array(file['t'][...], dtype=np.float64)
            log_p_analytic = np.array(file['log_p'][...], dtype=np.float64)
            r_in_analytic = np.array(file['r'][...], dtype=np.float64)
            u_in_analytic = np.array(file['u'][...], dtype=np.float64)
            log_p_analytic = log_p_analytic[1:]  # cut of first point
            u_in_analytic = u_in_analytic[:, 1:, :]

            p_analytic = np.exp(log_p_analytic) * p0
            ekin_analytic = np.sqrt(p_analytic ** 2 + mass_p ** 2) - mass_p
            beta_analytic = (p_analytic / mass_p) / np.sqrt(1. + (p_analytic / mass_p) ** 2)

            u_in_analytic, t_in_analytic, r_in_analytic = preprocess_u_for_interpolation(u_in_analytic, t_in_analytic,
                                                                                         r_in_analytic)

        source_count = int(3 * t_end / 1e-4)
        t_start = 1e-3
        r_max = R

        u = np.empty((sample_size, u_in.shape[2]), dtype=np.float64)
        if use_analytic:
            u_analytic = np.empty((sample_size, u_in_analytic.shape[2]), dtype=np.float64)
        print('Start calculating %i added spectra.' % sample_size)
        timer_start = time.time()
        for i in range(sample_size):
            t0, r0 = source_distribution(source_count, t_start, t_end, r_max)
            u_interpol = interpolate_template(u_in, t_in, r_in, t0, r0)
            u[i] = np.sum(u_interpol, 0)
            if use_analytic:
                u_interpol = interpolate_template(u_in_analytic, t_in_analytic, r_in_analytic, t0, r0)
                u_analytic[i] = np.sum(u_interpol, 0)
            print('Calculated %i/%i in %f seconds' % (i + 1, sample_size, time.time() - timer_start), end='\r')
        timer_end = time.time()
        print('Process finished in %f seconds.' % (timer_end - timer_start))

        """Save the results as .hdf5 file"""
        file = h5py.File('sample_spectrum%s.hdf5' % file_appendix, 'w')
        file.create_dataset('ekin', data=ekin)
        file.create_dataset('u', data=u)
        if use_analytic:
            file = h5py.File('sample_spectrum_analytic%s.hdf5' % file_appendix, 'w')
            file.create_dataset('ekin', data=ekin_analytic)
            file.create_dataset('u', data=u_analytic)

    """Calculate j0: j0 = beta / (4 pi) * sqrt(1 + (m c / p)^2) u"""
    j0 = beta / (4. * np.pi) * np.sqrt(1. + (mass_p / p) ** 2) * u * 1.02e-50  # intenesity in (m^2 s sr MeV)^-1
    if use_analytic:
        j0_analytic = beta_analytic / (4. * np.pi) * np.sqrt(1. + (mass_p / p_analytic) ** 2) * u_analytic * 1.02e-50
    j0_phan = intensity_phan(ekin)  # intensity by phan et al

    """Give out the values for energy loss time"""
    if False:
        index0 = 6
        p02 = np.sqrt((ekin[index0] + mass_p) ** 2 - mass_p ** 2)
        print('p0\t', p02)
        print('E0\t', ekin[index0])
        u02 = np.mean(u[:, index0])
        j02 = np.mean(j0[:, index0])
        print('j0\t', j02)
        print('u0\t', u02)
        A = 3e4 / (np.pi * (R * 1.5) ** 2 * 2 * h)
        print('A\t', A)
        b02 = b0 * (p02 / p0) ** n_analytic
        q02 = q0 * (p02 / p0) ** (- gamma)
        print('b0\t', b02)
        print('q0\t', q02)
        fac = u02 * b02 / (A * q02 * p02)
        print('fac\t', fac)
        print('fac2\t', A * q02 * p02 / b02 * 1 / (gamma - 1))
        # fac = 1
        print('parameter', fac * (gamma - 1) - 1, 1 / (1 - gamma))
        p1 = p02 * np.power(fac * (gamma - 1) - 1, 1 / (1 - gamma))
        print('p1\t', p1)

    if rescale:
        j0_median = np.median(j0, axis=0)
        scaling_factor = calculate_scaling_factor(ekin, j0_median)
        print('scaling factor: %.4f, difference factor: %.2f' % (scaling_factor, 1 / scaling_factor))
        j0 *= scaling_factor
        u *= scaling_factor
        if use_analytic:
            j0_median_analytic = np.median(j0_analytic, axis=0)
            scaling_factor = calculate_scaling_factor(ekin_analytic, j0_median_analytic)
            print(scaling_factor)
            j0_analytic *= scaling_factor
            u_analytic *= scaling_factor

    j0_median = np.median(j0, axis=0)
    p_quantile = np.array(p_quantile)
    j0_quantile_lower = np.quantile(j0, (1. - p_quantile) / 2., axis=0)
    j0_quantile_upper = np.quantile(j0, (1. + p_quantile) / 2., axis=0)
    if use_analytic:
        j0_median_analytic = np.median(j0_analytic, axis=0)
        j0_quantile_lower_analytic = np.quantile(j0_analytic, (1. - p_quantile) / 2., axis=0)
        j0_quantile_upper_analytic = np.quantile(j0_analytic, (1. + p_quantile) / 2., axis=0)

    if do_plot_spectrum:
        fig = plt.figure()
        label = ''
        if '_2' in file_appendix:
            label = ' sample 1'
        plt.plot(ekin, j0_median, label='Median' + label)
        for i, p in enumerate(p_quantile):
            plt.fill_between(ekin, j0_quantile_lower[i], j0_quantile_upper[i],
                             label='%.f%%' % (p * 100) + label, alpha=0.3)
        if use_analytic:
            label = ' analytic'
            if '_2' in file_appendix:
                label = ' sample 2'
            plt.plot(ekin_analytic, j0_median_analytic, label='Median' + label, color='red')
            # for i, p in enumerate(p_quantile):
            #     plt.fill_between(ekin_analytic, j0_quantile_lower_analytic[i], j0_quantile_upper_analytic[i],
            #                      label='%.f%%' % (p * 100) + label, alpha=0.3)
        plt.plot(ekin, j0_phan, label='Phan et al.')
        plt.xlabel('$E_{kin}$ [MeV]')
        plt.ylabel('$j_0$ [(m$^2$ s sr MeV)$^{-1}$]')
        # plt.title('CR Diffusion added spectrum')
        plot_cr_flux_data(False)
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffStochasticSpectrum%s.pdf' % file_appendix)

    log_ekin = np.log(ekin)  # log(E_kin / MeV)
    if use_analytic:
        log_ekin_analytic = np.log(ekin_analytic)
    nH2_array = np.power(10, np.linspace(20, 23, 10))
    ionisation_rate_phan = calculate_ionisation_rate(u, log_ekin, n_ion, nH2_array, use_phan=True)

    ionisation_rate = np.empty((sample_size, nH2_array.size), dtype=np.float64)
    if use_analytic:
        ionisation_rate_analytic = np.empty((sample_size, nH2_array.size), dtype=np.float64)

    for i in range(sample_size):
        ionisation_rate[i] = calculate_ionisation_rate(u[i], log_ekin, n_ion, nH2_array)
        if use_analytic:
            ionisation_rate_analytic[i] = calculate_ionisation_rate(u_analytic[i], log_ekin_analytic, n_ion, nH2_array)

    ionrate_median = np.median(ionisation_rate, axis=0)
    ionrate_quantile_lower = np.quantile(ionisation_rate, (1. - p_quantile) / 2., axis=0)
    ionrate_quantile_upper = np.quantile(ionisation_rate, (1. + p_quantile) / 2., axis=0)
    if use_analytic:
        ionrate_median_analytic = np.median(ionisation_rate_analytic, axis=0)
        ionrate_quantile_lower_analytic = np.quantile(ionisation_rate_analytic, (1. - p_quantile) / 2., axis=0)
        ionrate_quantile_upper_analytic = np.quantile(ionisation_rate_analytic, (1. + p_quantile) / 2., axis=0)

    if do_plot_ionisation:
        fig = plt.figure()
        plt.plot(nH2_array, ionrate_median, label='Median')
        for i, p in enumerate(p_quantile):
            plt.fill_between(nH2_array, ionrate_quantile_lower[i], ionrate_quantile_upper[i],
                             label='%.f%%' % (p * 100), alpha=0.3)
        if use_analytic:
            label = ' analytic'
            if file_appendix == '_2' or file_appendix == '_3':
                label = ' sample 2'
            plt.plot(nH2_array, ionrate_median_analytic, label='Median' + label)
            for i, p in enumerate(p_quantile):
                plt.fill_between(nH2_array, ionrate_quantile_lower_analytic[i], ionrate_quantile_upper_analytic[i],
                                 label='%.f%%' % (p * 100) + label, alpha=0.3)
        plt.plot(nH2_array, ionisation_rate_phan, label='Phan et al. - simple')
        plt.xlabel(r'$N_{H_2}$ [$cm^{-2}$]')
        plt.ylabel(r'$\xi (N_{H_2})$ [$s^{-1}$]')
        # plt.title('Ionisation rate')
        plot_ionisation_data(False, False, True)
        plt.legend()
        plt.xscale('log')
        plt.yscale('log')
        plt.grid()
        plt.tight_layout()
        plt.savefig('CRDiffStochasticIonisationRate%s.pdf' % file_appendix)
    return 0


def main():
    # stochastic_analysis('', 1e2, 100, [0.68, 0.95], False, True, False, False, False)  # Generate sample
    plt.rcParams.update({'font.size': 12})
    # plot_energy_loss()
    # plot_diffusion_coefficient()
    # plot_characteristic_time_scales()
    # plot_error_one_dimensional_diffusion('z')
    # plot_error_one_dimensional_diffusion('r')

    # plot_time_development('', [0.1], [40, 1e2, 1e3, 1e4], True, False)
    # plot_time_development('_analytic', [0.1], [40, 1e2, 1e3, 1e4], False, True)
    # plot_time_development('_2', [0.1], [40, 1e2, 1e3, 1e4], True, False)
    # plot_time_development('_2_2', [0.1], [40, 1e2, 1e3, 1e4], True, False)
    # plot_time_development('_3', [0.1], [40, 1e2, 1e3, 1e4], True, False),

    # plot_interpolate_time_development('', [0.1], [40, 1e2, 1e3, 1e4])

    # plot_spectrum('', [1e-2, 2e-2, 1e0, 8e1], [0.1, 1., 10.], 1e35, False)
    # plot_spectrum('_analytic', [1e-2, 2e-2, 1e0, 8e1], [0.1, 1., 10.], 1e35, False)
    # plot_spectrum('', [1e0], [0.1], 0, True)
    # plot_spectrum('_analytic', [1e0], [0.1], 0, True)

    # added_spectrum('', 1e2, True, True, False, True, True)
    # added_spectrum('_analytic', 1e2, True, True, False, True, True)

    # stochastic_analysis('', 1e2, 100, [0.68, 0.95], True, False, True, True, True)
    # stochastic_analysis('_analytic', 1e2, 100, [0.68, 0.95], True, False, True, True, True)
    # stochastic_analysis('_2', 0.5e2, 100, [0.68, 0.95], False, True, True, True, False)
    # stochastic_analysis('_analytic_2', 0.5e2, 100, [0.68, 0.95], False, True, True, True, False)
    return 0


if __name__ == '__main__':
    main()
