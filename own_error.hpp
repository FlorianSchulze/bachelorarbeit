//
// Created by flosc on 13.05.2020.
//

#ifndef PROGRAMM_OWN_ERROR_HPP
#define PROGRAMM_OWN_ERROR_HPP
#include <exception>
#define THROW(message) throw(own_error(message,__FILE__,__LINE__))

///A class that provides an exception with an char* message.
class own_error : std::exception {
public:
    const char *message;
    const char *file;
    int line;

    own_error(const char *m, const char *f, int l) : message(m), file(f), line(l) {}
};

///Print the error and exit the program
void own_catch(const own_error &err);

#endif //PROGRAMM_OWN_ERROR_HPP
