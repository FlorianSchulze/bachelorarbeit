//
// Created by flosc on 25.05.2020.
//

#include "CRDiffusion.hpp"
#include "ThreeDimDiffusion.hpp"
#include "NDNumerics.hpp"
#include "parameters.hpp"
#include <cmath>
#include <iostream>
#include <numeric>
#include <hdf5.h>
#include <omp.h>

using namespace std;
using namespace plot;
#define IND(j) (SizeType) (j), subvector_index, index
#define USE_CALCULATE_STEPS 2

///Parameters for diffusion and energy loss coefficients
const NumType mass_p = MASS_P / P0; ///<dimensionless proton mass
///Parameters for Energy loss
const NumType n_WNM = 0.5, n_WIM = 0.5, n_HIM = 0.006; ///<density in 1/cm^3
const NumType f_WNM = 0.25, f_WIM = 0.25, f_HIM = 0.5; ///<filling factor
const NumType f_ion = 5.73955 / B0, f_coulomb = 9.77616 / B0; ///<factor ionisation/coulomb losses, dimensionless
const NumType beta_0 = 0.01; ///<beta_0 in ionisation loss
///x_m = 0.0286*sqrt(Te/2*10^6 K) in coulomb loss
const NumType xm_WNM = 0.0286 * sqrt(4e-2), xm_WIM = 0.0286 * sqrt(4e-2), xm_HIM = 0.0286 * sqrt(0.5);

NumType CRDiff::energy_loss_rate(NumType_I &log_p) {
//    return -exp((POWER_N - 1) * log_p); ///power law energy loss rate
    ///beta = (p/m) / sqrt(1 + (p/m)^2)
    NumType beta = (exp(log_p) / mass_p) / sqrt(1.0 + exp(2.0 * log_p) / (mass_p * mass_p)); ///<beta = v/c
//    ///dE/dt = f_WNM * dE/dt_WNM + f_WIM * dE/dt_WIM + f_HIM * dE/dt_HIM
//    ///dE/dt_WNM = dE/dt_ion; dE/dt_WIM, dE/dt_HIM = dE/dt_ion + dE/dt_coulomb
//    ///dp/dt = dp/DE dE/dt = sqrt(1 + (m/p)^2) dE/dt
//    return exp(-log_p) * sqrt(1.0 + mass_p * mass_p * exp(-2.0 * log_p)) * ( ///u is scaled with exp(log_p)
//            f_WNM * (
//                    -f_ion * n_WNM * (1.0 + 0.0185 * log(beta)) * 2.0 * beta * beta /
//                    (pow(beta_0, 3) + 2.0 * pow(beta, 3)))
//            + f_WIM * (
//                    -f_ion * n_WIM * (1.0 + 0.0185 * log(beta)) * 2.0 * beta * beta /
//                    (pow(beta_0, 3) + 2.0 * pow(beta, 3))
//                    - f_coulomb * n_WIM * beta * beta / (pow(xm_WIM, 3) + pow(beta, 3)))
//            + f_HIM * (
//                    -f_ion * n_HIM * (1.0 + 0.0185 * log(beta)) * 2.0 * beta * beta /
//                    (pow(beta_0, 3) + 2.0 * pow(beta, 3))
//                    - f_coulomb * n_HIM * beta * beta / (pow(xm_HIM, 3) + pow(beta, 3))));

    ///dE/dt = f_WNM * dE/dt_WNM + f_WIM * dE/dt_WIM + f_HIM * dE/dt_HIM
    ///dE/dt_WNM = dE/dt_ion; dE/dt_WIM, dE/dt_HIM = dE/dt_coulomb
    ///dp/dt = dp/DE dE/dt = sqrt(1 + (m/p)^2) dE/dt
    return exp(-log_p) * sqrt(1.0 + mass_p * mass_p * exp(-2.0 * log_p)) * ( ///u is scaled with exp(log_p)
            f_WNM * (
                    -f_ion * n_WNM * (1.0 + 0.0185 * log(beta)) * 2.0 * beta * beta /
                    (pow(beta_0, 3) + 2.0 * pow(beta, 3)))
            + f_WIM * (
                    -f_coulomb * n_WIM * beta * beta / (pow(xm_WIM, 3) + pow(beta, 3)))
            + f_HIM * (
                    -f_coulomb * n_HIM * beta * beta / (pow(xm_HIM, 3) + pow(beta, 3))));
}

NumType cubicInterpolate(NumType_I p[4], NumType_I &x) {
    return p[1] + 0.5 * x * (p[2] - p[0] + x * (2.0 * p[0] - 5.0 * p[1] + 4.0 * p[2] - p[3] +
                                                x * (3.0 * (p[1] - p[2]) + p[3] - p[0])));
}

NumType bicubicInterpolate(NumType_I p[4][4], NumType_I &x, NumType_I &y) {
    NumType arr[4];
    arr[0] = cubicInterpolate(p[0], y);
    arr[1] = cubicInterpolate(p[1], y);
    arr[2] = cubicInterpolate(p[2], y);
    arr[3] = cubicInterpolate(p[3], y);
    return cubicInterpolate(arr, x);
}


///Linear interpolation for un
NumType interpol_u(VecND_I &un, NumType_I &rp, NumType_I &zp, SizeType_I &i_log_p) {
    if (rp < 0 or rp >= un.get_size(2) or zp < 0 or zp >= un.get_size(1))
        return 0;
//    ///Linear Interpolation
//    SizeType ir = rp;
//    SizeType iz = zp;
//    NumType xr = rp - ir;
//    NumType xz = zp - iz;
//    if (ir == un.get_size(2) - 1 and iz == un.get_size(1) - 1)
//        return un[{i_log_p, iz, ir}];
//    if (ir == un.get_size(2) - 1)
//        return un[{i_log_p, iz, ir}] * (1.0 - xz) + un[{i_log_p, iz + 1, ir}] * xz;
//    if (iz == un.get_size(1) - 1)
//        return un[{i_log_p, iz, ir}] * (1.0 - xr) + un[{i_log_p, iz, ir + 1}] * xr;
//
//    return (un[{i_log_p, iz, ir}] * (1.0 - xr) + un[{i_log_p, iz, ir + 1}] * xr) * (1.0 - xz)
//           + (un[{i_log_p, iz + 1, ir}] * (1.0 - xr) + un[{i_log_p, iz + 1, ir + 1}] * xr) * xz;

    ///Bicubic Interpolation, repeat first/last element
    SizeType ir = rp;
    SizeType iz = zp;
    NumType xr = rp - ir;
    NumType xz = zp - iz;
    if (ir == un.get_size(2) - 1 and iz == un.get_size(1) - 1)
        return un[{i_log_p, iz, ir}];
    if (ir == un.get_size(2) - 1) {
        NumType p[4];
        for (int i = 0; i < 4; i++) {
            SizeType iz2 = iz + i - 1;
            if (iz == 0 and i == 0) {
                iz2 = 0;
            }
            if (iz == un.get_size(1) - 2 and i == 3) {
                iz2 = un.get_size(1) - 1;
            }
            p[i] = un[{i_log_p, iz2, ir}];
        }
        return cubicInterpolate(p, xz);
    }
    if (iz == un.get_size(1) - 1) {
        NumType p[4];
        for (int j = 0; j < 4; j++) {
            SizeType ir2 = ir + j - 1;
            if (ir == 0 and j == 0) {
                ir2 = 0;
            }
            if (ir == un.get_size(2) - 2 and j == 3) {
                ir2 = un.get_size(2) - 1;
            }
            p[j] = un[{i_log_p, iz, ir2}];
        }
        return cubicInterpolate(p, xr);
    }

    NumType p[4][4];
    for (int i = 0; i < 4; i++) {
        ///repeat first/last elements
        SizeType iz2 = iz + i - 1;
        if (iz == 0 and i == 0) {
            iz2 = 0;
        }
        if (iz == un.get_size(1) - 2 and i == 3) {
            iz2 = un.get_size(1) - 1;
        }
        for (int j = 0; j < 4; j++) {
            SizeType ir2 = ir + j - 1;
            if (ir == 0 and j == 0) {
                ir2 = 0;
            }
            if (ir == un.get_size(2) - 2 and j == 3) {
                ir2 = un.get_size(2) - 1;
            }
            p[i][j] = un[{i_log_p, iz2, ir2}];
        }
    }

    return bicubicInterpolate(p, xz, xr);
}

void
CRDiff::interpolate_u(VecND &u_out, VecND_I &u_in, VecNum_I &r_in, VecNum_I &z_in, VecNum_I &r_out, VecNum_I &z_out) {
    ///Linear interpolation
    NumType dr = r_in[1] - r_in[0];
    NumType dz = z_in[1] - z_in[0];
    VecNum rp(r_out.size());
    VecNum zp(z_out.size());
    for (SizeType i = 0; i < rp.size(); i++) {
        rp[i] = abs((r_out[i] - r_in[0]) / dr); ///r[-i] = r[i]
    }
    for (SizeType i = 0; i < zp.size(); i++) {
        zp[i] = (z_out[i] - z_in[0]) / dz;
    }
    IndexType index = {0, 0, 0};
#pragma omp parallel for default(none) shared(u_out, u_in, rp, zp) firstprivate(index)
    for (SizeType i_log_p = 0; i_log_p < u_out.get_size(0); i_log_p++) {
        index[0] = i_log_p;
        for (SizeType i_z = 0; i_z < u_out.get_size(1); i_z++) {
            index[1] = i_z;
            for (SizeType i_r = 0; i_r < u_out.get_size(2); i_r++) {
                index[2] = i_r;
                ///Interpolate u
                u_out[index] = interpol_u(u_in, rp[i_r], zp[i_z], i_log_p);
            }
        }
    }

//    ///makes no sense, because negative values of z
//    ///Logarithmic r, z variables
//    VecNum log_r_in = vec_apply(r_in, log);
//    vec_print(log_r_in);
}

VecNum CRDiff::interpolate_u_r(VecNum_I &u_in, VecNum_I &r_in, VecNum_I &r_out) {
    VecNum u_out(r_out.size());
    NumType dr = r_in[1] - r_in[0];
    for (SizeType i = 0; i < r_out.size(); i++) {
        NumType rp = abs((r_out[i] - r_in[0]) / dr); ///r[-i] = r[i]
        ///Linear Interpolation
        SizeType ir = rp;
        NumType xr = rp - ir;
        if (rp >= r_in.size()) {
            u_out[i] = 0; ///Out of Bounds
        } else if (ir == r_in.size() - 1) {
            u_out[i] = u_in[ir];
        } else {
//            u_out[i] = u_in[ir] * (1.0 - xr) + u_in[ir + 1] * xr;
            ///Cubic Interpolation
            NumType p[4];
            for (int j = 0; j < 4; j++) {
                SizeType ir2 = ir + j - 1;
                if (ir == 0 and j == 0) {
                    ir2 = 0;
                }
                if (ir == u_in.size() - 2 and j == 3) {
                    ir2 = u_in.size() - 1;
                }
                p[j] = u_in[ir2];
            }
            u_out[i] = cubicInterpolate(p, xr);
        }
    }
    return u_out;
}

void CRDiff::diffusion_step_r_z(VecND &un, NumType_I &dr, NumType_I &dz, NumType_I &dt, NumType_I &delta, VecNum_I &r,
                                VecNum_I &log_p) {
    IndexType index = {0, 0, 0};
#pragma omp parallel for default(none) shared(un, r, log_p, dr, dz, dt, delta) firstprivate(index)
    for (SizeType i_log_p = 0; i_log_p < un.get_size(0); i_log_p++) {
        index[0] = i_log_p;
        NumType kappa_2 = exp(delta * log_p[i_log_p]) / 2.0; ///kappa/2, because needed in 1/2 for crank_nicolson

        ///Step in z
        NumType alpha_z = kappa_2 * dt / (dz * dz); ///For implicit method
        VecNum b_z(un.get_size(1), 1 + 2 * alpha_z);
        VecNum a_z(un.get_size(1), -alpha_z);
        VecNum c_z(un.get_size(1), -alpha_z);
        c_z.front() = 0; ///constant boundary start
        b_z.front() = 1;
        a_z.back() = 0; ///constant boundary end
        b_z.back() = 1;
        for (SizeType i_r = 0; i_r < un.get_size(2); i_r++) {
            index[2] = i_r;
            NDNumerics::diffusion_ftcs_index(un, 1, index, dz, dt, kappa_2, 0);
            NDNumerics::diffusion_implicit_index(un, 1, index, a_z, b_z, c_z);
        }

        ///Step in r
        NumType alpha_r = kappa_2 * dt / (dr * dr); ///For implicit method
        VecNum b_r(un.get_size(2), 1 + 2 * alpha_r);
        VecNum a_r(un.get_size(2), -alpha_r);
        VecNum c_r(un.get_size(2), -alpha_r);
        c_r.front() = -2 * alpha_r; ///boundary start
        a_r.back() = 0; ///constant boundary end
        b_r.back() = 1;
        ///du/dt = kappa/r du/dr part
        auto kappa_r_func = [&kappa_2](NumType_I &x) {
            return -kappa_2 * 2.0 / x;
        };
        VecNum kappa_r = vec_apply(r, kappa_r_func);
        for (SizeType i_z = 0; i_z < un.get_size(1); i_z++) {
            index[1] = i_z;
            NDNumerics::diffusion_ftcs_index(un, 2, index, dr, dt, kappa_2, 1);
            NDNumerics::diffusion_implicit_index(un, 2, index, a_r, b_r, c_r);
//            NDNumerics::diffusion_lax_wendroff_index(un, 2, index, dr, dt, kappa_2 * 2., r);
            NDNumerics::semi_lagrangian_index(un, 2, index, dr, dt, kappa_r);
        }
    }
}

void CRDiff::diffusion_step_r_z_2(VecND &un, NumType_I &dr, NumType_I &dz, NumType_I &dt, NumType_I &delta, VecNum_I &r,
                                  VecNum_I &log_p) {
    IndexType index = {0, 0, 0};
#pragma omp parallel for default(none) shared(un, r, log_p, dr, dz, dt, delta) firstprivate(index)
    for (SizeType i_log_p = 0; i_log_p < un.get_size(0); i_log_p++) {
        index[0] = i_log_p;
        ///beta = (p/m) / sqrt(1 + (p/m)^2)
        NumType beta = (exp(log_p[i_log_p]) / mass_p) /
                       sqrt(1.0 + exp(2.0 * log_p[i_log_p]) / (mass_p * mass_p)); ///<beta = v/c
        ///kappa = beta * (1 + p/m)^delta
        NumType kappa_2 = beta * pow(1 + exp(log_p[i_log_p]) / mass_p, delta) / 2.0; ///<kappa/2

        ///Step in z
        NumType alpha_z = kappa_2 * dt / (dz * dz); ///For implicit method
        VecNum b_z(un.get_size(1), 1 + 2 * alpha_z);
        VecNum a_z(un.get_size(1), -alpha_z);
        VecNum c_z(un.get_size(1), -alpha_z);
        c_z.front() = 0; ///constant boundary start
        b_z.front() = 1;
        a_z.back() = 0; ///constant boundary end
        b_z.back() = 1;
        for (SizeType i_r = 0; i_r < un.get_size(2); i_r++) {
            index[2] = i_r;
            NDNumerics::diffusion_ftcs_index(un, 1, index, dz, dt, kappa_2, 0);
            NDNumerics::diffusion_implicit_index(un, 1, index, a_z, b_z, c_z);
        }

        ///Step in r
        NumType alpha_r = kappa_2 * dt / (dr * dr); ///For implicit method
        VecNum b_r(un.get_size(2), 1 + 2 * alpha_r);
        VecNum a_r(un.get_size(2), -alpha_r);
        VecNum c_r(un.get_size(2), -alpha_r);
        c_r.front() = -2 * alpha_r; ///boundary start
        a_r.back() = 0; ///constant boundary end
        b_r.back() = 1;
        ///du/dt = kappa/r du/dr part
        auto kappa_r_func = [&kappa_2](NumType_I &x) {
            return -kappa_2 * 2.0 / x;
        };
        VecNum kappa_r = vec_apply(r, kappa_r_func);
        for (SizeType i_z = 0; i_z < un.get_size(1); i_z++) {
            index[1] = i_z;
            NDNumerics::diffusion_ftcs_index(un, 2, index, dr, dt, kappa_2, 1);
            NDNumerics::diffusion_implicit_index(un, 2, index, a_r, b_r, c_r);
//            NDNumerics::diffusion_lax_wendroff_index(un, 2, index, dr, dt, kappa_2 * 2., r);
            NDNumerics::semi_lagrangian_index(un, 2, index, dr, dt, kappa_r);
        }
    }
}

void CRDiff::calculate_step_p(VecND &un, NumType_I &dlog_p, NumType_I &dt, NumType_I &power_n, VecNum_I &log_p,
                              SizeType_I &start_z, SizeType_I &end_z) {
    ///Step in p

    ///Velocity is independent of r,z,phi
    ///v = -exp((n-1)*log_p)
    auto velocity = [&power_n](NumType_I &x) {
        return -exp((power_n - 1) * x);
    };
    VecNum v = vec_apply(log_p, velocity);

    IndexType index = {0, 0, 0};
#pragma omp parallel for default(none) shared(un, log_p, v, dlog_p, dt, start_z, end_z) firstprivate(index)
    for (SizeType i_z = start_z; i_z <= end_z; i_z++) { ///Energy loss only in disk
        index[1] = i_z;
        for (SizeType i_r = 0; i_r < un.get_size(2); i_r++) {
            index[2] = i_r;
//            NDNumerics::semi_lagrangian_2_index(un, 0, index, dlog_p, dt, v, 2);
            NDNumerics::mpdata_index(un, 0, index, dlog_p, dt, v);
        }
    }
}

void CRDiff::calculate_step_p_2(VecND &un, NumType_I &dlog_p, NumType_I &dt, VecNum_I &log_p,
                                SizeType_I &start_z, SizeType_I &end_z) {
    VecNum v = vec_apply(log_p, energy_loss_rate);

    IndexType index = {0, 0, 0};
#pragma omp parallel for default(none) shared(un, log_p, v, dlog_p, dt, start_z, end_z) firstprivate(index)
    for (SizeType i_z = start_z; i_z <= end_z; i_z++) { ///Energy loss only in disk
        index[1] = i_z;
        for (SizeType i_r = 0; i_r < un.get_size(2); i_r++) {
            index[2] = i_r;
//            NDNumerics::semi_lagrangian_2_index(un, 0, index, dlog_p, dt, v, 2);
            NDNumerics::mpdata_index(un, 0, index, dlog_p, dt, v);
        }
    }
}


void CRDiff::calculate_u0(VecND &un, NumType_I &gamma, VecNum_I &r, VecNum_I &z, VecNum_I &log_p, NumType_I &epsilon) {
    ///injection = dirac_delta(x) p^-gamma
    ///Replace driac_delta(x) with 1 / sqrt(2 pi epsilon) dirac_delta(r) dirac_delta(z)
    ///with epsilon = 2 * dt * offset in comparison with solution to TwoDimDiff equation
    function<NumType(VecNum_I &)> f0 = [&gamma, &epsilon](VecNum_I &val) {
        return 1.0 / sqrt(epsilon * 2.0 * M_PI)
               * dirac_delta(val[1], epsilon) * dirac_delta(val[2], epsilon) ///dirac_delta(x)
               * exp((-gamma + 1.0) * val[0]); ///Injection Spectrum and scale function with exp(log_p)
    };
    un.apply({log_p, z, r}, f0);

    ///Boundary in z
    auto set_boundaries_z = [](VecND &u, SizeType_I &subvector_index, IndexType_I &index) {
        u.at(0, subvector_index, index) = 0;
        u.at(-1, subvector_index, index) = 0;
    };
    un.apply_subvector(set_boundaries_z, 1);
    ///Boundary in r at end
    auto set_boundaries_r = [](VecND &u, SizeType_I &subvector_index, IndexType_I &index) {
        u.at(-1, subvector_index, index) = 0;
    };
    un.apply_subvector(set_boundaries_r, 2);
}

void CRDiff::calculate_steps(VecND &u, NumType_I &dr, NumType_I &dz, NumType_I &dlog_p, NumType_I &dt, const int &steps,
                             NumType_I &delta, NumType_I &power_n, VecNum_I &r, VecNum_I &log_p,
                             SizeType_I &start_z, SizeType_I &end_z) {
    for (int i = 0; i < steps; i++) {
        diffusion_step_r_z(u, dr, dz, dt, delta, r, log_p);
        calculate_step_p(u, dlog_p, dt, power_n, log_p, start_z, end_z);
    }
}

void
CRDiff::calculate_steps_2(VecND &u, NumType_I &dr, NumType_I &dz, NumType_I &dlog_p, NumType_I &dt, const int &steps,
                          NumType_I &delta, [[gnu::unused]] NumType_I &power_n, VecNum_I &r, VecNum_I &log_p,
                          SizeType_I &start_z, SizeType_I &end_z) {
    for (int i = 0; i < steps; i++) {
        diffusion_step_r_z_2(u, dr, dz, dt, delta, r, log_p);
        calculate_step_p_2(u, dlog_p, dt, log_p, start_z, end_z);
//        calculate_step_p(u, dlog_p, dt, power_n, log_p, start_z, end_z);
    }
}

void CRDiff::plot(NumType_I &dr, NumType_I &dz, NumType_I &dlog_p, NumType_I &dt, const int &steps, const int &offset,
                  NumType_I &length_r, NumType_I &length_z, NumType_I &log_p_start, NumType_I &log_p_end, NumType_I &h,
                  NumType_I &delta, NumType_I &power_n, NumType_I &gamma, const std::string &file_appendix) {
    VecNum r = arange(0, length_r, dr);
    VecNum z = arange(-length_z, length_z, dz);
    VecNum log_p = arange(log_p_start, log_p_end, dlog_p);

    VecND u;
    calculate_u0(u, gamma, r, z, log_p, 2.0 * dt * offset);

    ///Start-/End-index of disk in z
    SizeType start_z, end_z;
    if (h >= length_z) {
        start_z = 0;
        end_z = z.size() - 1;
    } else {
        SizeType i_z0 = z.size() / 2;
        SizeType i_h = h / dz;
        start_z = i_z0 - i_h;
        end_z = i_z0 + i_h;
    }

#if USE_CALCULATE_STEPS == 1
    calculate_steps(u, dr, dz, dlog_p, dt, steps, delta, power_n, r, log_p, start_z, end_z);
#elif USE_CALCULATE_STEPS == 2
    calculate_steps_2(u, dr, dz, dlog_p, dt, steps, delta, power_n, r, log_p, start_z, end_z);
#endif

    ///Test interpolating u
//    VecNum r_out = arange(0, R_2, DR_2);
//    VecNum z_out = arange(-L_2, L_2, DZ_2);
//    VecND u_out(3, {log_p.size(), z_out.size(), r_out.size()});
//    interpolate_u(u_out, u, r, z, r_out, z_out);
//    r = r_out;
//    z = z_out;
//    u = u_out;

    ///Save as .hdf5
    hid_t file_id, dataset_id;
    string file_name = "CRDiff" + file_appendix + ".hdf5";
    file_id = H5Fcreate(file_name.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

    save_in_hdf5(log_p, dataset_id, file_id, "log_p");
    H5Dclose(dataset_id);
    save_in_hdf5(z, dataset_id, file_id, "z");
    H5Dclose(dataset_id);
    save_in_hdf5(r, dataset_id, file_id, "r");
    H5Dclose(dataset_id);
    save_in_hdf5(u, dataset_id, file_id, "u");
    H5Dclose(dataset_id);

    H5Fclose(file_id);
}

void CRDiff::test(NumType_I &dr, NumType_I &dz, NumType_I &dlog_p, NumType_I &dt, const int &steps, const int &offset,
                  NumType_I &length_r, NumType_I &length_z, NumType_I &log_p_start, NumType_I &log_p_end,
                  NumType_I &delta, NumType_I &gamma, const std::string &file_appendix) {
    VecNum r = arange(0, length_r, dr);
    VecNum z = arange(-length_z, length_z, dz);
    VecNum log_p = arange(log_p_start, log_p_end, dlog_p);


    VecND u;
    function<NumType(VecNum_I &)> f0 = [&dt, &offset, &delta, &gamma](VecNum_I &val) {
        return threedim::psi_theory(val[2], val[1], val[0], dt * offset, delta, gamma);
    };
    u.apply({log_p, z, r}, f0);
    ///Boundaries
    auto set_boundaries_z = [](VecND &un, SizeType_I &subvector_index, IndexType_I &index) {
        un.at(0, subvector_index, index) = 0;
        un.at(-1, subvector_index, index) = 0;
    };
    u.apply_subvector(set_boundaries_z, 1);
    auto set_boundaries_r = [](VecND &un, SizeType_I &subvector_index, IndexType_I &index) {
        un.at(-1, subvector_index, index) = 0;
    };
    u.apply_subvector(set_boundaries_r, 2);

    VecND ut;
    function<NumType(VecNum_I &)> ft = [&dt, &steps, &offset, &delta, &gamma](VecNum_I &val) {
        return threedim::psi_theory(val[2], val[1], val[0], dt * (steps + offset), delta, gamma);
    };
    ut.apply({log_p, z, r}, ft);

    calculate_steps(u, dr, dz, dlog_p, dt, steps, delta, 2, r, log_p, 0, u.get_size(1) - 1);

    ///Save as .hdf5
    hid_t file_id, dataset_id;
    string file_name = "CRDiffTest" + file_appendix + ".hdf5";
    file_id = H5Fcreate(file_name.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

    save_in_hdf5(log_p, dataset_id, file_id, "log_p");
    H5Dclose(dataset_id);
    save_in_hdf5(z, dataset_id, file_id, "z");
    H5Dclose(dataset_id);
    save_in_hdf5(r, dataset_id, file_id, "r");
    H5Dclose(dataset_id);
    save_in_hdf5(u, dataset_id, file_id, "u");
    H5Dclose(dataset_id);
    save_in_hdf5(ut, dataset_id, file_id, "u_theory");
    H5Dclose(dataset_id);

    H5Fclose(file_id);
}

void CRDiff::plot_energy_loss_rate(NumType_I &dlog_p, NumType_I &log_p_start, NumType_I &log_p_end, NumType_I &dt,
                                   const std::string &file_appendix) {
    VecNum log_p = arange(log_p_start, log_p_end, dlog_p);
    VecNum v = vec_apply(log_p, energy_loss_rate);

    ///Give out the energy loss rate, so the step size can be fit to the maximum energy loss rate.
    cout << "energy loss rate:" << endl;
    NumType v_max = vec_max(vec_apply(v, abs));
    cout << " maximum energy loss: " << v_max << endl;
    if (dt > 0) {
        NumType alpha = v_max * dt / dlog_p;
        cout << " alpha = max(abs(v)) * dt / dx = " << alpha << endl;
        if (alpha <= 1) {
            cout << " mpdata is stable with this parameters." << endl;
        } else {
            cout << " mpdata is not stable with this parameters." << endl;
        }
        cout << " minimum required dt = " << dlog_p / v_max << " = " << dlog_p / v_max * TAU << " Myr" << endl;
    }
    save_as_csv(log_p, v, "CRDiff_energy_loss" + file_appendix + ".csv",
                "log_p", "energy loss rate", "energy loss rate");
    NumType ekin = 1.0; ///Ekin in MeV
    NumType p = sqrt((ekin + MASS_P) * (ekin + MASS_P) - MASS_P * MASS_P) / P0; ///p at Ekin
    cout << " energy loss at Ekin = " << ekin << " MeV: p = " << p << ", -dE/dt(Ekin=" << ekin << "MeV) = "
         << -energy_loss_rate(log(p)) * exp(log(p)) / sqrt(1.0 + (mass_p / p) * (mass_p / p)) * B0
         << " MeV/Myr" << endl;
    cout << " energy loss at p0 = " << P0 << " MeV/c: -dp/dt(p0) = b0 = "
         << -energy_loss_rate(log(P0)) * exp(log(P0)) * B0
         << " MeV/c/Myr" << endl;
}

void
CRDiff::plot_diffusion_coefficient(NumType_I &dlog_p, NumType_I &log_p_start, NumType_I &log_p_end, NumType_I &delta,
                                   const std::string &file_appendix) {
    VecNum log_p = arange(log_p_start, log_p_end, dlog_p);
    VecNum kappa(log_p.size());
    for (SizeType i = 0; i < log_p.size(); i++) {
        ///beta = (p/m) / sqrt(1 + (p/m)^2)
        NumType beta = (exp(log_p[i]) / mass_p) /
                       sqrt(1.0 + exp(2.0 * log_p[i]) / (mass_p * mass_p)); ///<beta = v/c
        ///kappa = beta * (1 + p/m)^delta
        kappa[i] = beta * pow(1 + exp(log_p[i]) / mass_p, delta); ///<kappa
//        kappa[i] = exp(delta * log_p[i]); ///<power law kappa
    }

    save_as_csv(log_p, kappa, "CRDiff_diffusion_coefficient" + file_appendix + ".csv",
                "log_p", "$\\kappa / \\kappa_0$", "Diffusion coefficient");
    NumType ekin = 1.0; ///Ekin in MeV
    NumType p = sqrt((ekin + MASS_P) * (ekin + MASS_P) - MASS_P * MASS_P) / P0; ///p at Ekin
    NumType beta = (p / mass_p) / sqrt(1.0 + p * p / (mass_p * mass_p)); ///<beta = v/c
    cout << "Diffusion coefficient at Ekin = " << ekin << " MeV: p = " << p << ", kappa(Ekin=" << ekin << "MeV) = "
         << beta * pow(1 + p / mass_p, delta) * KAPPA0
         << " kpc^2/Myr" << endl;
}

void CRDiff::generate_template(NumType_I &dr, NumType_I &dz, NumType_I &dlog_p, NumType_I &dt, const int &offset,
                               NumType_I &length_r, NumType_I &length_z, NumType_I &log_p_start, NumType_I &log_p_end,
                               NumType_I &dlog_t, NumType_I &log_t_start, NumType_I &log_t_end,
                               NumType_I &h, NumType_I &delta, NumType_I &power_n, NumType_I &gamma,
                               const std::string &file_appendix) {
    VecNum r = arange(0, length_r, dr);
    VecNum z = arange(-length_z, length_z, dz);
    VecNum log_p = arange(log_p_start, log_p_end, dlog_p);
    VecNum log_t = arange(log_t_start, log_t_end, dlog_t);
    VecNum t(log_t.size());

    VecND u;
    calculate_u0(u, gamma, r, z, log_p, 2.0 * dt * offset);

    ///Start-/End-index of disk in z
    SizeType index_z0 = z.size() / 2;
    SizeType start_z, end_z;
    if (h >= length_z) {
        start_z = 0;
        end_z = z.size() - 1;
    } else {
        SizeType i_z0 = z.size() / 2;
        SizeType i_h = h / dz;
        start_z = i_z0 - i_h;
        end_z = i_z0 + i_h;
    }

    ///Open .hdf5 file
    hid_t file_id, dataset_id;
    string file_name = "greens_template" + file_appendix + ".hdf5";
    file_id = H5Fcreate(file_name.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

    ///Save log_p and r
    save_in_hdf5(log_p, dataset_id, file_id, "log_p");
    H5Dclose(dataset_id);
    save_in_hdf5(r, dataset_id, file_id, "r");
    H5Dclose(dataset_id);

    ///Prepare dataspace for t
    hsize_t dims_t[1] = {0};
    hsize_t max_dims_t[1] = {H5S_UNLIMITED};
    hsize_t chunk_dims_t[1] = {1000};
    hid_t prop_t = H5Pcreate(H5P_DATASET_CREATE);
    H5Pset_chunk(prop_t, 1, chunk_dims_t);
    hid_t dataspace_t = H5Screate_simple(1, dims_t, max_dims_t);
    hid_t dataset_t = H5Dcreate2(file_id, "t", H5T_IEEE_F64BE, dataspace_t, H5P_DEFAULT, prop_t, H5P_DEFAULT);
    H5Pclose(prop_t);
    H5Sclose(dataspace_t);

    hsize_t offset_t[1] = {0};
    hsize_t count_t[1] = {1};
    hid_t memspace_t = H5Screate_simple(1, count_t, nullptr);

    ///Prepare dataspace for u
    hsize_t dims[3] = {0, log_p.size(), r.size()};
    hsize_t max_dims[3] = {H5S_UNLIMITED, log_p.size(), r.size()};
    hsize_t chunk_dims[3] = {1, log_p.size(), r.size()};
    hid_t prop_id = H5Pcreate(H5P_DATASET_CREATE);
    H5Pset_chunk(prop_id, 3, chunk_dims);
    hid_t dataspace_id = H5Screate_simple(3, dims, max_dims);
    dataset_id = H5Dcreate2(file_id, "u", H5T_IEEE_F64BE, dataspace_id, H5P_DEFAULT, prop_id, H5P_DEFAULT);
    H5Pclose(prop_id);
    H5Sclose(dataspace_id);

    hsize_t offset_u[3] = {0, 0, 0};
    hsize_t count_u[3] = {1, 1, r.size()};
    hid_t memspace_u = H5Screate_simple(3, count_u, nullptr);

    ///Close file
    H5Dclose(dataset_t);
    H5Dclose(dataset_id);
    H5Fclose(file_id);

    ///Steps
    SizeType steps = 0;
    NumType last_t = offset * dt;
    SizeType max_steps = round((pow(10, log_t_end) - last_t) / dt);
    SizeType calculated_steps = 0;
    cout << "Start calculation of " << file_name << ". Time steps to calculate: " << max_steps << endl;
    for (SizeType i = 0; i < log_t.size(); i++) {
        ///Calculate time
        steps = round((pow(10, log_t[i]) - last_t) / dt);
        t[i] = dt * steps + last_t;
        last_t = t[i];

        calculate_steps(u, dr, dz, dlog_p, dt, steps, delta, power_n, r, log_p, start_z, end_z);

        calculated_steps += steps;
        cout << "Calculated steps: " << calculated_steps << "/" << max_steps << " (last: " << steps << ")" << endl;

        ///Open file
        file_id = H5Fopen(file_name.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
        ///Extend and write t
        dims_t[0] += 1;
        dataset_t = H5Dopen2(file_id, "t", H5P_DEFAULT);
        H5Dset_extent(dataset_t, dims_t);
        dataspace_t = H5Dget_space(dataset_t);
        offset_t[0] = i;
        H5Sselect_hyperslab(dataspace_t, H5S_SELECT_SET, offset_t, nullptr, count_t, nullptr);
        H5Dwrite(dataset_t, H5T_NATIVE_DOUBLE, memspace_t, dataspace_t, H5P_DEFAULT, t.data() + i);
        H5Sclose(dataspace_t);
        ///Extend hdf5 file and write u(z=0)
        dims[0] += 1;
        dataset_id = H5Dopen2(file_id, "u", H5P_DEFAULT);
        H5Dset_extent(dataset_id, dims);
        dataspace_id = H5Dget_space(dataset_id);
        IndexType index = {0, index_z0, 0};
        offset_u[0] = i;
        for (SizeType i_logp = 0; i_logp < u.get_size(0); i_logp++) {
            index[0] = i_logp;
            offset_u[1] = i_logp;
            H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset_u, nullptr, count_u, nullptr);
            ///Scale back with exp(-log_p)
            VecNum sub_vec = u.subvector(2, index);
            NumType log_p_i = log_p[i_logp];
            auto func_scaleback = [&log_p_i](NumType_I &un_i) {
                return exp(-log_p_i) * un_i;
            };
            sub_vec = vec_apply(sub_vec, func_scaleback);
            H5Dwrite(dataset_id, H5T_NATIVE_DOUBLE, memspace_u, dataspace_id, H5P_DEFAULT,
                     sub_vec.data());
        }
        H5Sclose(dataspace_id);
        ///Close file
        H5Dclose(dataset_t);
        H5Dclose(dataset_id);
        H5Fclose(file_id);
    }
    H5Sclose(memspace_t);
    H5Sclose(memspace_u);
}

void CRDiff::generate_template_2(VecNum_I &vec_dr, VecNum_I &vec_dz, VecNum_I &vec_dt,
                                 VecNum_I &vec_length_r, VecNum_I &vec_length_z,
                                 NumType_I &dlog_t, VecNum_I &vec_log_t_start, VecNum_I &vec_log_t_end,
                                 const int &offset, NumType_I &dlog_p, NumType_I &log_p_start, NumType_I &log_p_end,
                                 NumType_I &dr_out, NumType_I &length_r_out,
                                 NumType_I &h, NumType_I &delta, NumType_I &power_n, NumType_I &gamma,
                                 const std::string &file_appendix) {
    NumType dr, dz, dt, length_r, length_z, log_t_start, log_t_end;
    SizeType index_z0, start_z, end_z; ///<Start-/End-index of disk in z
    double timer_start, timer_now, timer_last;
    timer_now = timer_start = omp_get_wtime();

    VecNum r, z, log_t, t;
    VecNum log_p = arange(log_p_start, log_p_end, dlog_p);
    SizeType index_t = 0;
    NumType last_t = 0;

    VecNum r_out = arange(0, length_r_out, dr_out);

    VecND u;

    ///Open .hdf5 file
    hid_t file_id, dataset_id;
    string file_name = "greens_template" + file_appendix + ".hdf5";
    file_id = H5Fcreate(file_name.c_str(), H5F_ACC_TRUNC, H5P_DEFAULT, H5P_DEFAULT);

    ///Save log_p and r
    save_in_hdf5(log_p, dataset_id, file_id, "log_p");
    H5Dclose(dataset_id);
    save_in_hdf5(r_out, dataset_id, file_id, "r");
    H5Dclose(dataset_id);

    ///Prepare dataspace for t
    hsize_t dims_t[1] = {0};
    hsize_t max_dims_t[1] = {H5S_UNLIMITED};
    hsize_t chunk_dims_t[1] = {1000};
    hid_t prop_t = H5Pcreate(H5P_DATASET_CREATE);
    H5Pset_chunk(prop_t, 1, chunk_dims_t);
    hid_t dataspace_t = H5Screate_simple(1, dims_t, max_dims_t);
    hid_t dataset_t = H5Dcreate2(file_id, "t", H5T_IEEE_F64BE, dataspace_t, H5P_DEFAULT, prop_t, H5P_DEFAULT);
    H5Pclose(prop_t);
    H5Sclose(dataspace_t);

    hsize_t offset_t[1] = {0};
    hsize_t count_t[1] = {1};
    hid_t memspace_t = H5Screate_simple(1, count_t, nullptr);

    ///Prepare dataspace for u
    hsize_t dims[3] = {0, log_p.size(), r_out.size()};
    hsize_t max_dims[3] = {H5S_UNLIMITED, log_p.size(), r_out.size()};
    hsize_t chunk_dims[3] = {1, log_p.size(), r_out.size()};
    hid_t prop_id = H5Pcreate(H5P_DATASET_CREATE);
    H5Pset_chunk(prop_id, 3, chunk_dims);
    hid_t dataspace_id = H5Screate_simple(3, dims, max_dims);
    dataset_id = H5Dcreate2(file_id, "u", H5T_IEEE_F64BE, dataspace_id, H5P_DEFAULT, prop_id, H5P_DEFAULT);
    H5Pclose(prop_id);
    H5Sclose(dataspace_id);

    hsize_t offset_u[3] = {0, 0, 0};
    hsize_t count_u[3] = {1, 1, r_out.size()};
    hid_t memspace_u = H5Screate_simple(3, count_u, nullptr);

    ///Close file
    H5Dclose(dataset_t);
    H5Dclose(dataset_id);
    H5Fclose(file_id);

    ///different t parts
    for (SizeType index_part = 0; index_part < vec_dr.size(); index_part++) {
        dr = vec_dr[index_part];
        dz = vec_dz[index_part];
        dt = vec_dt[index_part];
        length_r = vec_length_r[index_part];
        length_z = vec_length_z[index_part];
        log_t_start = vec_log_t_start[index_part];
        log_t_end = vec_log_t_end[index_part];

        log_t = arange(log_t_start, log_t_end, dlog_t);
        t.reserve(t.size() + log_t.size());

        if (index_part == 0) {
            r = arange(0, length_r, dr);
            z = arange(-length_z, length_z, dz);
            calculate_u0(u, gamma, r, z, log_p, 2.0 * dt * offset);
            last_t = offset * dt;
        } else {
            VecNum r_new = arange(0, length_r, dr);
            VecNum z_new = arange(-length_z, length_z, dz);
            VecND u_new(3, {log_p.size(), z_new.size(), r_new.size()});
            interpolate_u(u_new, u, r, z, r_new, z_new);
            r = r_new;
            z = z_new;
            u = u_new;
        }

        ///Start-/End-index of disk in z
        index_z0 = z.size() / 2;
        if (h >= length_z) {
            start_z = 0;
            end_z = z.size() - 1;
        } else {
            SizeType i_z0 = z.size() / 2;
            SizeType i_h = h / dz;
            start_z = i_z0 - i_h;
            end_z = i_z0 + i_h;
        }

        ///Steps
        SizeType steps = 0;
        SizeType max_steps = round((pow(10, log_t_end) - last_t) / dt);
        SizeType calculated_steps = 0;
        cout << "Start calculation part " << index_part + 1 << "/" << vec_dr.size() << " of " << file_name
             << ". Time steps to calculate: " << max_steps << endl;
        timer_last = omp_get_wtime();
        for (SizeType i = 0; i < log_t.size(); i++) {
            ///Calculate time
            steps = round((pow(10, log_t[i]) - last_t) / dt);
            if (steps == 0 and not(i == 0 and index_part == 0))
                continue;
            t.push_back(dt * steps + last_t);
            index_t = t.size() - 1;
            last_t = t.back();

#if USE_CALCULATE_STEPS == 1
            calculate_steps(u, dr, dz, dlog_p, dt, steps, delta, power_n, r, log_p, start_z, end_z);
#elif USE_CALCULATE_STEPS == 2
            calculate_steps_2(u, dr, dz, dlog_p, dt, steps, delta, power_n, r, log_p, start_z, end_z); ///Real CR diff
#endif

            calculated_steps += steps;

            ///Open file
            file_id = H5Fopen(file_name.c_str(), H5F_ACC_RDWR, H5P_DEFAULT);
            ///Extend and write t
            dims_t[0] += 1;
            dataset_t = H5Dopen2(file_id, "t", H5P_DEFAULT);
            H5Dset_extent(dataset_t, dims_t);
            dataspace_t = H5Dget_space(dataset_t);
            offset_t[0] = index_t;
            H5Sselect_hyperslab(dataspace_t, H5S_SELECT_SET, offset_t, nullptr, count_t, nullptr);
            H5Dwrite(dataset_t, H5T_NATIVE_DOUBLE, memspace_t, dataspace_t, H5P_DEFAULT, t.data() + index_t);
            H5Sclose(dataspace_t);
            ///Extend hdf5 file and write u(z=0)
            dims[0] += 1;
            dataset_id = H5Dopen2(file_id, "u", H5P_DEFAULT);
            H5Dset_extent(dataset_id, dims);
            dataspace_id = H5Dget_space(dataset_id);
            offset_u[0] = index_t;
            IndexType index = {0, index_z0, 0};
            for (SizeType i_logp = 0; i_logp < log_p.size(); i_logp++) {
                index[0] = i_logp;
                offset_u[1] = i_logp;
                H5Sselect_hyperslab(dataspace_id, H5S_SELECT_SET, offset_u, nullptr, count_u, nullptr);
                VecNum u_out = interpolate_u_r(u.subvector(2, index), r, r_out); ///Interpolate u
                ///Scale back with exp(-log_p)
                NumType log_p_i = log_p[i_logp];
                auto func_scaleback = [&log_p_i](NumType_I &un_i) {
                    return exp(-log_p_i) * un_i;
                };
                u_out = vec_apply(u_out, func_scaleback);
                H5Dwrite(dataset_id, H5T_NATIVE_DOUBLE, memspace_u, dataspace_id, H5P_DEFAULT,
                         u_out.data());
            }
            H5Sclose(dataspace_id);
            ///Close file
            H5Dclose(dataset_t);
            H5Dclose(dataset_id);
            H5Fclose(file_id);

            timer_now = omp_get_wtime();
            cout << "Calculated steps: " << calculated_steps << "/" << max_steps
                 << " (last: " << steps << ", " << timer_now - timer_last
                 << "s, total: " << timer_now - timer_start << "s) \r" << flush;
            timer_last = timer_now;
        }
        cout << " \r " << flush;
        cout << "Finished calculation part " << index_part + 1 << " after " << timer_now - timer_start << " seconds."
             << endl;
    }
    H5Sclose(memspace_t);
    H5Sclose(memspace_u);
    timer_now = omp_get_wtime();
    cout << "Finished calculation of " << file_name << " in " << timer_now - timer_start << " seconds." << endl;
}

void CRDiff::generate_templates() {
    generate_template_2({DR_1, DR_2, DR_3, DR_4, DR_5}, {DZ_1, DZ_2, DZ_3, DZ_4, DZ_5}, {DT_1, DT_2, DT_3, DT_4, DT_5},
                        {R_1, R_2, R_3, R_4, R_5}, {L_1, L_2, L_3, L_4, L_5}, DLOG_T,
                        {LOG_T_START_1, LOG_T_START_2, LOG_T_START_3, LOG_T_START_4, LOG_T_START_5},
                        {LOG_T_END_1, LOG_T_END_2, LOG_T_END_3, LOG_T_END_4, LOG_T_END_5},
                        OFFSET_1, DLOG_P, LOG_P_START, LOG_P_END, DR_OUT, R_OUT,
                        H, DELTA, POWER_N, GAMMA, "");
}
