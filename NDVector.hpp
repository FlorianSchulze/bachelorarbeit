//
// Created by flosc on 08.05.2020.
//

#ifndef PROGRAMM_NDVECTOR_HPP
#define PROGRAMM_NDVECTOR_HPP
#include "own_error.hpp"
#include <vector>
#include <initializer_list>
#include <functional>

/**
 * An N dimensional vector of type T.
 * NDVector(3, {Nx, Ny, Nz}) creates an 3 dimensional vector of size {Nx, Ny, Nz}.
 * NDVector(std::vector) or NDVector = std::vector or NDVector = {x1, x2, ..} or NDVector({x1, x2, ..})
 * creates an one dimensional vector with the values of std::vector.
 * The elements of the vector can be accessed via NDVector[{x, y, z}] or NDVector[i].
 * @tparam T type of the elements in the vector
 */
template <class T>
class NDVector {
    ///Some type definitions for more readable and changeable code, _I for constant input
    typedef std::vector<T> VecType;
    typedef const std::vector<T> VecType_I;
    typedef std::size_t SizeType;
    typedef const std::size_t SizeType_I;
    typedef std::vector<SizeType> IndexType;
    typedef const std::vector<SizeType> IndexType_I;

    ///The dimension of the vector
    SizeType dimension{};
    ///The size of the vector in every dimension
    IndexType size;
    ///The values of the vector in one big vector. NDVector[{i, j, k}] = values[k + j * Nk + i * Nk * Nj]
    VecType values;

public:
    ///Creates an 0 dimensional vector
    NDVector() = default;
//    ///Copy the vector //Gives warnings at compilation
//    NDVector(const NDVector<T> &vector): dimension(vector.dimension), size(vector.size), values(vector.values){};
    ///Creates an 'dimension' dimensional vector with size 'size' and initial values 'value'. Throws own_error if size.size() != dimension.
    NDVector(SizeType_I &dimension, IndexType_I &size, const T &value = {});
    ///Creates an one dimensional vector with the values 'values'.
    NDVector(const std::initializer_list<T> &values): dimension(1), size({values.size()}), values(values){};
    ///Creates an one dimensional vector with the values 'values'. Isn't marked with explicit to allow NDVec = std::vector.
    NDVector(VecType_I &values): dimension(1), size({values.size()}), values(values) {};
    ///Creates a one dimensional vector of size 'size' with initial values 'value'.
    explicit NDVector(SizeType_I &size, const T &value = {}): dimension(1), size({size}), values(size, value) {};
    ///Destructor
    ~NDVector() = default;

    ///Creates a one dimensional vector values = val.
    NDVector<T>& operator=(const std::initializer_list<T> &val);
    ///Creates a one dimensional vector values = val.
    NDVector<T>& operator=(VecType_I &val);

    ///Gets the element at value[index].
    T& operator[] (SizeType_I &index);
    const T& operator[] (SizeType_I &index) const;
    ///Gets the element at {index0, index1, ...}.
    T& operator[] (IndexType_I &index);
    const T& operator[] (IndexType_I &index) const;

    ///Gets the element at value[index], but with check for boundary and negative index stand for backward counted. Throws own_error if index out ouf bounds.
    T& at(int index);
    const T& at(int index) const;
    ///Gets the element at {index0, index1, ...}, but with check for boundary and negative index stand for backward counted. Throws own_error if index out ouf bounds.
    T& at(const std::vector<int> &index);
    const T& at(const std::vector<int> &index) const;
    ///Gets the element at {index0, index1, ...}, but with index[subvectorindex] = index_in_subvector without boundary check.
    T& at(SizeType_I &index_in_subvector, SizeType_I &subvector_index, IndexType_I &index);
    const T& at(SizeType_I &index_in_subvector, SizeType_I &subvector_index, IndexType_I &index) const;
    ///Gets the element at {index0, index1, ...}, but with index[subvectorindex] = index_in_subvector with boundary check and negative index stand for backward counted. Throws own_error if index out ouf bounds.
    T& at(const int &index_in_subvector, SizeType_I &subvector_index, IndexType_I &index);
    const T& at(const int &index_in_subvector, SizeType_I &subvector_index, IndexType_I &index) const;

    ///Converts the n dimensional index to an one dimensional index.
    SizeType to_one_dim_index(IndexType_I &index) const;

    ///Gets the dimension.
    SizeType_I& get_dimension() const {return dimension;};
    ///Gets the size.
    IndexType_I& get_size() const {return size;};
    ///Gets the size at index i.
    SizeType_I& get_size(SizeType_I index) const {
        if(index > dimension) THROW("Dimension of the Vector doesn't fit.");
        return size[index];
    };
    ///Gets the values.
    VecType_I& get_values() const {return values;};

    /**
     * Apply the function func to every element of the vector.
     * @param func function that should be applied
     */
    void apply(const std::function<T(T)> &func);

    /**
     * Apply the multidimensional function func the base vectors x.
     * If the this vector is 0, the size reset to fit to x.
     * @param x the base vectors the function should be applied to
     * @param func function that should be applied
     * @throws own_error if the dimension of this vector and x doesn't fit
     */
    void apply(const std::vector<VecType> &x, const std::function<T(VecType)> &func);

    /**
     * Creates an vector with the operator func applied to x and y.
     * @param x, y input vectors for the operator
     * @param func operator that should be applied
     * @return vector f(x, y)
     * @throws own_error if the dimension and size of the input vectors doesn't fit.
     */
    static NDVector<T> operate(const NDVector<T> &x, const NDVector<T> &y, const std::function<T(T, T)> &func);

    /**
     * Give the subvector at index 'index'.
     * @param subvector_index number of the dimension that should be returned
     * @param index indexes of the other dimensions
     * @return subvector
     */
    std::vector<T> subvector(SizeType_I &subvector_index, IndexType_I &index) const;

    /**
     * Give the subvector at index 'index'.
     * @param output_index number aof the dimension that should be given
     * @param index indexes of the other dimensions
     * @return subvector
     */
    void set_subvector(VecType_I &subvector, SizeType_I &subvector_index, IndexType_I &index);

    /**
     * Apply the function func that deals with a one dimensional subvector to all subvectors.
     * In func the indices of the vector must be addressed like with vec.at(i, subvector_index, index).
     * @param func function of the form f(&NDVector, &subvector_index, &index), that fork only on the subvector at index 'index'
     * in dimension subvector_index.
     * @param subvector_index the index of the dimension of the subvector, the function should be applied to
     * @param dimension_index, index parameters need for the recursion. They are set automatic, don't use them!
     */
    void apply_subvector(const std::function<void(NDVector<T>&, SizeType_I&, IndexType_I &)> &func,
                         SizeType_I &subvector_index, SizeType dimension_index=0, IndexType index={});

    /**
     * Prints the vector.
     * @param max_print_size the maximal count of elements to be printed, 0 for inf
     */
    void print(SizeType_I &max_print_size=10) const;

private:
    ///Needed for apply. Iterate recursive trough all dimensions.
    void apply_recursive_step(const std::vector<VecType> &x, const std::function<T(VecType)> &func,
            SizeType dimension_index, SizeType index, VecType val);
};

#include "NDVector.cpp"
#endif //PROGRAMM_NDVECTOR_HPP
