//
// Created by flosc on 03.06.2020.
//

#ifndef PROGRAMM_PARAMETERS_HPP
#define PROGRAMM_PARAMETERS_HPP

#include <cmath>

/**
 * Define the chosen Parameters as Macros
 */

///Protonmass
#define MASS_P 938.272088 ///<m_p in MeV

///Parameters for light_curve plot
//#define P0 1000.0 ///<p0 in MeV, make p dimensionless
//#define B0 0.1 ///<b0 in Mev/Myr
//#define KAPPA0 0.1 ///<kappa0 in kpc^2/Myr
//#define DELTA 0.33 ///<delta in kappa
//#define POWER_N 0.0 ///<n in b
//#define GAMMA 2.2 ///<gamma in q

///Parameters for real CR diffusion
#define P0 MASS_P ///<p0 in MeV, make p dimensionless
#define B0 1.96879 ///<b0 in Mev/Myr, only for normalization
#define KAPPA0 0.035 ///<kappa0 in kpc^2/Myr
#define DELTA 0.63 ///<delta in kappa
#define POWER_N -2.0 ///<n in b (not needed!)
#define GAMMA 2.2 ///<gamma in q

///Parameters for making dimensionless
#define TAU (P0 / B0) ///<tau in Myr, make t dimensionless
#define LAMBDA std::sqrt(KAPPA0 * P0 / B0) ///<lambda in kpc, make r dimensionless

///Dimensionless parameters
#define R (10.0 / LAMBDA) ///<R dimensionless (10 kpc)
#define L (4.0 / LAMBDA) ///<L dimensionless (4 kpc)
#define H (0.1 / LAMBDA) ///<h dimensionless (0.1 kpc)

///Parameters for calculation, dimensionless
#define LOG_T_START std::log10(1e-3 / TAU) ///<log_t_start dimensionless (1e-3 Myr)
#define LOG_T_END std::log10(1e2 / TAU) ///<log_t_end dimensionless (1e2 Myr)
#define DLOG_T 0.05 ///<dlog_t dimensionless

#define LOG_P_START std::log(35 / P0) ///<log_p_start dimensionless (35 MeV) (Ekin ca. 1 MeV)
#define LOG_P_END std::log(15000.0 / P0) ///<log_p_end dimensionless (15 GeV) (Ekin ca. 10GeV)
#define DLOG_P 0.2 ///<dlog_p dimensionless (give ca. 12 values in a interval [p, 10p])

#define STEPS 9e1 ///<steps
#define OFFSET 1e1 ///<offset
#define DT (5e-3 / TAU) ///<dt dimensionless, maximal value for chosen dlog_p (maximal could be 5e-3 Myr)
//#define DT (4.9e-3 / TAU) ///<dt dimensionless, maximal value for chosen dlog_p (maximal could be 5e-3 Myr)

///Parameter set t part 1
#define LOG_T_START_1 LOG_T_START ///<log_t_start dimensionless (part 1)
#define LOG_T_END_1 std::log10(1e-2 / TAU) ///<log_t_end dimensionless (part 1)
#define R_1 (0.4 / LAMBDA) ///<R dimensionless (part 1)
#define L_1 (0.4 / LAMBDA) ///<L dimensionless (part 1)
#define DR_1 (0.001 / LAMBDA) ///<dr dimensionless (part 1)
#define DZ_1 (0.001 / LAMBDA) ///<dz dimensionless (part 1)
#define DT_1 (1e-4 / TAU) ///<dt dimensionless (part 1)
#define OFFSET_1 10 ///<offset (part 1)
///Parameter set t part 2
#define LOG_T_START_2 std::log10(1e-2 / TAU) ///<log_t_start dimensionless (part 2)
#define LOG_T_END_2 std::log10(1e-1 / TAU) ///<log_t_end dimensionless (part 2)
#define R_2 (1.0 / LAMBDA) ///<R dimensionless (part 2)
#define L_2 (1.0 / LAMBDA) ///<L dimensionless (part 2)
#define DR_2 (0.002 / LAMBDA) ///<dr dimensionless (part 2)
#define DZ_2 (0.002 / LAMBDA) ///<dz dimensionless (part 2)
//#define DT_2 DT ///<dt dimensionless (part 2)
#define DT_2 (1e-3 / TAU) ///<dt dimensionless (part 2)

///Parameter set t part 3
#define LOG_T_START_3 std::log10(1e-1 / TAU) ///<log_t_start dimensionless (part 3)
#define LOG_T_END_3 std::log10(1e0 / TAU) ///<log_t_end dimensionless (part 3)
#define R_3 (4.0 / LAMBDA) ///<R dimensionless (part 3)
#define L_3 L ///<L dimensionless (part 3)
//#define L_3 (4.0 / LAMBDA) ///<L dimensionless (part 3)
#define DR_3 (0.01 / LAMBDA) ///<dr dimensionless (part 3)
#define DZ_3 (0.01 / LAMBDA) ///<dz dimensionless (part 3)
#define DT_3 DT ///<dt dimensionless (part 3)
//#define DT_3 (1e-2 / TAU) ///<dt dimensionless (part 3)

///Parameter set t part 4
#define LOG_T_START_4 std::log10(1e0 / TAU) ///<log_t_start dimensionless (part 4)
#define LOG_T_END_4 std::log10(1e1 / TAU) ///<log_t_end dimensionless (part 4)
#define R_4 (10.0 / LAMBDA) ///<R dimensionless (part 4)
#define L_4 L ///<L dimensionless (part 4)
//#define L_4 (10.0 / LAMBDA) ///<L dimensionless (part 4)
#define DR_4 (0.05 / LAMBDA) ///<dr dimensionless (part 4)
#define DZ_4 DZ_3 ///<dz dimensionless (part 4)
//#define DZ_4 (0.05 / LAMBDA) ///<dz dimensionless (part 4)
#define DT_4 DT ///<dt dimensionless (part 4)
//#define DT_4 (1e-1 / TAU) ///<dt dimensionless (part 4)

///Parameter set t part 5
#define LOG_T_START_5 std::log10(1e1 / TAU) ///<log_t_start dimensionless (part 5)
#define LOG_T_END_5 LOG_T_END ///<log_t_end dimensionless (part 5)
#define R_5 (20.0 / LAMBDA) ///<R dimensionless (part 5)
#define L_5 L ///<L dimensionless (part 5)
//#define L_5 (20.0 / LAMBDA) ///<L dimensionless (part 5)
#define DR_5 (0.05 / LAMBDA) ///<dr dimensionless (part 5)
#define DZ_5 DZ_4 ///<dz dimensionless (part 5)
//#define DZ_5 (0.05 / LAMBDA) ///<dz dimensionless (part 5)
#define DT_5 DT ///<dt dimensionless (part 5)
//#define DT_5 (1e0 / TAU) ///<dt dimensionless (part 5)

///Parameter set out
#define DR_OUT DR_5 ///<dr_out dimensionless
#define R_OUT (R + DR_OUT) ///<R_out dimensionless

///Parameters for testing the log_p part
#define TEST_DLOG_P DLOG_P
#define TEST_LOG_P_START -0.5
#define TEST_LOG_P_END 0.5
#define TEST_EPSILON_P 1e-2

///Parameters for TOY PARAMETERS
#define TOY_DX 0.1
#define TOY_DT 0.01
#define TOY_STEPS 10
//#define TOY_STEPS 0
#define TOY_OFFSET 100
#define TOY_LENGTH 10
#define TOY_DLOG_P 0.005
#define TOY_LOG_P_START -1.5
#define TOY_LOG_P_END -0.5
#define TOY_EPSILON_P 1e-2

///Parameters for the error plots
#define ERROR_OFFSET OFFSET
#define ERROR_LENGTH_R R_3
#define ERROR_LENGTH_Z L_3
//#define ERROR_LENGTH_Z -1 ///<not needed at the moment
#define ERROR_LOG_P_START LOG_P_START
#define ERROR_LOG_P_END LOG_P_END
///Plot in steps
#define RANGE_POW10(x) 1e##x, 2e##x, 3e##x, 4e##x, 5e##x, 6e##x, 7e##x, 8e##x, 9e##x
//#define ERROR_STEPS {RANGE_POW10(0), RANGE_POW10(1), RANGE_POW10(2), RANGE_POW10(3), 1e4}
//#define ERROR_STEPS {RANGE_POW10(0), RANGE_POW10(1), RANGE_POW10(2), 1e3}
#define ERROR_STEPS {RANGE_POW10(0), RANGE_POW10(1), 1e2}
#define ERROR_STEPS_DT {DT_3}
#define ERROR_STEPS_DR {DR_3}
#define ERROR_STEPS_DZ {DZ_3}
#define ERROR_STEPS_DLOG_P {DLOG_P}
///Plots in dt
//#define ERROR_DT {1e-4 / TAU, 5e-4/ TAU, 1e-3/TAU}
#define ERROR_DT {} ///<not needed at the moment
#define ERROR_DT_STEP STEPS
#define ERROR_DT_DR ERROR_STEPS_DR
#define ERROR_DT_DZ ERROR_STEPS_DZ
#define ERROR_DT_DLOG_P ERROR_STEPS_DLOG_P

#endif //PROGRAMM_PARAMETERS_HPP
