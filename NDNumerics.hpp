//
// Created by flosc on 13.05.2020.
//

#ifndef PROGRAMM_NDNUMERICS_HPP
#define PROGRAMM_NDNUMERICS_HPP

#include "NDVector.hpp"
#include "plot.hpp"

/**
 * Provides some function that calculate numerical solutions to differential equations.
 * The functions work with 'NDVector' and calculate the simulation step in subvector of ine dimension via apply_subvector.
 * @relatesalso NDVector
 */
namespace NDNumerics {
    ///Calculates diffusion one time step with the FTCS method in an diffusion problem at only one subvector. See diffusion_ftcs for more information.
    void diffusion_ftcs_index(VecND &u, SizeType_I &subvector_index, IndexType_I &index, NumType_I &dx, NumType_I &dt,
                              NumType_I &kappa0, const int &boundary_condition);

    /**
    * Calculates one time step with the FTCS method in an diffusion problem. <BR>
    * Equation: du/dt = kappa0 * d^2u/dx^2
    * @param[in, out] un function u at time step n, returns u at time step n+1
    * @param[in] dx, dt step size
    * @param[in] kappa0 diffusion coefficient
    * @param[in] dimension_index number of the dimension in u, in that the equation should be solved
    * @param[in] boundary_condition type of boundary condition: <BR>
    *  0: Constant boundary at start and end <BR>
    *  1: Boundary conditions for an r variable - constant at end, at start -r = r <BR>
    *  2: Cyclic boundary conditions
    * @see Numerical Recipes p. 1044
    * @note stable for: 2*kappa0*dt / (dx)^2 &le 1
    */
    void diffusion_ftcs(VecND &un, NumType_I &dx, NumType_I &dt, NumType_I &kappa0, SizeType_I &dimension_index,
                        const int &boundary_condition = 0);

    ///Calculates diffusion one time step with the fully implicit method in an diffusion problem at only one subvector. See diffusion_implicit for more information.
    void diffusion_implicit_index(VecND &u, SizeType_I &subvector_index, IndexType_I &index, VecNum_I &a, VecNum_I &b,
                                  VecNum_I &c);

    ///Calculates diffusion one time step with the fully implicit method in an diffusion problem at only one subvector for cyclic boundary conditions. See diffusion_implicit for more information.
    void
    diffusion_implicit_cyclic_index(VecND &u, SizeType_I &subvector_index, IndexType_I &index, VecNum_I &a, VecNum_I &b,
                                    VecNum_I &c);

    /**
    * Calculates one time step with the fully implicit method in an diffusion problem. <BR>
    * Equation: du/dt = kappa0 * d^2u/dx^2
    * @param[in, out] un function u at time step n, returns u at time step n+1
    * @param[in] dx, dt step size
    * @param[in] kappa0 diffusion coefficient
    * @param[in] dimension_index number of the dimension in u, in that the equation should be solved
    * @param[in] boundary_condition type of boundary condition: <BR>
    *  0: Constant boundary at start and end <BR>
    *  1: Boundary conditions for an r variable - constant at end, at start -r = r <BR>
    *  2: Cyclic boundary conditions
    * @relatesalso tridiag
    * @see Numerical Recipes p. 1045
    */
    void diffusion_implicit(VecND &un, NumType_I &dx, NumType_I &dt, NumType_I &kappa0, SizeType_I &dimension_index,
                            const int &boundary_condition = 0);


    /**
    * Calculates one time step with the Crank-Nicolson method in an diffusion problem. <BR>
    * Equation: du/dt = kappa0 * d^2u/dx^2
    * @param[in, out] un function u at time step n, returns u at time step n+1
    * @param[in] dx, dt step size
    * @param[in] kappa0 diffusion coefficient
    * @param[in] dimension_index number of the dimension in u, in that the equation should be solved
    * @param[in] boundary_condition type of boundary condition: <BR>
    *  0: Constant boundary at start and end <BR>
    *  1: Boundary conditions for an r variable - constant at end, at start -r = r
    * @relatesalso diffusion_ftcs, diffusion_implicit
    * @see Numerical Recipes p. 1046
    * @note Works with FTCS and fully implicit method via operator splitting
    */
    void
    diffusion_crank_nicolson(VecND &un, NumType_I &dx, NumType_I &dt, NumType_I &kappa0, SizeType_I &dimension_index,
                             const int &boundary_condition = 0);

    ///Calculates one time step with the Lax-Wendroff method for the first order radial part of the three dimensional diffusion equation at only one subvector. See diffusion_lax_wendroff for more information.
    ///@param x_type 0: x[0] = 0, 1: x[1] = 0
    void diffusion_lax_wendroff_index(VecND &u, SizeType_I &subvector_index, IndexType_I &index, NumType_I &dx,
                                      NumType_I &dt, NumType_I &kappa0, VecNum_I &x, const int &x_type = 0);

    /**
    * Calculates one time step with the Lax-Wendroff method for the first order radial part of the three dimensional diffusion equation. <BR>
    * Equation: du/dt = - kappa0/x * du/dx <BR>
    * Boundary Conditions: Constant boundaries at start and end.
    * @param[in, out] un function u at time step n, returns u at time step n+1
    * @param[in] dx, dt step size
    * @param[in] kappa0 diffusion coefficient
    * @param[in] x variable x, see Equation
    * @param[in] dimension_index number of the dimension in u, in that the equation should be solved
    * @see Numerical Recipes p. 1040
    * @note stable for: abs(kappa0/x) * dt/dx &le 1
    */
    void diffusion_lax_wendroff(VecND &un, NumType_I &dx, NumType_I &dt, NumType_I &kappa0, VecNum_I &x,
                                SizeType_I &dimension_index);

    /**
    * Calculates one time step with the Lax-Wendroff method in an flux conservative first order differential equation. <BR>
    * Equation: du/dt = - dF(u)/dx = - v * du/dx, with F(u) = v * u <BR>
    * Boundary Conditions: Constant boundaries at start and end.
    * @param[in, out] un function u at time step n, returns u at time step n+1
    * @param[in] dx, dt step size
    * @param[in] v velocity
    * @param[in] dimension_index number of the dimension in u, in that the equation should be solved
    * @see Numerical Recipes p. 1040
    * @note stable for: abs(v)*dt/dx &le 1
    */
    void lax_wendroff(VecND &un, NumType_I &dx, NumType_I &dt, NumType_I &v, SizeType_I &dimension_index);

    /**
    * Calculates one time step with the Lax method in an flux conservative first order differential equation. <BR>
    * Equation: du/dt = - dF(u)/dx = - v * du/dx =, with F(u) = v * u <BR>
    * Boundary Conditions: Constant boundaries at start and end.
    * @param[in, out] un function u at time step n, returns u at time step n+1
    * @param[in] dx, dt step size
    * @param[in] v velocity
    * @param[in] dimension_index number of the dimension in u, in that the equation should be solved
    * @see Numerical Recipes p. 1034
    * @note stable for: abs(v)*dt/dx &le 1
    */
    void lax_method(VecND &un, NumType_I &dx, NumType_I &dt, NumType_I &v, SizeType_I &dimension_index);

    /**
    * Calculates one time step with the staggered leapfrog method in an flux conservative first order differential equation. <BR>
    * Equation: du/dt = - dF(u)/dx = - v * du/dx =, with F(u) = v * u <BR>
    * Boundary Conditions: Constant boundaries at start and end.
    * @param[in, out] un function u at time step n, returns u at time step n+1
    * @param[in] um function u at time step n-1
    * @param[in] dx, dt step size
    * @param[in] v velocity
    * @param[in] dimension_index number of the dimension in u, in that the equation should be solved
    * @see Numerical Recipes p. 1034
    * @note stable for: abs(v)*dt/dx &le 1
    */
    void
    staggered_leapfrog(VecND &un, VecND &um, NumType_I &dx, NumType_I &dt, NumType_I &v, SizeType_I &dimension_index);

    /**
     * Calculates the F(j-1/2) for a donor cell.
     * @param cjm c at j-1
     * @param cj c at j
     * @param v V at j-1/2
     * @return F at j-1/2
     */
    NumType donor_cell(NumType_I &cjm, NumType_I &cj, NumType_I &v, NumType_I &dx, NumType_I &dt);

    ///Calculates one time step with a upwind scheme at only one subvector. See upwind for more information.
    void upwind_index(VecND &un, SizeType_I &subvector_index, IndexType_I &index, NumType_I &dx,
                      NumType_I &dt, VecNum_I &v);

    /**
    * Calculates one time step with a simple upwind method. <BR>
    * Equation: du/dt = - d/dx (v(x) * u) <BR>
    * Boundary Conditions: Constant boundaries at start and end.
    * @param[in, out] un function u at time step n, returns u at time step n+1
    * @param[in] dx, dt step size
    * @param[in] v velocity NDVector
    * @param[in] dimension_index number of the dimension in u, in that the equation should be solved
    * @see Myths and Methods in Modelling p. 61
    * @note stable for: max(abs(v)*dt/dx) &le 1
    */
    void upwind(VecND &un, NumType_I &dx, NumType_I &dt, VecND_I &v, SizeType_I &dimension_index);

    ///Calculates one time step with a mpdata upwind scheme at only one subvector. See mpdata for more information.
    void mpdata_index(VecND &un, SizeType_I &subvector_index, IndexType_I &index, NumType_I &dx,
                      NumType_I &dt, VecNum_I &v);

    /**
    * Calculates one time step with a mpdata upwind scheme. <BR>
    * Equation: du/dt = - d/dx (v(x) * u) <BR>
    * Boundary Conditions: Constant boundaries at start and end.
    * @param[in, out] un function u at time step n, returns u at time step n+1
    * @param[in] dx, dt step size
    * @param[in] v velocity NDVector
    * @param[in] dimension_index number of the dimension in u, in that the equation should be solved
    * @see Myths and Methods in Modelling p. 62
    * @see P. K. Smolarkiewicz. A simple positive deﬁnite advection scheme with small implicit diffusion, Mon Weather Rev 111, 479–486, 1983.
    * @note stable for: max(abs(v)*dt/dx) &le 1
    */
    void
    mpdata(VecND &un, NumType_I &dx, NumType_I &dt, VecND_I &v, SizeType_I &dimension_index);

    /**
     * Crates an interpolated vector un1[j] = un[xp[j]] using linear interpolation.
     * @param un vector to be interpolated
     * @param xp index vector
     * @return un1
     */
    VecNum interpolate_linear(VecNum_I &un, VecNum_I &xp);

    /**
     * Crates an interpolated vector un1[j] = un[xp[j]] using cubic interpolation.
     * @param un vector to be interpolated
     * @param xp index vector
     * @return un1
     * @see https://en.wikipedia.org/wiki/Cubic_Hermite_spline
     * @see https://www.paulinternet.nl/?page=bicubic
     */
    VecNum interpolate_cubic(VecNum_I &un, VecNum_I &xp);

    ///Calculates  one time step with a Semi-Lagrangian scheme at only one subvector. See semi_lagrangian for more information.
    void semi_lagrangian_index(VecND &un, SizeType_I &subvector_index, IndexType_I &index, NumType_I &dx, NumType_I &dt,
                               VecNum_I &v, const int &boundary_condition = 0, const int &iterations = 5);

    /**
    * Calculates one time step with a Semi-Lagrangian scheme. <BR>
    * Equation: du/dt = -v(x) * du/dx <BR>
    * Boundary Conditions: Constant boundaries at start and end. <BR>
    * Assume velocity is constant in time v(t,x) = v(x).
    * @param[in, out] un function u at time step n, returns u at time step n+1
    * @param[in] dx, dt step size
    * @param[in] v velocity NDVector
    * @param[in] dimension_index number of the dimension in u, in that the equation should be solved
    * @param[in] boundary_condition type of boundary condition: <BR>
    *  0: Constant boundary at start and end <BR>
    *  1: Constant at end, not constant at start <BR>
    *  2: Not constant boundary at start and end
    * @param[in] iterations number of iterations calculation v
    * @see Myths and Methods in Modelling p. 64
    * @note stable for: ??
    */
    void semi_lagrangian(VecND &un, NumType_I &dx, NumType_I &dt, VecND_I &v, SizeType_I &dimension_index,
                         const int &boundary_condition = 0, const int &iterations = 5);

    ///Calculates one time step with a Semi-Lagrangian scheme and derivative of v at only one subvector. See semi_lagrangian_2 for more information.
    void
    semi_lagrangian_2_index(VecND &un, SizeType_I &subvector_index, IndexType_I &index, NumType_I &dx, NumType_I &dt,
                            VecNum_I &v, const int &boundary_condition = 0, const int &iterations = 5);

    /**
    * Calculates one time step with a Semi-Lagrangian scheme, takes also the derivative of the velocity into account. <BR>
    * Equation: du/dt = - d/dx (v(x) * u) = -v(x) * du/dx - u * dv/dx <BR>
    * Boundary Conditions: Constant boundaries at start and end. <BR>
    * Assume velocity is constant in time v(t,x) = v(x).
    * @param[in, out] un function u at time step n, returns u at time step n+1
    * @param[in] dx, dt step size
    * @param[in] v velocity NDVector
    * @param[in] dimension_index number of the dimension in u, in that the equation should be solved
    * @param[in] boundary_condition type of boundary condition: <BR>
    *  0: Constant boundary at start and end <BR>
    *  1: Constant at end, not constant at start <BR>
    *  2: Not constant boundary at start and end
    * @param[in] iterations number of iterations calculation v
    * @see Myths and Methods in Modelling p. 71
    * @note stable for: ??
    */
    void semi_lagrangian_2(VecND &un, NumType_I &dx, NumType_I &dt, VecND_I &v, SizeType_I &dimension_index,
                           const int &boundary_condition = 0, const int &iterations = 5);
}

#endif //PROGRAMM_NDNUMERICS_HPP
