"""Create some standard test plots for the program."""
import plot
import CRDiffusion
import matplotlib.pyplot as plt
import numpy as np
import glob
import h5py
import time


def OneDimDiff(index=1, plot_error=True):
    file_list = glob.glob('OneDimDiff_*_%i.csv' % index)
    if not file_list:
        return -1
    plt.figure()
    for file in file_list:
        plot.plot_csv_file(file)
    title = '1D Diffusion'
    if index == 1:
        title += ', TOY PARAMETER'
    plt.title(title)
    plt.legend()
    plt.savefig('OneDimDiff_%i.pdf' % index)

    if plot_error:
        file_list = glob.glob('OneDimDiff_*_%i_error.csv' % index)
        if not file_list:
            return -1
        plt.figure()
        for file in file_list:
            plot.plot_csv_file(file)
        title = '1D Diffusion Error'
        if index == 1:
            title += ', TOY PARAMETER'
        plt.title(title)
        plt.legend()
        plt.yscale('log')
        plt.savefig('OneDimDiff_%i_error.pdf' % index)
    return 0


def OneDimDiffR(index=1, plot_error=True):
    file_list = glob.glob('OneDimDiffR_*_%i.csv' % index)
    if not file_list:
        return -1
    plt.figure()
    for file in file_list:
        plot.plot_csv_file(file)
    title = '1D Diffusion R coordinate'
    if index == 1:
        title += ', TOY PARAMETER'
    plt.title(title)
    plt.legend()
    plt.savefig('OneDimDiffR_%i.pdf' % index)

    if plot_error:
        file_list = glob.glob('OneDimDiffR_*_%i_error.csv' % index)
        if not file_list:
            return -1
        plt.figure()
        for file in file_list:
            plot.plot_csv_file(file)
        title = '1D Diffusion Error'
        if index == 1:
            title += ', TOY PARAMETER'
        plt.title(title)
        plt.legend()
        plt.yscale('log')
        plt.savefig('OneDimDiffR_%i_error.pdf' % index)
    return 0


def OneDimError(method='steps', variant=''):
    file_list = glob.glob('OneDimDiff%sError_%s_*.csv' % (variant, method))
    if not file_list:
        return -1
    plt.figure()
    for file in file_list:
        plot.plot_csv_file(file)
    title = 'Error of 1D Diffusion'
    if variant == 'R':
        title += ', R diffusion'
    plt.title(title)
    plt.legend()
    if method == 'steps':
        plt.xscale('log')
    plt.yscale('log')
    plt.savefig('OneDimDiff%sError_%s.pdf' % (variant, method))
    return 0


def NonDiffusive():
    file_list = glob.glob('NonDiffusive_*.csv')
    if not file_list:
        return -1
    plt.figure()
    for file in file_list:
        plot.plot_csv_file(file)
    title = 'Non Diffusive Test of numeric algorithms'
    plt.title(title)
    plt.legend()
    plt.savefig('NonDiffusive.pdf')
    return 0


def TwoDimDiff(axis='r', index=1, plot_error=True):
    file_list = glob.glob('TwoDimDiff_%s_*_%i.csv' % (axis, index))
    if not file_list:
        return -1
    plt.figure()
    for file in file_list:
        plot.plot_csv_file(file)
    title = '2D Diffusion, %s axis' % axis
    if index == 1:
        title += ', TOY PARAMETER'
    plt.title(title)
    plt.legend()
    plt.savefig('TwoDimDiff_%s_%i.pdf' % (axis, index))

    if plot_error:
        file_list = glob.glob('TwoDimDiff_%s_*_%i_error.csv' % (axis, index))
        if not file_list:
            return -1
        plt.figure()
        for file in file_list:
            plot.plot_csv_file(file)
        title = '2D Diffusion Error, %s axis' % axis
        if index == 1:
            title += ', TOY PARAMETER'
        plt.title(title)
        plt.legend()
        plt.yscale('log')
        plt.savefig('TwoDimDiff_%s_%i_error.pdf' % (axis, index))
    return 0


def TwoDimDiffError(method='steps'):
    file_list = glob.glob('TwoDimDiffError_%s_*.csv' % method)
    if not file_list:
        return -1
    plt.figure()
    for file in file_list:
        plot.plot_csv_file(file)
    plt.title('Error of 2D Diffusion')
    plt.legend()
    if method == 'steps':
        plt.xscale('log')
    plt.yscale('log')
    plt.savefig('TwoDimDiffError_%s.pdf' % method)
    return 0


def ThreeDimDiff(axis='r', index=1, plot_error=True):
    file_list = glob.glob('ThreeDimDiff_%s_*_%i.csv' % (axis, index))
    if not file_list:
        return -1
    plt.figure()
    for file in file_list:
        plot.plot_csv_file(file)
    title = '3D Diffusion, %s axis' % axis
    if index == 1:
        title += ', TOY PARAMETER'
    plt.title(title)
    plt.legend()
    if axis == 'logp':
        plt.yscale('log')
    plt.savefig('ThreeDimDiff_%s_%i.pdf' % (axis, index))

    if plot_error:
        file_list = glob.glob('ThreeDimDiff_%s_*_%i_error.csv' % (axis, index))
        if not file_list:
            return -1
        plt.figure()
        for file in file_list:
            plot.plot_csv_file(file)
        title = '3D Diffusion Error, %s axis' % axis
        if index == 1:
            title += ', TOY PARAMETER'
        plt.title(title)
        plt.legend()
        plt.yscale('log')
        plt.savefig('ThreeDimDiff_%s_%i_error.pdf' % (axis, index))
    return 0


def ThreeDimDiffError(method='steps'):
    file_list = glob.glob('ThreeDimDiffError_%s_*.csv' % method)
    if not file_list:
        return -1
    plt.figure()
    for file in file_list:
        plot.plot_csv_file(file)
    plt.title('Error of 3D Diffusion')
    plt.legend()
    if method == 'steps':
        plt.xscale('log')
    plt.yscale('log')
    plt.savefig('ThreeDimDiffError_%s.pdf' % method)
    return 0


def ThreeDimTestDiff(axis='r', index=1, plot_error=True):
    file_list = glob.glob('ThreeDimTestDiff_%s_*_%i.csv' % (axis, index))
    if not file_list:
        return -1
    plt.figure()
    for file in file_list:
        plot.plot_csv_file(file)
    title = 'Test of the diffusion part in the 3D Diffusion, %s axis' % axis
    if index == 1:
        title += ', TOY PARAMETER'
    plt.title(title)
    plt.legend()
    plt.savefig('ThreeDimTestDiff_%s_%i.pdf' % (axis, index))

    if plot_error:
        file_list = glob.glob('ThreeDimTestDiff_%s_*_%i_error.csv' % (axis, index))
        if not file_list:
            return -1
        plt.figure()
        for file in file_list:
            plot.plot_csv_file(file)
        title = 'Test of the diffusion part in the 3D Diffusion Error, %s axis' % axis
        if index == 1:
            title += ', TOY PARAMETER'
        plt.title(title)
        plt.legend()
        plt.yscale('log')
        plt.savefig('ThreeDimTestDiff_%s_%i_error.pdf' % (axis, index))
    return 0


def ThreeDimTestP(index=1, plot_error=True):
    file_list = glob.glob('ThreeDimTestP_*_%i.csv' % index)
    if not file_list:
        return -1
    plt.figure()
    for file in file_list:
        plot.plot_csv_file(file)
    title = 'Test of the energy loss part in the 3D Diffusion'
    if index == 1:
        title += ', TOY PARAMETER'
    plt.title(title)
    plt.legend()
    plt.savefig('ThreeDimTestP_%i.pdf' % index)

    if plot_error:
        file_list = glob.glob('ThreeDimTestP_*_%i_error.csv' % index)
        if not file_list:
            return -1
        plt.figure()
        for file in file_list:
            plot.plot_csv_file(file)
        title = 'Test of the energy loss part in the 3D Diffusion Error'
        if index == 1:
            title += ', TOY PARAMETER'
        plt.title(title)
        plt.legend()
        plt.yscale('log')
        plt.savefig('ThreeDimTestP_%i_error.pdf' % index)
    return 0


def CRDiff(index=1):
    file_list = glob.glob('CRDiff_%i.hdf5' % index)
    if not file_list:
        return -1
    file = h5py.File('CRDiff_%i.hdf5' % index, 'r')
    r = file['r'][...]
    z = file['z'][...]
    log_p = file['log_p'][...]
    u = file['u'][...]
    index_r0 = 0
    index_z0 = np.abs(z).argmin()
    # index_log_p0 = u[:, index_z0, index_r0].argmax()
    index_log_p0 = -1

    # Plot
    plt.figure()
    plt.plot(r, u[index_log_p0, index_z0, :], label='Numerical')
    plt.xlabel('r')
    plt.ylabel('u(r, z=0, log_p=%.1f)' % log_p[index_log_p0])
    plt.title('CR Diffusion, r axis')
    plt.legend()
    plt.savefig('CRDiff_r_%i.pdf' % index)

    plt.figure()
    plt.plot(z, u[index_log_p0, :, index_r0], label='Numerical')
    plt.xlabel('z')
    plt.ylabel('u(z, r=0, log_p=%.1f)' % log_p[index_log_p0])
    plt.title('CR Diffusion, z axis')
    plt.legend()
    plt.savefig('CRDiff_z_%i.pdf' % index)

    plt.figure()
    plt.plot(log_p, u[:, index_z0, index_r0], label='Numerical')
    plt.xlabel('log_p')
    plt.ylabel('u(log_p, z=0, r=0)')
    plt.title('CR Diffusion, log_p axis')
    plt.legend()
    plt.savefig('CRDiff_logp_%i.pdf' % index)
    return 0


def CRDiffTest(index=1):
    file_list = glob.glob('CRDiffTest_%i.hdf5' % index)
    if not file_list:
        return -1
    file = h5py.File('CRDiffTest_%i.hdf5' % index, 'r')
    r = file['r'][...]
    z = file['z'][...]
    log_p = file['log_p'][...]
    u = file['u'][...]
    ut = file['u_theory'][...]
    index_r0 = 0
    index_z0 = np.abs(z).argmin()
    index_log_p0 = ut[:, index_z0, index_r0].argmax()

    # Plot
    plt.figure()
    plt.plot(r, u[index_log_p0, index_z0, :], label='Numerical')
    plt.plot(r, ut[index_log_p0, index_z0, :], label='Theory')
    plt.xlabel('r')
    plt.ylabel('u(r, z=0, log_p=%.1f)' % log_p[index_log_p0])
    plt.title('CR Diffusion Test, r axis')
    plt.legend()
    plt.savefig('CRDiffTest_r_%i.pdf' % index)

    plt.figure()
    plt.plot(z, u[index_log_p0, :, index_r0], label='Numerical')
    plt.plot(z, ut[index_log_p0, :, index_r0], label='Theory')
    plt.xlabel('z')
    plt.ylabel('u(z, r=0, log_p=%.1f)' % log_p[index_log_p0])
    plt.title('CR Diffusion Test, z axis')
    plt.legend()
    plt.savefig('CRDiffTest_z_%i.pdf' % index)

    plt.figure()
    plt.plot(log_p, u[:, index_z0, index_r0], label='Numerical')
    plt.plot(log_p, ut[:, index_z0, index_r0], label='Theory')
    plt.xlabel('log_p')
    plt.ylabel('u(log_p, z=0, r=0)')
    plt.title('CR Diffusion Test, log_p axis')
    plt.legend()
    plt.savefig('CRDiffTest_logp_%i.pdf' % index)
    return 0


def CRDiffEnergyLoss(index=1):
    file_list = glob.glob('CRDiff_energy_loss_%i.csv' % index)
    if not file_list:
        return -1
    plt.figure()
    for file in file_list:
        plot.plot_csv_file(file)
    title = 'Energy loss rate'
    plt.title(title)
    plt.legend()
    plt.savefig('CRDiff_energy_loss_%i.pdf' % index)
    return 0


def CRDiffDiffusionCoefficient(index=1):
    file_list = glob.glob('CRDiff_diffusion_coefficient_%i.csv' % index)
    if not file_list:
        return -1
    plt.figure()
    for file in file_list:
        plot.plot_csv_file(file)
    title = 'Diffusion coefficient'
    plt.title(title)
    plt.legend()
    plt.savefig('CRDiff_diffusion_coefficient_%i.pdf' % index)
    return 0


def main():
    print('Start creating plots.')
    t_start = time.time()

    for i in [1, 2]:
        OneDimDiff(i)
        OneDimDiffR(i)
    # OneDimError('steps')
    # OneDimError('dt')
    # OneDimError('steps', 'R')
    # OneDimError('dt', 'R')

    NonDiffusive()

    for i in [1, 2]:
        TwoDimDiff('r', i)
        TwoDimDiff('z', i)
    TwoDimDiffError('steps')
    TwoDimDiffError('dt')

    for i in [1, 2]:
        ThreeDimTestDiff('r', i)
        ThreeDimTestDiff('z', i)
        ThreeDimTestP(i)

    for i in [1, 2]:
        ThreeDimDiff('r', i)
        ThreeDimDiff('z', i)
        ThreeDimDiff('logp', i)
    ThreeDimDiffError('steps')
    ThreeDimDiffError('dt')

    for i in [1, 2]:
        CRDiffTest(i)
        CRDiff(i)
        CRDiffEnergyLoss(i)
        CRDiffDiffusionCoefficient(i)

    CRDiffusion.main()

    t_end = time.time()
    print('Process finished in %f seconds.' % (t_end - t_start))
    plt.show()
    return 0


if __name__ == '__main__':
    main()
