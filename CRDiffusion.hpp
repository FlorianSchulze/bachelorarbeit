//
// Created by flosc on 25.05.2020.
//

#ifndef PROGRAMM_CRDIFFUSION_HPP
#define PROGRAMM_CRDIFFUSION_HPP

#include "NDVector.hpp"
#include "plot.hpp"

/**
 * Solve the diffusion equation for cosmic rays (CR) numerical: <BR>
 * du/dt - kappa Laplace(u) + d/dp (b * u) Heaviside(z &le h) = 0 <BR>
 * u(r=R) = 0, u(z=-L) = u(r=L) = 0 <BR>
 * u(t=0) = DiracDelta(r) q <BR>
 * kappa = p^delta, b = -p^n, q = p^-gamma <BR>
 * u = u[log_p, z, r]
 * @relatesalso NDVector, NDNumerics
 */
namespace CRDiff {

    /**
     * Calculate the energy loss rate of real CR diffusion for the given log_p. <BR>
     * dp/dt = dp/dE dE/dt
     * @param log_p the log_p to calculate the energy loss rate
     * @return energy loss rate at log_p
     * @note The function u is scaled with exp(log_p), so the energy loss rate is scaled with exp(-log_p)
     */
    NumType energy_loss_rate(NumType_I &log_p);

    /**
     * Interpolate u_in with the axis r_in, z_in on u_out the axis r_out, z_out.
     * @param[out] u_out the interpolated u
     * @param[in] u_in the original u
     * @param[in] r_in, z_in the r, z axis of u_in
     * @param[in] r_out, z_out the r, z axis of u_out
     */
    void interpolate_u(VecND &u_out, VecND_I &u_in, VecNum_I &r_in, VecNum_I &z_in, VecNum_I &r_out, VecNum_I &z_out);

    /**
     * Interpolate the r part of an u_in with the axis r_in and returns u_out with the axis r_out.
     * @param u_in subvector of the original u at the wanted log_p, z
     * @param r_in the r axis of u_in
     * @param r_out the r axis of u_out
     * @return u_out
     * @see interplote_u
     */
    VecNum interpolate_u_r(VecNum_I &u_in, VecNum_I &r_in, VecNum_I &r_out);

    /**
     * Calculate the diffusion part of the equation for one time step. <BR>
     * du/dt = kappa(p) * (d^2u/dr^2 + 1/r du/dr + d^2u/dz^2) <BR>
     * kappa(p) = p^delta
     * @param[in, out] un the function u to be calculated. In at time step n, out at time step n+1.
     * @param[in] dr, dz, dt step size
     * @param[in] delta delta in kappa = p^delta
     * @param[in] r, log_p variables
     */
    void diffusion_step_r_z(VecND &un, NumType_I &dr, NumType_I &dz, NumType_I &dt, NumType_I &delta,
                            VecNum_I &r, VecNum_I &log_p);

    /**
     * Calculate the diffusion part of the equation for one time step, using the "parameter_values". <BR>
     * du/dt = kappa(p) * (d^2u/dr^2 + 1/r du/dr + d^2u/dz^2) <BR>
     * kappa(p) = beta (1 + p/m)^delta
     * @param[in, out] un the function u to be calculated. In at time step n, out at time step n+1.
     * @param[in] dr, dz, dt step size
     * @param[in] delta delta in kappa
     * @param[in] r, log_p variables
     */
    void diffusion_step_r_z_2(VecND &un, NumType_I &dr, NumType_I &dz, NumType_I &dt, NumType_I &delta,
                              VecNum_I &r, VecNum_I &log_p);

    /**
     * Calculate the energy loss part of the equation for one time step. <BR>
     * du/dt = - d/dp(b(p) u) = 2 exp(log_p) u + exp(log_p) du/dlog_p
     * @param[in, out] un the function u to be calculated. In at time step n, out at time step n+1.
     * @param[in] dlog_p, dt step size
     * @param[in] power_n n in b = -p^n
     * @param[in] log_p variable
     * @param[in] start_z, end_z start/stop index for energy loss in z
     */
    void calculate_step_p(VecND &un, NumType_I &dlog_p, NumType_I &dt, NumType_I &power_n, VecNum_I &log_p,
                          SizeType_I &start_z, SizeType_I &end_z);

    /**
     * Calculate the energy loss part of the equation for one time step, using the "parameter_values". <BR>
     * du/dt = dE/dt du/dp = exp(-log_p) dE/dt du/dlog_p
     * @param[in, out] un the function u to be calculated. In at time step n, out at time step n+1.
     * @param[in] dlog_p, dt step size
     * @param[in] log_p variable
     * @param[in] start_z, end_z start/stop index for energy loss in z
     */
    void calculate_step_p_2(VecND &un, NumType_I &dlog_p, NumType_I &dt, VecNum_I &log_p,
                            SizeType_I &start_z, SizeType_I &end_z);

    /**
     * Calculate u(t=0) = dirac_delta(r) q(p) = 1/(2 pi r) dirac_delta(r) dirac_delta(z) q(p). <BR>
     * u is scaled with exp(log_p). <BR>
     * In comparison with the greens function to the diffusion equation dirac_delta(r) is written as: <BR>
     * dirac_delta(r) = 1 / sqrt(2 pi epsilon) dirac_delta(r, epsilon) dirac_delta(z, epsilon) <BR>
     * with dirac_delta(x, epsilon) = 1 / sqrt(2 pi epsilon) exp(-x^2 / (2 epsilon)) <BR>
     * with epsilon = 2 * dt * offset
     * @param[in, out] un the function u to be set to u0.
     * @param[in] gamma gamma in q = p^-gamma
     * @param[in] r, z, log_p variables
     * @param[in] epsilon epsilon = 2 * dt * offset
     */
    void calculate_u0(VecND &un, NumType_I &gamma, VecNum_I &r, VecNum_I &z, VecNum_I &log_p, NumType_I &epsilon);

    /**
     * Calculate CR diffusion numerical.
     * @param[in, out] u the function u to be calculated. Must have the initial values at beginning, will be returned with
     *      calculated values after steps*dt
     * @param[in] dr, dz, dlog_p, dt step size
     * @param[in] steps count of steps to calculate
     * @param[in] delta delta in kappa = p^delta
     * @param[in] power_n n in b = -p^n
     * @param[in] r, log_p variables
     * @param[in] start_z, end_z start/stop index for energy loss in z
     */
    void calculate_steps(VecND &u, NumType_I &dr, NumType_I &dz, NumType_I &dlog_p, NumType_I &dt, const int &steps,
                         NumType_I &delta, NumType_I &power_n, VecNum_I &r, VecNum_I &log_p,
                         SizeType_I &start_z, SizeType_I &end_z);

    /**
     * Calculate CR diffusion numerical, using the "parameter_values".
     * @param[in, out] u the function u to be calculated. Must have the initial values at beginning, will be returned with
     *      calculated values after steps*dt
     * @param[in] dr, dz, dlog_p, dt step size
     * @param[in] steps count of steps to calculate
     * @param[in] delta delta in kappa
     * @param[in] power_n unused, has only the purpose so calculate_steps and calculate_steps_2 have the same form
     * @param[in] r, log_p variables
     * @param[in] start_z, end_z start/stop index for energy loss in z
     */
    void calculate_steps_2(VecND &u, NumType_I &dr, NumType_I &dz, NumType_I &dlog_p, NumType_I &dt, const int &steps,
                           NumType_I &delta, NumType_I &power_n, VecNum_I &r, VecNum_I &log_p,
                           SizeType_I &start_z, SizeType_I &end_z);

    /**
     * Calculate the diffusion with the given parameters and save the NDVector as .hdf5.
     * @param dr, dz, dlog_p, dt step size
     * @param steps count of steps to calculate
     * @param offset offset steps estimate epsilon in dirac_delta(x), epsilon = 2 * dt * offset
     * @param length_r, length_z length in r/z direction
     * @param log_p_start, log_p_end start/end of log_p
     * @param h thickness of the disk with energy loss
     * @param delta delta in kappa = p^delta
     * @param power_n n in b = -p^n
     * @param gamma gamma in q = p^-gamma
     * @param file_appendix the appendix to the file to identify different calculations
     */
    void plot(NumType_I &dr, NumType_I &dz, NumType_I &dlog_p, NumType_I &dt, const int &steps, const int &offset,
              NumType_I &length_r, NumType_I &length_z, NumType_I &log_p_start, NumType_I &log_p_end,
              NumType_I &h, NumType_I &delta, NumType_I &power_n, NumType_I &gamma,
              const std::string &file_appendix = "");

    /**
     * Tests the CR diffusion, using an delta peak injection at t0=0, r0=0, and compares the results with ThreeDimDiffusion.
     * Calculate the diffusion with the given parameters and save the NDVector as .hdf5.
     * @param dr, dz, dlog_p, dt step size
     * @param steps count of steps to calculate
     * @param offset count of steps to begin the calculation
     * @param length_r, length_z length in r/z direction
     * @param log_p_start, log_p_end start/end of log_p
     * @param delta delta in kappa = p^delta
     * @param gamma gamma in q = p^-gamma
     * @param file_appendix the appendix to the file to identify different calculations
     */
    void test(NumType_I &dr, NumType_I &dz, NumType_I &dlog_p, NumType_I &dt, const int &steps, const int &offset,
              NumType_I &length_r, NumType_I &length_z, NumType_I &log_p_start, NumType_I &log_p_end,
              NumType_I &delta, NumType_I &gamma, const std::string &file_appendix = "");

    /**
     * Plots and print out the energy loss rate.
     * @param dlog_p step size
     * @param log_p_start, log_p_end start/end of log_p
     * @param dt step size, to calculate whether the chosen parameters are stable
     * @param file_appendix the appendix to the file to identify different calculations
     */
    void plot_energy_loss_rate(NumType_I &dlog_p, NumType_I &log_p_start, NumType_I &log_p_end, NumType_I &dt = 0,
                               const std::string &file_appendix = "");

    /**
     * Plots and print out the diffusion coefficient.
     * @param dlog_p step size
     * @param log_p_start, log_p_end start/end of log_p
     * @param delta delta in kappa
     * @param file_appendix the appendix to the file to identify different calculations
     */
    void plot_diffusion_coefficient(NumType_I &dlog_p, NumType_I &log_p_start, NumType_I &log_p_end, NumType_I &delta,
                                    const std::string &file_appendix = "");

    /**
     * Calculate the template of the green's function of CRDiffusion and saves it as .hdf5.
     * @param dr, dz, dlog_p, dt step size
     * @param offset offset steps estimate epsilon in dirac_delta(x), epsilon = 2 * dt * offset
     * @param length_r, length_z length in r/z direction
     * @param log_p_start, log_p_end start/end of log_p
     * @param dlog_t step size of the logarithmic t variable
     * @param log_t_start, log_t_end start/end of the logarithmic t variable
     * @param h thickness of the disk with energy loss
     * @param delta delta in kappa = p^delta
     * @param power_n n in b = -p^n
     * @param gamma gamma in q = p^-gamma
     * @param file_appendix the appendix to the file to identify different calculations
     */
    void generate_template(NumType_I &dr, NumType_I &dz, NumType_I &dlog_p, NumType_I &dt, const int &offset,
                           NumType_I &length_r, NumType_I &length_z, NumType_I &log_p_start, NumType_I &log_p_end,
                           NumType_I &dlog_t, NumType_I &log_t_start, NumType_I &log_t_end,
                           NumType_I &h, NumType_I &delta, NumType_I &power_n, NumType_I &gamma,
                           const std::string &file_appendix = "");

    /**
     * Calculate the template of the green's function of CRDiffusion and saves it as .hdf5.
     * Split the calculation in different time parts with different step sizes.
     * @param vec_dr, vec_dz, vec_dt step sizes for the different parts
     * @param vec_length_r, vec_length_z length in r/z direction for the different parts
     * @param dlog_t step size of the logarithmic t variable
     * @param vec_log_t_start, vec_log_t_end start/end of t for the different parts (logarithmic)
     * @param offset offset steps estimate epsilon in dirac_delta(x), epsilon = 2 * dt * offset
     * @param dlog_p step size of log_p
     * @param log_p_start, log_p_end start/end of log_p
     * @param dr_out, length_r_out step size and length of the r coordinate in the template
     * @param h thickness of the disk with energy loss
     * @param delta delta in kappa = p^delta
     * @param power_n n in b = -p^n
     * @param gamma gamma in q = p^-gamma
     * @param file_appendix the appendix to the file to identify different calculations
     */
    void generate_template_2(VecNum_I &vec_dr, VecNum_I &vec_dz, VecNum_I &vec_dt,
                             VecNum_I &vec_length_r, VecNum_I &vec_length_z,
                             NumType_I &dlog_t, VecNum_I &vec_log_t_start, VecNum_I &vec_log_t_end,
                             const int &offset, NumType_I &dlog_p, NumType_I &log_p_start, NumType_I &log_p_end,
                             NumType_I &dr_out, NumType_I &length_r_out, NumType_I &h, NumType_I &delta,
                             NumType_I &power_n, NumType_I &gamma, const std::string &file_appendix = "");

    /**
     * Generates the templates.
     */
    void generate_templates();
}

#endif //PROGRAMM_CRDIFFUSION_HPP
