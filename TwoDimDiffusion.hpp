//
// Created by flosc on 11.05.2020.
//

#ifndef PROGRAMM_TWODIMDIFFUSION_HPP
#define PROGRAMM_TWODIMDIFFUSION_HPP

#include "NDVector.hpp"
#include "plot.hpp"

/**
 * An algorithm to solve the three dimensional diffusion equation with rotational symmetry: <BR>
 * du/dt = kappa0 * (d^2u/dr^2 + 1/r du/dr + d^2u/dz^2) <BR>
 * The initial values and boundary conditions must be set in u.
 * @relatesalso NDVector, NDNumerics
 */
namespace twodim {
    /**
     * Greens function of the equation.
     * @param r, z, t parameter
     * @param kappa0 diffusion coefficient
     * @return exp(-(r^2 + z^2) / (4 t kappa0)) / (4 pi t kappa0)^(3/2)
     */
    NumType psi_theory(NumType_I &z, NumType_I &r, NumType_I &t, NumType_I &kappa0);

    /**
     * Calculate two dim diffusion numerical.
     * Uses Crank-Nicolson method in r and z, and an Semi-Lanrangian scheme for the du/dt = -kappa0/r du/dr part.
     * @param[in, out] u the function u to be calculated. Must have the initial values at beginning, will be returned with
     *      calculated values after steps*dt
     * @param[in] dr, dz, dt step size
     * @param[in] steps count of steps to calculate
     * @param[in] kappa0 diffusion coefficient
     * @param[in] r the variable r, needed in the equation
     */
    void calculate_steps(VecND &u, NumType_I &dr, NumType_I &dz, NumType_I &dt, const int &steps, NumType_I &kappa0,
                         VecNum_I &r);

    /**
     * Generates .csv files for the plots of two dimensional diffusion.
     * @param dr, dz, dt step size
     * @param steps count of steps to calculate
     * @param offset count of steps to start calculation
     * @param length_r, length_z length in r/z direction
     * @param kappa0 diffusion coefficient
     * @param file_appendix the appendix to the file to identify different plots
     */
    void plot(NumType_I &dr, NumType_I &dz, NumType_I &dt, const int &steps, const int &offset, NumType_I &length_r,
              NumType_I &length_z, NumType_I &kappa0, const std::string &file_appendix = "");

    /**
     * Calculates the difference of the theory and the Numerical method at the different time steps.
     * @param steps NumVector of the different step counts, is casted into int
     * @param dr, dz, dt step size
     * @param offset count of steps to start calculation
     * @param length_r, length_z length in r/z direction
     * @param kappa0 diffusion coefficient
     * @return vector with the maximum absolute difference at the time steps
     */
    VecNum error(VecNum_I &steps, NumType_I &dr, NumType_I &dz, NumType_I &dt, const int &offset,
                 NumType_I &length_r, NumType_I &length_z, NumType_I &kappa0);

    /**
     * Makes the plot for the differences at different parameters.
     */
    void error_plot();
}
#endif //PROGRAMM_TWODIMDIFFUSION_HPP
