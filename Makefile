CPPC=h5c++
CPPC_FLAGS=-Wall -Wextra -pedantic -O3 -std=c++11 -fopenmp
CPPC_LINKER_FLAGS=
OUT_FILES=own_error.o plot.o tridiag.o NDNumerics.o OneDimDiffusion.o TwoDimDiffusion.o ThreeDimDiffusion.o CRDiffusion.o

all: $(OUT_FILES) main.exe

start: main.exe
	./$<

%.o: %.cpp
	$(CPPC) $(CPPC_FLAGS) -c $<

%.exe: %.cpp $(OUT_FILES)
	$(CPPC) $(CPPC_FLAGS) -o $@ $< -L. $(OUT_FILES) $(CPPC_LINKER_FLAGS)

clean:
	rm -f *.exe
	rm -f *.out
	rm -f *.o

clean_plots:
	rm -f *.csv
	rm -f *.hdf5
	rm -f *.pdf
	rm -f output.*.txt

.PHONY: start all clean clean_plots

