//
// Created by flosc on 08.05.2020.
//

#include "NDVector.hpp"
#include <iostream>
#include <algorithm>

template<class T>
NDVector<T>::NDVector(NDVector::SizeType_I &dimension, NDVector::IndexType_I &size, const T &value): dimension(
        dimension), size(size) {
    if (dimension != size.size()) THROW("Dimension of the Vector doesn't fit.");
    SizeType complete_size = 1;
    for (auto &i: size) complete_size *= i;
    if (dimension == 0) complete_size = 0;
    values = VecType(complete_size, value);
}

template<class T>
NDVector<T> &NDVector<T>::operator=(const std::initializer_list<T> &val) {
    values = val;
    dimension = 1;
    size = {values.size()};
    return *this;
}

template<class T>
NDVector<T> &NDVector<T>::operator=(NDVector::VecType_I &val) {
    values = val;
    dimension = 1;
    size = {values.size()};
    return *this;
}

template<class T>
T &NDVector<T>::operator[](NDVector::SizeType_I &index) {
    return values[index];
}

template<class T>
const T &NDVector<T>::operator[](NDVector::SizeType_I &index) const {
    return values[index];
}

template<class T>
T &NDVector<T>::operator[](NDVector::IndexType_I &index) {
    SizeType ind = 0;
    for (SizeType i = 0; i < dimension; i++) {
        ind = ind * size[i] + index[i];
    }
    return values[ind];
}

template<class T>
const T &NDVector<T>::operator[](NDVector::IndexType_I &index) const {
    SizeType ind = 0;
    for (SizeType i = 0; i < dimension; i++) {
        ind = ind * size[i] + index[i];
    }
    return values[ind];
}

template<class T>
T &NDVector<T>::at(int index) {
    if (index < 0) {
        index = values.size() + index;
    }
    if (index >= (int) values.size() || index < 0) THROW("Index out of bounds");
    return values[index];
}

template<class T>
const T &NDVector<T>::at(int index) const {
    if (index < 0) {
        index = values.size() + index;
    }
    if (index >= (int) values.size() || index < 0) THROW("Index out of bounds");
    return values[index];
}

template<class T>
T &NDVector<T>::at(const std::vector<int> &index) {
    if (index.size() != dimension) THROW("Index out of bounds");
    SizeType ind = 0;
    for (SizeType i = 0; i < dimension; i++) {
        int index_i = index[i];
        if (index_i < 0) {
            index_i = size[i] + index_i;
        }
        if (index_i >= (int) size[i] || index_i < 0) THROW("Index out of bounds");
        ind = ind * size[i] + index_i;
    }
    return values[ind];
}

template<class T>
const T &NDVector<T>::at(const std::vector<int> &index) const {
    if (index.size() != dimension) THROW("Index out of bounds");
    SizeType ind = 0;
    for (SizeType i = 0; i < dimension; i++) {
        int index_i = index[i];
        if (index_i < 0) {
            index_i = size[i] + index_i;
        }
        if (index_i >= (int) size[i] || index_i < 0) THROW("Index out of bounds");
        ind = ind * size[i] + index_i;
    }
    return values[ind];
}

template<class T>
T &NDVector<T>::at(NDVector::SizeType_I &index_in_subvector, NDVector::SizeType_I &subvector_index,
                   NDVector::IndexType_I &index) {
    SizeType ind = 0;
    for (SizeType i = 0; i < subvector_index; i++) {
        ind = ind * size[i] + index[i];
    }
    ind = ind * size[subvector_index] + index_in_subvector;
    for (SizeType i = subvector_index + 1; i < dimension; i++) {
        ind = ind * size[i] + index[i];
    }
    return values[ind];
}

template<class T>
const T &NDVector<T>::at(NDVector::SizeType_I &index_in_subvector, NDVector::SizeType_I &subvector_index,
                         NDVector::IndexType_I &index) const {
    SizeType ind = 0;
    for (SizeType i = 0; i < subvector_index; i++) {
        ind = ind * size[i] + index[i];
    }
    ind = ind * size[subvector_index] + index_in_subvector;
    for (SizeType i = subvector_index + 1; i < dimension; i++) {
        ind = ind * size[i] + index[i];
    }
    return values[ind];
}

template<class T>
T &NDVector<T>::at(const int &index_in_subvector, NDVector::SizeType_I &subvector_index,
                   NDVector::IndexType_I &index) {
    if (index.size() != dimension or subvector_index >= dimension) THROW("Index out of bounds");
    int index_subvector = index_in_subvector;
    if (index_subvector < 0) {
        index_subvector = size[subvector_index] + index_subvector;
    }
    if (index_subvector >= (int) size[subvector_index]) THROW("Index out of bounds");
    SizeType ind = 0;
    for (SizeType i = 0; i < subvector_index; i++) {
        ind = ind * size[i] + index[i];
        if (index[i] >= size[i]) THROW("Index out of bounds");
    }
    ind = ind * size[subvector_index] + index_subvector;
    for (SizeType i = subvector_index + 1; i < dimension; i++) {
        ind = ind * size[i] + index[i];
        if (index[i] >= size[i]) THROW("Index out of bounds");
    }
    return values[ind];
}

template<class T>
const T &NDVector<T>::at(const int &index_in_subvector, NDVector::SizeType_I &subvector_index,
                         NDVector::IndexType_I &index) const {
    if (index.size() != dimension or subvector_index >= dimension) THROW("Index out of bounds");
    int index_subvector = index_in_subvector;
    if (index_subvector < 0) {
        index_subvector = size[subvector_index] + index_subvector;
    }
    if (index_subvector >= (int) size[subvector_index]) THROW("Index out of bounds");
    SizeType ind = 0;
    for (SizeType i = 0; i < subvector_index; i++) {
        ind = ind * size[i] + index[i];
        if (index[i] >= size[i]) THROW("Index out of bounds");
    }
    ind = ind * size[subvector_index] + index_subvector;
    for (SizeType i = subvector_index + 1; i < dimension; i++) {
        ind = ind * size[i] + index[i];
        if (index[i] >= size[i]) THROW("Index out of bounds");
    }
    return values[ind];
}

template<class T>
std::size_t NDVector<T>::to_one_dim_index(NDVector::IndexType_I &index) const {
    SizeType ind = 0;
    for (SizeType i = 0; i < dimension; i++) {
        ind = ind * size[i] + index[i];
    }
    return ind;
}

template<class T>
void NDVector<T>::apply(const std::function<T(T)> &func) {
    transform(values.begin(), values.end(), values.begin(), func);
}

template<class T>
void NDVector<T>::apply_recursive_step(const std::vector<VecType> &x, const std::function<T(VecType)> &func,
                                       NDVector::SizeType dimension_index, NDVector::SizeType index, VecType val) {
    index *= size[dimension_index];
    for (SizeType i = 0; i < size[dimension_index]; i++) {
        val[dimension_index] = x[dimension_index][i];
        if (dimension_index < dimension - 1) {
            apply_recursive_step(x, func, dimension_index + 1, index + i, val);
        } else {
            values[index + i] = func(val);
        }
    }
}

template<class T>
void NDVector<T>::apply(const std::vector<NDVector::VecType> &x, const std::function<T(NDVector::VecType)> &func) {
    if (dimension == 0) {
        dimension = x.size();
        size = IndexType(dimension);
        SizeType complete_size = 1;
        for (SizeType i = 0; i < dimension; i++) {
            size[i] = x[i].size();
            complete_size *= x[i].size();
        }
        if (dimension == 0) complete_size = 0;
        values = VecType(complete_size);
    } else {
        if (dimension != x.size()) THROW("Dimension of the Vector doesn't fit.");
        for (SizeType i = 0; i < dimension; i++) {
            if (size[i] != x[i].size()) THROW("Dimension of the Vector doesn't fit.");
        }
    }
    VecType val(dimension);
    apply_recursive_step(x, func, 0, 0, val);
}

template<class T>
NDVector<T> NDVector<T>::operate(const NDVector<T> &x, const NDVector<T> &y, const std::function<T(T, T)> &func) {
    if (x.dimension != y.dimension) THROW("Dimensions of the Vectors doesn't fit.");
    for (SizeType i = 0; i < x.dimension; i++) {
        if (x.size[i] != y.size[i]) THROW("Dimensions of the Vectors doesn't fit.");
    }
    NDVector<T> ret(x.dimension, x.size);
    transform(x.values.begin(), x.values.end(), y.values.begin(), ret.values.begin(), func);
    return ret;
}

template<class T>
std::vector<T> NDVector<T>::subvector(NDVector::SizeType_I &subvector_index, NDVector::IndexType_I &index) const {
    VecType ret(size[subvector_index]);
    SizeType ind = 0;
    SizeType add_size = 1;
    for (SizeType i = 0; i < subvector_index; i++) {
        ind = ind * size[i] + index[i];
    }
    ind = ind * size[subvector_index];
    for (SizeType i = subvector_index + 1; i < dimension; i++) {
        ind = ind * size[i] + index[i];
        add_size *= size[i];
    }
    for (SizeType i = 0; i < size[subvector_index]; i++) {
        ret[i] = values[ind];
        ind += add_size;
    }
    return ret;
}

template<class T>
void NDVector<T>::set_subvector(NDVector::VecType_I &subvector, NDVector::SizeType_I &subvector_index,
                                NDVector::IndexType_I &index) {
    SizeType ind = 0;
    SizeType add_size = 1;
    for (SizeType i = 0; i < subvector_index; i++) {
        ind = ind * size[i] + index[i];
    }
    ind = ind * size[subvector_index];
    for (SizeType i = subvector_index + 1; i < dimension; i++) {
        ind = ind * size[i] + index[i];
        add_size *= size[i];
    }
    for (SizeType i = 0; i < size[subvector_index]; i++) {
        values[ind] = subvector[i];
        ind += add_size;
    }
}

template<class T>
void NDVector<T>::apply_subvector(const std::function<void(NDVector<T> &, SizeType_I &, IndexType_I &)> &func,
                                  NDVector::SizeType_I &subvector_index, NDVector::SizeType dimension_index,
                                  NDVector::IndexType index) {
    if (index.size() != dimension) index = IndexType(dimension, 0);
    if (dimension_index == dimension) {
        func((*this), subvector_index, index);
    } else if (dimension_index == subvector_index) {
        apply_subvector(func, subvector_index, dimension_index + 1, index);
    } else {
        for (SizeType i = 0; i < size[dimension_index]; i++) {
            index[dimension_index] = i;
            apply_subvector(func, subvector_index, dimension_index + 1, index);
        }
    }
}

template<class T>
void NDVector<T>::print(NDVector::SizeType_I &max_print_size) const {
    if (size.size() != dimension) {
        std::cout << "Attention: size.size() != dimension!!";
    }
    if (dimension == 1 && size[0] <= max_print_size) {
        std::cout << "{";
        for (auto &x: values) std::cout << x << ", ";
        if (values.size() > 1) std::cout << '\b' << '\b';
        std::cout << "}" << std::endl;
    } else if (dimension == 2 && size[0] <= max_print_size && size[1] <= max_print_size) {
        std::cout << "{";
        for (SizeType i = 0; i < size[0]; i++) {
            std::cout << "{";
            for (SizeType j = 0; j < size[1]; j++) std::cout << values[i * size[1] + j] << ", ";
            if (size[1] > 1) std::cout << '\b' << '\b';
            std::cout << "}";
            if (i < size[0] - 1) std::cout << ";\n ";
        }
        std::cout << "}" << std::endl;
    } else {
        std::cout << "NDVector of dimension " << dimension << " with size {";
        for (auto &x: size) std::cout << x << ", ";
        if (size.size() > 1) std::cout << '\b' << '\b';
        std::cout << "}, values: {";
        if (values.size() <= max_print_size or max_print_size == 0) {
            for (auto const &value: values) std::cout << value << "; ";
        } else {
            for (SizeType i = 0; i < 3; i++) std::cout << values[i] << "; ";
            std::cout << "[" << values.size() - 6 << "]; ";
            for (SizeType i = values.size() - 3; i < values.size(); i++) std::cout << values[i] << "; ";
        }
        if (values.size() > 1) std::cout << '\b' << '\b';
        std::cout << "}" << std::endl;
    }
}
