//
// Created by flosc on 11.05.2020.
//

#include "TwoDimDiffusion.hpp"
#include "NDNumerics.hpp"
#include "parameters.hpp"
#include <cmath>
#include <sstream>

using namespace std;
using namespace plot;
using namespace NDNumerics;

NumType twodim::psi_theory(NumType_I &z, NumType_I &r, NumType_I &t, NumType_I &kappa0) {
    return exp(-(r * r + z * z) / (4.0 * t * kappa0)) / pow(4.0 * M_PI * t * kappa0, 3.0 / 2.0);
}

void twodim::calculate_steps(VecND &u, NumType_I &dr, NumType_I &dz, NumType_I &dt, const int &steps, NumType_I &kappa0,
                             VecNum_I &r) {
    ///du/dt = kappa/r du/dr part
    auto kappa_r_func = [&r, &kappa0](VecND &v_in, SizeType_I &subvector_index, IndexType_I &index) {
        for (SizeType j = 1; j < v_in.get_size(subvector_index); j++) {
            v_in.at(j, subvector_index, index) = -kappa0 / r[j];
        }
        v_in.at(0, subvector_index, index) = v_in.at(1, subvector_index, index);
    };
    VecND kappa_r(u.get_dimension(), u.get_size());
    kappa_r.apply_subvector(kappa_r_func, 1);

    for (int i = 0; i < steps; i++) {
        diffusion_crank_nicolson(u, dz, dt, kappa0, 0, 0);
        diffusion_crank_nicolson(u, dr, dt, kappa0, 1, 1);
//        diffusion_lax_wendroff(u, dr, dt, kappa0, r, 1); ///Old version
        NDNumerics::semi_lagrangian(u, dr, dt, kappa_r, 1, 0);
    }
}

void twodim::plot(NumType_I &dr, NumType_I &dz, NumType_I &dt, const int &steps, const int &offset,
                  NumType_I &length_r, NumType_I &length_z, NumType_I &kappa0, const string &file_appendix) {
    VecNum r = arange(0, length_r, dr);
    VecNum z = arange(-length_z, length_z, dz);
    SizeType index_z0 = (z.size() / 2);

    VecND u;
    function<NumType(VecNum_I &)> f0 = [&dt, &kappa0, &offset](VecNum_I &val) {
        return psi_theory(val[0], val[1], offset * dt, kappa0);
    };
    u.apply({z, r}, f0);
    for (std::size_t i = 0; i < r.size(); i++) {
        u[{0, i}] = 0;
        u.at({-1, (int) i}) = 0;
    }
//    save_as_csv(r, u.subvector(1, {index_z0, 0}), "TwoDimDiff_r_u0" + file_appendix+ ".csv", "r", "u(z=0)", "u0");
//    save_as_csv(z, u.subvector(0, {0, 0}), "TwoDimDiff_z_u0" + file_appendix+ ".csv", "z", "u(r=0)", "u0");

    VecND ut;
    function<NumType(VecNum_I &)> ft = [&dt, &steps, &offset, &kappa0](VecNum_I &val) {
        return psi_theory(val[0], val[1], dt * (steps + offset), kappa0);
    };
    ut.apply({z, r}, ft);
    save_as_csv(r, ut.subvector(1, {index_z0, 0}), "TwoDimDiff_r_theory" + file_appendix + ".csv",
                "r", "u(z=0)", "Theory");
    save_as_csv(z, ut.subvector(0, {0, 0}), "TwoDimDiff_z_theory" + file_appendix + ".csv",
                "z", "u(r=0)", "Theory");

    twodim::calculate_steps(u, dr, dz, dt, steps, kappa0, r);
    save_as_csv(r, u.subvector(1, {index_z0, 0}), "TwoDimDiff_r_numerical" + file_appendix + ".csv",
                "r", "u(z=0)", "Numerical");
    save_as_csv(z, u.subvector(0, {0, 0}), "TwoDimDiff_z_numerical" + file_appendix + ".csv",
                "z", "u(r=0)", "Numerical");

    ///Calculate the error
    NumType max_value = abs(vec_max(ut.get_values()));
    function<NumType(NumType_I &, NumType_I &)> func_diff;
    VecND diff;
    ///absolute difference
//    func_diff = [](NumType_I &a, NumType_I &b) { return abs(a - b); };
//    diff = VecND::operate(ut, u, func_diff);
//    save_as_csv(r, diff.subvector(1, {index_z0, 0}), "TwoDimDiff_r_abs" + file_appendix + "_error.csv",
//                "r", "$\\Delta u(z=0)$", "absolute difference");
//    save_as_csv(z, diff.subvector(0, {0, 0}), "TwoDimDiff_z_abs" + file_appendix + "_error.csv",
//                "z", "$\\Delta u(r=0)$", "absolute difference");
    ///relative difference
//    func_diff = [](NumType_I &a, NumType_I &b) { return abs(a - b) / abs(a); };
//    diff = VecND::operate(ut, u, func_diff);
//    save_as_csv(r, diff.subvector(1, {index_z0, 0}), "TwoDimDiff_r_rel" + file_appendix + "_error.csv",
//                "r", "$\\Delta u(z=0)$", "relative difference");
//    save_as_csv(z, diff.subvector(0, {0, 0}), "TwoDimDiff_z_rel" + file_appendix + "_error.csv",
//                "z", "$\\Delta u(r=0)$", "relative difference");
    ///absolute difference / max_value
    func_diff = [&max_value](NumType_I &a, NumType_I &b) { return abs(a - b) / max_value; };
    diff = VecND::operate(ut, u, func_diff);
    save_as_csv(r, diff.subvector(1, {index_z0, 0}), "TwoDimDiff_r_absrel" + file_appendix + "_error.csv",
                "r", "$\\Delta u(z=0)$", "absolute difference / max_value");
    save_as_csv(z, diff.subvector(0, {0, 0}), "TwoDimDiff_z_absrel" + file_appendix + "_error.csv",
                "z", "$\\Delta u(r=0)$", "absolute difference / max_value");
}

VecNum twodim::error(VecNum_I &steps, NumType_I &dr, NumType_I &dz, NumType_I &dt, const int &offset,
                     NumType_I &length_r, NumType_I &length_z, NumType_I &kappa0) {
    VecNum r = arange(0, length_r, dr);
    VecNum z = arange(-length_z, length_z, dz);

    VecND u;
    function<NumType(VecNum_I &)> f0 = [&dt, &offset, &kappa0](VecNum_I &val) {
        return psi_theory(val[0], val[1], offset * dt, kappa0);
    };
    u.apply({z, r}, f0);
    for (std::size_t i = 0; i < r.size(); i++) {
        u[{0, i}] = 0;
        u.at({-1, (int) i}) = 0;
    }

    VecNum ret_diff;
    int step, last_step = 0;
    for (auto &step_num: steps) {
        step = (int) step_num;
        VecND ut;
        function<NumType(VecNum_I &)> ft = [&dt, &step, &offset, &kappa0](VecNum_I &val) {
            return psi_theory(val[0], val[1], dt * (step + offset), kappa0);
        };
        ut.apply({z, r}, ft);

        twodim::calculate_steps(u, dr, dz, dt, step - last_step, kappa0, r);

//        ret_diff.push_back(vec_max(difference(ut.get_values(), u.get_values()))); ///absolute difference
//        ret_diff.push_back(vec_max(relative_difference(ut.get_values(), u.get_values()))); ///relative difference
        ret_diff.push_back(max_difference_relative(ut.get_values(), u.get_values())); ///max difference relative

        last_step = step;
    }
    return ret_diff;
}

void twodim::error_plot() {
    const int offset = ERROR_OFFSET;
    const NumType length_r = ERROR_LENGTH_R;
    const NumType length_z = ERROR_LENGTH_Z;
    VecNum steps, dr_steps, dz_steps, dt_steps;
    ///Plot as function of steps
    steps = ERROR_STEPS;
    dt_steps = ERROR_STEPS_DT;
    dr_steps = ERROR_STEPS_DR;
    dz_steps = ERROR_STEPS_DZ;
    for (auto &dt: dt_steps) {
        for (auto &dr: dr_steps) {
            for (auto &dz: dz_steps) {
                VecNum diff = error(steps, dr, dz, dt, offset, length_r, length_z, 1);
                ostringstream file, label;
                file << "TwoDimDiffError_steps_dr" << dr << "_dz" << dz << "_dt" << dt << ".csv";
                label << "dr = " << dr << ", dz = " << dz << ", dt = " << dt;
                save_as_csv(steps, diff, file.str(), "steps", "$\\Delta u$", label.str());
            }
        }
    }
    ///Plot as function of dt
    dt_steps = ERROR_DT;
    steps = {ERROR_DT_STEP};
    dr_steps = ERROR_DT_DR;
    dz_steps = ERROR_DT_DZ;
    for (auto &dr: dr_steps) {
        for (auto &dz: dz_steps) {
            VecNum diff;
            for (auto &dt: dt_steps) {
                diff.push_back(error(steps, dr, dz, dt, offset, length_r, length_z, 1)[0]);
            }
            ostringstream file, label;
            file << "TwoDimDiffError_dt_dr" << dr << "_dz" << dz << ".csv";
            label << "dr = " << dr << ", dz = " << dz;
            save_as_csv(dt_steps, diff, file.str(), "dt", "$\\Delta u$", label.str());
        }
    }
}