//
// Created by Flo on 04.05.2020.
//

#include "OneDimDiffusion.hpp"
#include "NDNumerics.hpp"
#include "parameters.hpp"
#include <cmath>
#include <sstream>

using namespace std;
using namespace plot;
using namespace NDNumerics;

NumType onedim::psi_theory(NumType_I &x, NumType_I &t, NumType_I &kappa0) {
    return exp(-x * x / (4 * t * kappa0)) / (2 * sqrt(M_PI * t * kappa0));
}

NumType onedim::psi_r_theory(NumType_I &x, NumType_I &t, NumType_I &kappa0) {
    return exp(-x * x / (4 * t * kappa0)) / (4 * M_PI * t * kappa0);
}

void
onedim::plot(NumType_I &dx, NumType_I &dt, const int &steps, const int &offset, NumType_I &length, NumType_I &kappa0,
             const string &file_appendix) {
    VecNum x = arange(-length, length, dx);

    VecND u0;
    function<NumType(VecNum_I &)> f0 = [&dt, &offset, &kappa0](VecNum_I &val) {
        return psi_theory(val[0], offset * dt, kappa0);
    };
    u0.apply({x}, f0);
    u0[0] = 0;
    u0.at(-1) = 0;
    VecND u;

    VecND ut;
    function<NumType(VecNum_I &)> ft = [&dt, &steps, &offset, &kappa0](VecNum_I &val) {
        return psi_theory(val[0], dt * (steps + offset), kappa0);
    };
    ut.apply({x}, ft);
    save_as_csv(x, ut.subvector(0, {0}), "OneDimDiff_theory" + file_appendix + ".csv",
                "x", "u(x)", "Theory");

    u = u0;
    for (int i = 0; i < steps; i++) {
        diffusion_crank_nicolson(u, dx, dt, kappa0, 0);
    }
    save_as_csv(x, u.subvector(0, {0}), "OneDimDiff_numerical" + file_appendix + ".csv",
                "x", "u(x)", "Numerical");

    ///Calculate the error
    NumType max_value = abs(vec_max(ut.get_values()));
    function<NumType(NumType_I &, NumType_I &)> func_diff;
    VecND diff;
    ///absolute difference
//    func_diff = [](NumType_I &a, NumType_I &b) { return abs(a - b); };
//    diff = VecND::operate(ut, u, func_diff);
//    save_as_csv(x, diff.subvector(0, {0}), "OneDimDiff_abs" + file_appendix + "_error.csv",
//                "x", "$\\Delta$ u(x)", "absolute difference");
    ///relative difference
//    func_diff = [](NumType_I &a, NumType_I &b) { return abs(a - b) / abs(a); };
//    diff = VecND::operate(ut, u, func_diff);
//    save_as_csv(x, diff.subvector(0, {0}), "OneDimDiff_rel" + file_appendix + "_error.csv",
//                "x", "$\\Delta$ u(x)", "relative difference");
    ///absolute difference / max_value
    func_diff = [&max_value](NumType_I &a, NumType_I &b) { return abs(a - b) / max_value; };
    diff = VecND::operate(ut, u, func_diff);
    save_as_csv(x, diff.subvector(0, {0}), "OneDimDiff_absrel" + file_appendix + "_error.csv",
                "x", "$\\Delta$ u(x)", "absolute difference / max_value");
}

void
onedim::plot_r(NumType_I &dx, NumType_I &dt, const int &steps, const int &offset, NumType_I &length, NumType_I &kappa0,
               const string &file_appendix) {
    VecNum x = arange(0, length, dx);

    VecND u0;
    function<NumType(VecNum_I &)> f0 = [&dt, &offset, &kappa0](VecNum_I &val) {
        return psi_r_theory(val[0], offset * dt, kappa0);
    };
    u0.apply({x}, f0);
    u0.at(-1) = 0;
    VecND u;

    /// kappa / r for the first order term.
    auto kappa_r_func = [&x, &kappa0](VecND &v_in, SizeType_I &subvector_index, IndexType_I &index) {
        for (SizeType j = 1; j < v_in.get_size(subvector_index); j++) {
            v_in.at(j, subvector_index, index) = -kappa0 / x[j];
        }
        v_in.at(0, subvector_index, index) = v_in.at(1, subvector_index, index);
    };
    VecND kappa_r(u0.get_dimension(), u0.get_size());
    kappa_r.apply_subvector(kappa_r_func, 0);

    VecND ut;
    function<NumType(VecNum_I &)> ft = [&dt, &steps, &offset, &kappa0](VecNum_I &val) {
        return psi_r_theory(val[0], dt * (steps + offset), kappa0);
    };
    ut.apply({x}, ft);
    save_as_csv(x, ut.subvector(0, {0}), "OneDimDiffR_theory" + file_appendix + ".csv", "x", "u(x)", "Theory");

    u = u0;
    for (int i = 0; i < steps; i++) {
        diffusion_crank_nicolson(u, dx, dt, kappa0, 0, 1);
//        diffusion_lax_wendroff(u, dx, dt, kappa0, x, 0); ///Old method
        NDNumerics::semi_lagrangian(u, dx, dt, kappa_r, 0);
    }
    save_as_csv(x, u.subvector(0, {0}), "OneDimDiffR_numerical" + file_appendix + ".csv", "x", "u(x)",
                "Numerical");

    ///Calculate the error
    NumType max_value = abs(vec_max(ut.get_values()));
    function<NumType(NumType_I &, NumType_I &)> func_diff;
    VecND diff;
    ///absolute difference
//    func_diff = [](NumType_I &a, NumType_I &b) { return abs(a - b); };
//    diff = VecND::operate(ut, u, func_diff);
//    save_as_csv(x, diff.subvector(0, {0}), "OneDimDiffR_abs" + file_appendix + "_error.csv",
//                "x", "$\\Delta$ u(x)", "absolute difference");
    ///relative difference
//    func_diff = [](NumType_I &a, NumType_I &b) { return abs(a - b) / abs(a); };
//    diff = VecND::operate(ut, u, func_diff);
//    save_as_csv(x, diff.subvector(0, {0}), "OneDimDiffR_rel" + file_appendix + "_error.csv",
//                "x", "$\\Delta$ u(x)", "relative difference");
    ///absolute difference / max_value
    func_diff = [&max_value](NumType_I &a, NumType_I &b) { return abs(a - b) / max_value; };
    diff = VecND::operate(ut, u, func_diff);
    save_as_csv(x, diff.subvector(0, {0}), "OneDimDiffR_absrel" + file_appendix + "_error.csv",
                "x", "$\\Delta$ u(x)", "absolute difference / max_value");
}

VecNum
onedim::error(VecNum_I &steps, NumType_I &dx, NumType_I &dt, const int &offset, NumType_I &length, NumType_I &kappa0) {
    VecNum x = arange(-length, length, dx);

    VecND u;
    function<NumType(VecNum_I &)> f0 = [&dt, &offset, &kappa0](VecNum_I &val) {
        return psi_theory(val[0], offset * dt, kappa0);
    };
    u.apply({x}, f0);
    u[0] = 0;
    u.at(-1) = 0;

    VecNum ret_diff;
    int step, last_step = 0;
    for (auto &step_num: steps) {
        step = (int) step_num;
        VecND ut;
        function<NumType(VecNum_I &)> ft = [&dt, &step, &offset, &kappa0](VecNum_I &val) {
            return psi_theory(val[0], dt * (step + offset), kappa0);
        };
        ut.apply({x}, ft);

        for (int i = last_step; i < step; i++) {
            diffusion_crank_nicolson(u, dx, dt, kappa0, 0);
        }

//        ret_diff.push_back(vec_max(difference(ut.get_values(), u.get_values()))); ///Absolute difference
//        ret_diff.push_back(vec_max(relative_difference(ut.get_values(), u.get_values()))); ///relative difference
        ret_diff.push_back(max_difference_relative(ut.get_values(), u.get_values())); ///max difference relative

        last_step = step;
    }
    return ret_diff;
}


VecNum
onedim::error_r(VecNum_I &steps, NumType_I &dx, NumType_I &dt_in, const int &offset, NumType_I &length,
                NumType_I &kappa0) {
    VecNum x = arange(0, length, dx);
    NumType dt = dt_in; ///for different dt

    VecND u;
    function<NumType(VecNum_I &)> f0 = [&dt_in, &offset, &kappa0](VecNum_I &val) {
        return psi_r_theory(val[0], offset * dt_in, kappa0);
    };
    u.apply({x}, f0);
    u.at(-1) = 0;

    /// kappa / r for the first order term.
    auto kappa_r_func = [&x, &kappa0](VecND &v_in, SizeType_I &subvector_index, IndexType_I &index) {
        for (SizeType j = 1; j < v_in.get_size(subvector_index); j++) {
            v_in.at(j, subvector_index, index) = -kappa0 / x[j];
        }
        v_in.at(0, subvector_index, index) = v_in.at(1, subvector_index, index);
    };
    VecND kappa_r(u.get_dimension(), u.get_size());
    kappa_r.apply_subvector(kappa_r_func, 0);

    VecNum ret_diff;
    int step, last_step = 0;
    NumType t_last = dt * offset;
    for (auto &step_num: steps) {
        step = (int) step_num;
//        int steps_todo = round(((step_num + offset) * dt_in - t_last) / dt);
        int steps_todo = step - last_step;
//        cout << step_num << " " << step - last_step << " " << steps_todo << " " << dt << " " << t_last << " " << endl;
        VecND ut;
        function<NumType(VecNum_I &)> ft = [&dt, &steps_todo, &t_last, &kappa0](VecNum_I &val) {
            return psi_r_theory(val[0], dt * steps_todo + t_last, kappa0);
        };
        ut.apply({x}, ft);

        for (int i = 0; i < steps_todo; i++) {
            diffusion_crank_nicolson(u, dx, dt, kappa0, 0, 1);
//            diffusion_lax_wendroff(u, dx, dt, kappa0, x, 0); ///Old method
            NDNumerics::semi_lagrangian(u, dx, dt, kappa_r, 0);
        }

//        ret_diff.push_back(vec_max(difference(ut.get_values(), u.get_values()))); ///Absolute difference
//        ret_diff.push_back(vec_max(relative_difference(ut.get_values(), u.get_values()))); ///relative difference
        ret_diff.push_back(max_difference_relative(ut.get_values(), u.get_values())); ///max difference relative

        last_step = step;
        t_last += dt * steps_todo;
//        if (steps_todo >= 100) {
//            dt *= 2;
//            cout << "Increased dt " << dt << endl;
//        }
    }
    return ret_diff;

}

void onedim::error_plot() {
    const int offset = ERROR_OFFSET;
    const NumType length_r = ERROR_LENGTH_R;
    const NumType length_z = ERROR_LENGTH_Z;
    VecNum steps, dr_steps, dz_steps, dt_steps;
    ///Plot as function of steps
    steps = ERROR_STEPS;
    dt_steps = ERROR_STEPS_DT;
    dr_steps = ERROR_STEPS_DR;
    dz_steps = ERROR_STEPS_DZ;
    for (auto &dt: dt_steps) {
        for (auto &dx: dr_steps) {
            ostringstream file, label;
            file << "steps_dx" << dx << "_dt" << dt << ".csv";
            label << "dx = " << dx << ", dt = " << dt;

            VecNum diff = error_r(steps, dx, dt, offset, length_r, 1);
            save_as_csv(steps, diff, "OneDimDiffRError_" + file.str(), "steps", "$\\Delta u$", label.str());
        }
        for (auto &dx: dz_steps) {
            ostringstream file, label;
            file << "steps_dx" << dx << "_dt" << dt << ".csv";
            label << "dx = " << dx << ", dt = " << dt;

            VecNum diff = error(steps, dx, dt, offset, length_z, 1);
            save_as_csv(steps, diff, "OneDimDiffError_" + file.str(), "steps", "$\\Delta u$", label.str());
        }
    }
    ///Plot as function of dt
    dt_steps = ERROR_DT;
    steps = {ERROR_DT_STEP};
    dr_steps = ERROR_DT_DR;
    dz_steps = ERROR_DT_DZ;
    for (auto &dx: dr_steps) {
        VecNum diff;
        for (auto &dt: dt_steps) {
            diff.push_back(error_r(steps, dx, dt, offset, length_r, 1)[0]);
        }
        ostringstream file, label;
        file << "dt_dx" << dx << ".csv";
        label << "dx = " << dx;
        save_as_csv(dt_steps, diff, "OneDimDiffRError_" + file.str(), "dt", "$\\Delta u$", label.str());
    }
    for (auto &dx: dz_steps) {
        VecNum diff;
        for (auto &dt: dt_steps) {
            diff.push_back(error(steps, dx, dt, offset, length_r, 1)[0]);
        }
        ostringstream file, label;
        file << "dt_dx" << dx << ".csv";
        label << "dx = " << dx;
        save_as_csv(dt_steps, diff, "OneDimDiffError_" + file.str(), "dt", "$\\Delta u$", label.str());
    }
}

void onedim::non_diffusive_plot(NumType_I &dx, NumType_I &dt, const int &steps, NumType_I &length,
                                NumType_I &v, NumType_I &epsilon, const std::string &file_appendix) {
    VecNum x = arange(-length, length, dx);

    VecND u0;
    function<NumType(VecNum_I &)> f0 = [&epsilon](VecNum_I &val) { return dirac_delta(val[0], epsilon); };
    u0.apply({x}, f0);
    u0[0] = 0;
    u0.at(-1) = 0;
    VecND u = u0;
    VecND velocity(u.get_dimension(), u.get_size(), v);

    VecND ut;
    function<NumType(VecNum_I &)> ft = [&dt, &steps, &v, & epsilon](VecNum_I &val) {
        return dirac_delta(val[0] - v * (dt * steps), epsilon);
    };
    ut.apply({x}, ft);
    save_as_csv(x, ut.subvector(0, {0}), "NonDiffusive_Theory" + file_appendix + ".csv", "x", "u(x)", "Theory");

//    for (int i = 0; i < steps; i++) {
//        lax_method(u, dx, dt, v, 0);
//    }
//    save_as_csv(x, u.subvector(0, {0}), "NonDiffusive_Lax" + file_appendix + ".csv", "x", "u(x)", "Lax");

//    u = u0;
//    VecND un;
//    VecND um;
//    function<NumType(VecNum_I &)> fm = [&dt, &steps, &v](VecNum_I &val) {
//        return non_diffusive_theory(val[0], -dt, v);
//    };
//    um.apply({x}, fm);
//    for (int i = 0; i < steps; i++) {
//        un = u;
//        staggered_leapfrog(u, um, dx, dt, v, 0);
//        um = un;
//    }
//    save_as_csv(x, u.subvector(0, {0}), "NonDiffusive_SL" + file_appendix + ".csv", "x", "u(x)", "staggered leapfrog");

//    u = u0;
//    for (int i = 0; i < steps; i++) {
//        lax_wendroff(u, dx, dt, v, 0);
//    }
//    save_as_csv(x, u.subvector(0, {0}), "NonDiffusive_LW" + file_appendix + ".csv", "x", "u(x)", "Lax-Wendroff");

//    u = u0;
//    for (int i = 0; i < steps; i++) {
//        upwind(u, dx, dt, velocity, 0);
//    }
//    save_as_csv(x, u.subvector(0, {0}), "NonDiffusive_upwind" + file_appendix + ".csv", "x", "u(x)", "Upwind");

    u = u0;
    for (int i = 0; i < steps; i++) {
        mpdata(u, dx, dt, velocity, 0);
    }
    save_as_csv(x, u.subvector(0, {0}), "NonDiffusive_mpdata" + file_appendix + ".csv", "x", "u(x)", "mpdata");

//    u = u0;
//    for (int i = 0; i < steps; i++) {
//        semi_lagrangian(u, dx, dt, velocity, 0);
//    }
//    save_as_csv(x, u.subvector(0, {0}), "NonDiffusive_semi_lagrangian" + file_appendix + ".csv", "x", "u(x)",
//                "Semi Lagrangian");

//    u = u0;
//    for (int i = 0; i < steps; i++) {
//        semi_lagrangian_2(u, dx, dt, velocity, 0);
//    }
//    save_as_csv(x, u.subvector(0, {0}), "NonDiffusive_semi_lagrangian_2" + file_appendix + ".csv", "x", "u(x)",
//                "Semi Lagrangian 2");
}
