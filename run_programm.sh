#!/usr/local_rwth/bin/zsh

# name the job
#SBATCH --job-name=CRDiffusion

# output file
#SBATCH --output=output.%J.txt

# time limit
#SBATCH --time=0-05:00:00

# CPUs
#SBATCH --cpus-per-task=20

# memory space
##SBATCH --mem-per-cpu=2G

### beginning of the executable commands
module load gcc/9; module load LIBRARIES; module load hdf5
make all
$MPIEXEC $FLAGS_MPI_BATCH ./main.exe
