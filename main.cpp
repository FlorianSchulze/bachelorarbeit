//
// Created by Flo on 01.05.2020.
//

#include "parameters.hpp"
#include "own_error.hpp"
#include "NDVector.hpp"
#include "plot.hpp"
#include "OneDimDiffusion.hpp"
#include "TwoDimDiffusion.hpp"
#include "ThreeDimDiffusion.hpp"
#include "CRDiffusion.hpp"
#include <iostream>
#include <cmath>
#include <ctime>
#include <hdf5.h>
#include <omp.h>

using namespace std;
using namespace plot;

///Print out the parameters
void print_parameters() {
    printf("p0:\t\t %f MeV\n", P0);
    printf("tau:\t\t %f Myr\n", TAU);
    printf("lambda:\t\t %f kpc\n\n", LAMBDA);

//    printf("R:\t\t %f\n", R_OUT);
//    printf("L:\t\t %f\n", L);
//    printf("h:\t\t %f\n\n", H);
//
//    printf("dr:\t\t %e\n", DR);
//    printf("dz:\t\t %e\n", DZ);
//    printf("dlog_p:\t\t %e\n", DLOG_P);
//    printf("dt:\t\t %e\n\n", DT);

//    printf("log_t_start:\t %f\n", LOG_T_START);
//    printf("log_t_end:\t %f\n", LOG_T_END);
//    printf("log_p_start:\t %f\n", LOG_P_START);
//    printf("log_p_end:\t %f\n\n", LOG_P_END);

//    printf("steps:\t\t %e\n", STEPS);
//    printf("offset:\t\t %e\n\n", OFFSET);

//    printf("min steps t:\t %e\n", std::pow(10, LOG_T_START) / DT - OFFSET);
//    printf("max steps t:\t %e\n", std::pow(10, LOG_T_END) / DT - OFFSET);
//    printf("steps r:\t %e\n", R / DR);
//    printf("steps z:\t %e\n", 2. * L / DZ);
//    printf("steps log_p:\t %e\n", (LOG_P_END - LOG_P_START) / DLOG_P);
//    printf("steps test_log_p:%e\n", (TEST_LOG_P_END - TEST_LOG_P_START) / TEST_DLOG_P);

    printf("log_p_start:\t %f\n", LOG_P_START);
    printf("log_p_end:\t %f\n", LOG_P_END);
    printf("dlog_p:\t\t %e\n", DLOG_P);
    printf("steps log_p:\t %e\n", (LOG_P_END - LOG_P_START) / DLOG_P);

    //    printf("\nPart 1\n");
    //    printf("log_t_start:\t %f\n", LOG_T_START_1);
    //    printf("log_t_end:\t %f\n", LOG_T_END_1);
    //    printf("dt:\t\t %e\n", DT_1);
    ////    printf("R:\t\t %f\n", R_1);
    ////    printf("L:\t\t %f\n", L_1);
    ////    printf("dr:\t\t %e\n", DR_1);
    ////    printf("dz:\t\t %e\n", DZ_1);
    //    printf("offset:\t\t %d\n", OFFSET_1);
    //    printf("min steps t:\t %e\n", std::pow(10, LOG_T_START_1) / DT_1 - OFFSET_1);
    //    printf("max steps t:\t %e\n", std::pow(10, LOG_T_END_1) / DT_1 - OFFSET_1);
    //    printf("steps r:\t %e\n", R_1 / DR_1);
    //    printf("steps z:\t %e\n", 2. * L_1 / DZ_1);
    //
    //    printf("\nPart 2\n");
    //    printf("log_t_start:\t %f\n", LOG_T_START_2);
    //    printf("log_t_end:\t %f\n", LOG_T_END_2);
    //    printf("dt:\t\t %e\n", DT_2);
    ////    printf("R:\t\t %f\n", R_2);
    ////    printf("L:\t\t %f\n", L_2);
    ////    printf("dr:\t\t %e\n", DR_2);
    ////    printf("dz:\t\t %e\n", DZ_2);
    //    printf("min steps t:\t %e\n", (std::pow(10, LOG_T_START_2) - std::pow(10, LOG_T_END_1)) / DT_2);
    //    printf("max steps t:\t %e\n", (std::pow(10, LOG_T_END_2) - std::pow(10, LOG_T_END_1)) / DT_2);
    //    printf("steps r:\t %e\n", R_2 / DR_2);
    //    printf("steps z:\t %e\n", 2. * L_2 / DZ_2);
    //
    //    printf("\nPart 3\n");
    //    printf("log_t_start:\t %f\n", LOG_T_START_3);
    //    printf("log_t_end:\t %f\n", LOG_T_END_3);
    //    printf("dt:\t\t %e\n", DT_3);
    ////    printf("R:\t\t %f\n", R_3);
    ////    printf("L:\t\t %f\n", L_3);
    ////    printf("dr:\t\t %e\n", DR_3);
    ////    printf("dz:\t\t %e\n", DZ_3);
    //    printf("min steps t:\t %e\n", (std::pow(10, LOG_T_START_3) - std::pow(10, LOG_T_END_2)) / DT_3);
    //    printf("max steps t:\t %e\n", (std::pow(10, LOG_T_END_3) - std::pow(10, LOG_T_END_2)) / DT_3);
    //    printf("steps r:\t %e\n", R_3 / DR_3);
    //    printf("steps z:\t %e\n", 2. * L_3 / DZ_3);
    //
    //    printf("\nPart 4\n");
    //    printf("log_t_start:\t %f\n", LOG_T_START_4);
    //    printf("log_t_end:\t %f\n", LOG_T_END_4);
    //    printf("dt:\t\t %e\n", DT_4);
    ////    printf("R:\t\t %f\n", R_4);
    ////    printf("L:\t\t %f\n", L_4);
    ////    printf("dr:\t\t %e\n", DR_4);
    ////    printf("dz:\t\t %e\n", DZ_4);
    //    printf("min steps t:\t %e\n", (std::pow(10, LOG_T_START_4) - std::pow(10, LOG_T_END_3)) / DT_4);
    //    printf("max steps t:\t %e\n", (std::pow(10, LOG_T_END_4) - std::pow(10, LOG_T_END_3)) / DT_4);
    //    printf("steps r:\t %e\n", R_4 / DR_4);
    //    printf("steps z:\t %e\n", 2. * L_4 / DZ_4);
    //
    //    printf("\nPart 5\n");
    //    printf("log_t_start:\t %f\n", LOG_T_START_5);
    //    printf("log_t_end:\t %f\n", LOG_T_END_5);
    //    printf("dt:\t\t %e\n", DT_5);
    ////    printf("R:\t\t %f\n", R_5);
    ////    printf("L:\t\t %f\n", L_5);
    ////    printf("dr:\t\t %e\n", DR_5);
    ////    printf("dz:\t\t %e\n", DZ_5);
    //    printf("min steps t:\t %e\n", (std::pow(10, LOG_T_START_5) - std::pow(10, LOG_T_END_4)) / DT_5);
    //    printf("max steps t:\t %e\n", (std::pow(10, LOG_T_END_5) - std::pow(10, LOG_T_END_4)) / DT_5);
    //    printf("steps r:\t %e\n", R_5 / DR_5);
    //    printf("steps z:\t %e\n", 2. * L_5 / DZ_5);

    printf("\nPart \t\t   1\t\t   2\t\t   3\t\t   4\t\t   5\n");
    printf("log_t_start:\t %f\t %f\t %f\t %f\t %f\n", LOG_T_START_1, LOG_T_START_2, LOG_T_START_3, LOG_T_START_4,
           LOG_T_START_5);
    printf("log_t_end:\t %f\t %f\t %f\t %f\t %f\n", LOG_T_END_1, LOG_T_END_2, LOG_T_END_3, LOG_T_END_4, LOG_T_END_5);
    printf("dt:\t\t %e\t %e\t %e\t %e\t %e\n", DT_1, DT_2, DT_3, DT_4, DT_5);
    printf("min steps t:\t %e\t %e\t %e\t %e\t %e\n",
            std::pow(10, LOG_T_START_1) / DT_1 - OFFSET_1,
           (std::pow(10, LOG_T_START_2) - std::pow(10, LOG_T_END_1)) / DT_2,
           (std::pow(10, LOG_T_START_3) - std::pow(10, LOG_T_END_2)) / DT_3,
           (std::pow(10, LOG_T_START_4) - std::pow(10, LOG_T_END_3)) / DT_4,
           (std::pow(10, LOG_T_START_5) - std::pow(10, LOG_T_END_4)) / DT_5);
    printf("max steps t:\t %e\t %e\t %e\t %e\t %e\n",
            std::pow(10, LOG_T_END_1) / DT_1 - OFFSET_1,
           (std::pow(10, LOG_T_END_2) - std::pow(10, LOG_T_END_1)) / DT_2,
           (std::pow(10, LOG_T_END_3) - std::pow(10, LOG_T_END_2)) / DT_3,
           (std::pow(10, LOG_T_END_4) - std::pow(10, LOG_T_END_3)) / DT_4,
           (std::pow(10, LOG_T_END_5) - std::pow(10, LOG_T_END_4)) / DT_5);
    printf("steps r:\t %e\t %e\t %e\t %e\t %e\n", R_1 / DR_1, R_2 / DR_2, R_3 / DR_3, R_4 / DR_4, R_5 / DR_5);
    printf("steps z:\t %e\t %e\t %e\t %e\t %e\n", 2. * L_1 / DZ_1, 2. * L_2 / DZ_2, 2. * L_3 / DZ_3, 2. * L_4 / DZ_4,
           2. * L_5 / DZ_5);
}

int main() {
//    omp_set_nested(1);
//    omp_set_num_threads(3); ///Limit CPU usage, so I can work while the program is running
    double t_start, t_end;
    t_start = omp_get_wtime();

    try {
        printf("Start program with %d maximal threads.\n", omp_get_max_threads());
//        print_parameters();

        ///OneDim part
//        onedim::plot(TOY_DX, TOY_DT, TOY_STEPS, TOY_OFFSET, TOY_LENGTH, 1, "_1");
//        onedim::plot_r(TOY_DX, TOY_DT, TOY_STEPS, TOY_OFFSET, TOY_LENGTH, 1, "_1");
//        onedim::plot(DZ_1, DT_1, STEPS, OFFSET_1, L_1, 1, "_2");
//        onedim::plot_r(DR_1, DT_1, 10, OFFSET_1, R_1, 1, "_2");
//        onedim::error_plot();

        ///OneDim non diffusive part
//        onedim::non_diffusive_plot(0.1, 0.01, 100, 10, 1, 1e0, "_1");
//        onedim::non_diffusive_plot(0.1, 0.01, 200, 10, 1, 1e0, "_2");
//        onedim::non_diffusive_plot(0.1, 0.01, 300, 10, 1, 1e0, "_3");

        ///TwoDim part
//        twodim::plot(TOY_DX, TOY_DX, TOY_DT, TOY_STEPS, TOY_OFFSET, TOY_LENGTH, TOY_LENGTH, 1, "_1");
//        twodim::plot(DR_1, DZ_1, DT_1, 10, OFFSET_1, R_1, L_1, 1, "_2");
//        twodim::error_plot();

        ///Test of ThreeDim parts and it's errors
//        threedim::test_diffusion(TOY_DX, TOY_DX, TOY_DT, TOY_STEPS, TOY_OFFSET, TOY_LENGTH, TOY_LENGTH, DELTA, "_1");
//        threedim::test_p(TOY_DLOG_P, TOY_DT, TOY_STEPS, TOY_OFFSET, TOY_LOG_P_START, TOY_LOG_P_END, TOY_EPSILON_P, "_1");
//        threedim::test_diffusion(DR, DZ, DT, STEPS, OFFSET, R, L, DELTA, "_2");
//        threedim::test_p(TEST_DLOG_P, DT, STEPS, OFFSET, TEST_LOG_P_START, TEST_LOG_P_END, TEST_EPSILON_P, "_2");

        ///ThreeDim part
//        threedim::plot(TOY_DX, TOY_DX, TOY_DLOG_P, TOY_DT, TOY_STEPS, TOY_OFFSET, TOY_LENGTH, TOY_LENGTH,
//                       TOY_LOG_P_START, TOY_LOG_P_END, DELTA, GAMMA, "_1");
//        threedim::plot(DR, DZ, DLOG_P, DT, STEPS, OFFSET, R, L, LOG_P_START, LOG_P_END, DELTA, GAMMA, "_2");
//        threedim::plot(DR_1, DZ_1, DLOG_P, DT_1, 1, OFFSET_1, R_1, L_1, LOG_P_START, LOG_P_END, DELTA, GAMMA, "_2");
//        threedim::plot(DR_3, DZ_3, DLOG_P, DT_3, 0, OFFSET, R_3, L_3, LOG_P_START, LOG_P_END, DELTA, GAMMA, "_2");
//        threedim::error_plot();

        ///Tests of the CRDiffusion
//        CRDiff::test(TOY_DX, TOY_DX, TOY_DLOG_P, TOY_DT, TOY_STEPS, TOY_OFFSET, TOY_LENGTH, TOY_LENGTH,
//                     TOY_LOG_P_START, TOY_LOG_P_END, DELTA, GAMMA, "_1");
//        CRDiff::plot(TOY_DX, TOY_DX, TOY_DLOG_P, TOY_DT, TOY_STEPS, TOY_OFFSET, TOY_LENGTH, TOY_LENGTH,
//                     TOY_LOG_P_START, TOY_LOG_P_END, TOY_LENGTH, DELTA, POWER_N, GAMMA, "_1");
//        CRDiff::test(DR_4, DZ_4, DLOG_P, DT_4, 0, OFFSET, R_4, L_4, LOG_P_START, LOG_P_END, DELTA, GAMMA, "_2");
//        CRDiff::plot(DR_5, DZ_5, DLOG_P, DT_5, 1, OFFSET, R_4, L_4,
//                     LOG_P_START, LOG_P_END, H, DELTA, POWER_N, GAMMA, "_2");
//        CRDiff::plot_energy_loss_rate(DLOG_P, LOG_P_START, LOG_P_END, DT, "_complete");
//        CRDiff::plot_diffusion_coefficient(DLOG_P, LOG_P_START, LOG_P_END, DELTA, "_complete");

        ///Generate Templates for the CRDiffusion
//        CRDiff::generate_templates();
    } catch (const own_error &err) {
        own_catch(err);
    }

    t_end = omp_get_wtime();
    double duration_sec = t_end - t_start;
    cout << "Process finished in " << duration_sec << " seconds." << endl;

    return 0;
}
