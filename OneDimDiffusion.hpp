//
// Created by Flo on 04.05.2020.
//

#ifndef PROGRAM_ONEDIMDIFFUSION_HPP
#define PROGRAM_ONEDIMDIFFUSION_HPP

#include "plot.hpp"

/**
 * Some algorithms to solve the one dimensional diffusion equation: <BR>
 * in an x coordinate: du/dt = kappa0 * d^2u/dx^2 <BR>
 * in an r coordinate: du/dt = kappa0 * (1/r du/dx + d^2u/dx^2) <BR>
 * and the one dimensional non-diffusive problem: <BR>
 * du/dt = -v * du/dx <BR>
 * The initial values and boundary conditions must be set in u.
 * @relatesalso NDVector, NDNumerics
 */
namespace onedim {
    /**
     * Theoretical value of the flux, with dirac_delta(x) at t = 0.
     * @param x, t
     * @param kappa0 diffusion coefficient
     * @return exp(-x^2 / (4 t kappa0)) / (2 sqrt(pi t kappa0))
     */
    NumType psi_theory(NumType_I &x, NumType_I &t, NumType_I &kappa0);

    /**
     * Theoretical value of the flux in r, with dirac_delta(x) at t = 0.
     * @param x, t
     * @param kappa0 diffusion coefficient
     * @return exp(-x^2 / (4 t kappa0)) / (4 pi t kappa0)
     */
    NumType psi_r_theory(NumType_I &x, NumType_I &t, NumType_I &kappa0);

    /**
     * Calculate dimensional diffusion in an normal coordinate and saves the plots as .csv. <BR>
     * du/dt = kappa0 * d^2u/dx^2
     * @param dx, dt step size
     * @param steps count of steps to calculate
     * @param steps count of offset steps to begin calculation
     * @param length length L
     * @param kappa0 diffusion coefficient
     * @param file_appendix the appendix t0 the file to identify different plots
     */
    void plot(NumType_I &dx, NumType_I &dt, const int &steps, const int &offset, NumType_I &length,
              NumType_I &kappa0, const std::string &file_appendix = "");

    /**
     * Calculate dimensional diffusion in the r coordinate and saves the plots as .csv. <BR>
     * du/dt = kappa0 * (1/r du/dx + d^2u/dx^2)
     * @param dx, dt step size
     * @param steps count of steps to calculate
     * @param steps count of offset steps to begin calculation
     * @param length length L
     * @param kappa0 diffusion coefficient
     * @param file_appendix the appendix t0 the file to identify different plots
     */
    void plot_r(NumType_I &dx, NumType_I &dt, const int &steps, const int &offset, NumType_I &length,
                NumType_I &kappa0, const std::string &file_appendix = "");

    /**
     * Calculates the difference of the theory and the Crank-Nicolson method at the different time steps.
     * @param steps NumVector of the different step counts, is casted into int
     * @param dx, dt step size
     * @param steps count of offset steps to begin calculation
     * @param length length L
     * @param kappa0 diffusion coefficient
     * @return vector with the maximum absolute difference at the time steps
     */
    VecNum
    error(VecNum_I &steps, NumType_I &dx, NumType_I &dt, const int &offset, NumType_I &length, NumType_I &kappa0);

    /**
     * Calculates the difference of the theory and the numerical method at the different time steps for diffusion in an r coordinate.
     * @param steps NumVector of the different step counts, is casted into int
     * @param dx, dt step size
     * @param steps count of offset steps to begin calculation
     * @param length length L
     * @param kappa0 diffusion coefficient
     * @return vector with the maximum absolute difference at the time steps
     */
    VecNum
    error_r(VecNum_I &steps, NumType_I &dx, NumType_I &dt, const int &offset, NumType_I &length, NumType_I &kappa0);

    /**
     * Makes the plot for the differences at different parameters.
     */
    void error_plot();

    /**
     * Generates .csv files for the non diffusive problem. <BR>
     * du/dt = -v * du/dx <BR>
     * u(t=0) = dirac_delta(x, epsilon)
     * @param dx, dt step size
     * @param steps count of steps to calculate
     * @param length length L
     * @param v velocity
     * @param epsilon epsilon in dirac_delta
     * @param file_appendix the appendix zo the file to identify different plots
     */
    void non_diffusive_plot(NumType_I &dx, NumType_I &dt, const int &steps, NumType_I &length,
                            NumType_I &v, NumType_I &epsilon, const std::string &file_appendix = "");
}

#endif //PROGRAM_ONEDIMDIFFUSION_HPP
