# Changelog

All notable changes to this project will be documented in this file.

## 11.07.20
Last update of the Readme and the documentation.

## 02.07.20
Introduce a rescale factor for the spectrum, so it fit for high energies.
Create a plot of the characteristic time scales.

Write documentation for python code.

## 30.06.20
Some improvements of the plots.

## 29.06.20
Some minor changes and improvements of the plots.

## 28.06.20
Changed the interpolation of u.
Has problems with overflows for times outside the gird,
so it best is only used inside the grid.

Changed `b0`.
Improved the plots.

## 26.06.20
Function `stochastic_analysis` now save the results as `.hdf5`.

Set the parameters, reset counting of the templates.

## 25.06.20
Added the function `plot_diffusion_coefficient`.
This `plot_diffusion_coefficient` and `plot_energy_loss` now also fit the power-law coefficients.

## 24.06.20
Created the mathematica notebook `CRDiffusion.nb` to calculate the analytic template.

## 23.06.20
Created the `stochastic_analysis` and `calculate_ionisationrate` function in `CRDiffusion.py`.

## 22.06.20
Further improved the `interpolate_template` function,
by using numpy functions and getting rid of loops.

Corrected a wrong factor in the `added_spectrum`.
The calculated Spectrum and the data now fit very much better.

Improved the `added_spectrum` function in `CRDiffusoin.py`.
Problem was not the interpolation, but the creating of source distribution.
Use numpy now numpy for that.

## 21.06.20
Tried to improve the interpolation in python with numba. Didn't work!

## 20.06.20
Changed the numeric boundaries in R und L.
Use now 5 time parts.

## 19.06.20
Fixed the normalization factors.

Changed the representation of the dirac_delta(x) in the injection
for the CRDiffusion.
Now gives results that fit the analytical with the right normalization factor.

Made some improvements in the parameter choice.

## 17.06.20
Created plots with the ionisation rates and 
a function that should calculate the ionisation rate in the future.

## 16.06.20
Created a distribution class for the r distribution in `CRDiffusion.py`.

Created a separate function `energy_loss` in `CRDiffusion` to calculate the energy loss rate.

Created a plot with the data from AMS and Voyager.

Test the program with the mpdata scheme.
Time step must be very very small.

## 15.06.20
Implemented a new upwind and mpdata scheme. The mpdata scheme works better now.

## 14.06.20
Added the analytical result to the Spectrum_5 plot.
Changed the scaling factor from dimensionless to normal variables.

## 12.06.20
Run the program and generated a light curve plot
and a similar plot with the parameters of the real CRDiffusion.
For the real CR diffusion the log_p step size must be smaller.
Also the R,L boundary for the different time parts must be greater.

Corrected some problems the real CRDiffusion.

## 11.06.20
The diffusion part now uses different boundary conditions.

Use bicubic interpolation for the changing the gird. 

Now implemented all parameters and functions for the real CR Diffusion.

## 10.06.20
The template calculation now uses different time parts
with different step sizes (`generate_template_2`).
For the interpolation between the girds a bilinear interpolation is used.

## 09.06.20
The `interpolate_u` works now and uses bilinear interpolation in r and z.

Added function `interpoalte_u` in CRDiffusion. 
The Idea is to split the simulation in two (or more) parts
with different sized Grids.
This function should interpolate u from one grid to the other.
The function isn't implemented yet and does nothing at the moment.

Used OpenMP to parallelize code in ThreeDimDiffusion and CRDiffusion.

Now use a power law injection to test the ThreeDimDiffusion.

## 07.06.20
The log_p part now uses `semi_lagrangian_2`,
so the derivation of b is made by finite differences.

Implemented an parallel run in ThreeDimDiff with OpenMP.

## 06.06.20
Excluded the HDF5 files from the Project.
The now uses CMake for working at my Laptop
and the Makefile using the h5c++ compiler for the Cluster.
**It now works on the Cluster!!!**

## 05.06.20
`generate_template()` saves the data now every calculated time step,
and extend the data.
So the calculation can be interrupted,
but the data generated until then is saved and can be used.

Implemented an second version of the CRDiffusion, that uses the parameters "parameter_values".
Is not complete!

Implemented the linear interpolation for the template in CRDiffusion.py

## 04.06.20
Make a run with parameters for a fast simulation to get the lightcurve plot.

Improved the error plots.

## 03.06.20
Rewrite all `*Diffusion.cpp/.hpp` files.
The syntax the now the same everywhere and the parameters can be changed more easy.
Updated the documentation and the Readme.

Diffusion part 1/r du/dr now uses the Semi-Langrangian scheme.

Parameters are now defined as macros in `pamrameters.hpp`

## 02.06.20
TODO: Update Documentation and Readme!

Make some fist estimates of the parameters.

Added some error tests.

Fixed the Problem with the 3DDiffusion.
Analytic solutions give now the same results as the numeric.

## 01.06.20
Create `CRDiffusion.py`.

Added `generate_template` in `CRDiffusion.cpp/hpp`.

Make the dirac_delta 'save', by returning 0 if `x^2 > 10 epsilon`.

Added save_in_hdf5 to plot.cpp/.hpp

## 31.05.20
Project now include HDF5 library.

Removed phi part from CRDiffusion.

Added targets `clean_plots` and `clean_all` to the Makefile.


## 30.05.20
Project now is compiled using cmake.

## 29.05.20
Implemented a solution for the r=0 problem.
The r vector starts now at -dr. So `r[0]=-dr`, and `r[1] = 0`.
The `r[0]` part stand for the r at dr shifted by pi in phi.

## 28.05.20
Still TODO:
* Boundary of r at r=0
* injection spectrum part 1/r dirac_delta(r-r0)

Solved the problem for phi at r=0 by taking the mean in phi.

Implemented normal and uniform distribution generators in plot.cpp 

Phi has now cyclic boundary conditions.

Added cyclic boundary conditions to all diffusion methods.
Implemented the `cyclic` method form Numerical Recipes in `tridiag.cpp` for solving cyclic tridiagonal systems.
The OneDimDiffusion now can also plot the results of a calculation with r like boundaries or cyclic boundaries.

Injection spectrum and energy have now the right power law.

## 27.05.20
Improved the CRDiffusion.
Still TODO:
* Cyclic boundaries of phi
* Boundary of r (r at -dr == r at dr but at phi + pi)
* injection spectrum and energy loss power law
* diffusion in phi, especially at r = 0

## 25.05.20
Created `CRDiffussion.cpp/.hpp`.
The file for the the complete diffusion equation of CRs.
There are still many TODOs in it.

Add the function `save_as_csv(x, file)` that saves a `NDVector` in a `.csv` file,
and can be load with `plot.py`: `load_csv_file(file)`.

The plots are now saved in the folder `plots`.

Add the error plots for ThreeDimDiffusion.
Add an plot that shows the error in the 2D case in dependence of r/z.

## 22.05.20
Improved the Semi-Lagrangian scheme by using an cubic interpolation method.
It works very good now, and is used in the energy loss part.
Added an advanced Semi-Lagrangian scheme,
that also calculates the derivative of the velocity vector.
This doesn't work as well as updating it extra, so it isn't used.

Changed the parameters.
The computation takes a while, but the results are good.

## 21.05.20
Write the energy loss part of the ThreeDimDiff.
It uses the mpdata upwind scheme from Myths and Methods in Modelling.
It doesn't give very good results,
but there are results somehow comparable with the theory.
If also tried an semi-lagrangian scheme, but that somehow doesn't work.

## 20.05.20
Created `TreeDimDiffusion.cpp / .hpp` for the three dimensional diffusion equation
with homogeneous energy loss.
Tested the diffusion part, it works at it should,
see `ThreeDimTestDiff_r/z_1.pdf`.
The energy loss part is not written yet.

Fixed a bug in the indexing of `NDVector.get_subvector()`.
In `NDNumerics` are the functions for `apply_subvector` now writen extra,
so `kappa0` can be changed for different indices. 

## 17.05.20
Changed the start condition from theory at `dt`
to theory at `10*dt` in the TwoDimDiff.cpp.

Make plots for the errors of the TwoDimDiff.
The chose of the parameters `L` must still be improved.

## 15.05.20
Numerical calculation works now for the Two Dimensional part,
but depends very much on the chosen parameters!

Found an some Errors. Some Signs where wrong, the Parameter dx and dz should somehow be lesser than 0.1.

The Problems of stability is in the first order part od the equation.
To test this part, the one dimensional flux-conservative (in the code named non_diffusive) problem is solved.
There was an Problem with Integer Diffusion in the theory part of the TwoDimDiffusion.