//
// Created by flosc on 20.05.2020.
//

#ifndef PROGRAMM_THREEDIMDIFFUSION_HPP
#define PROGRAMM_THREEDIMDIFFUSION_HPP

#include "NDVector.hpp"
#include "plot.hpp"

/**
 * Some algorithms to solve the three dimensional diffusion equation with rotational symmetry and energy loss: <BR>
 * du/dt = kappa(p) * (d^2u/dr^2 + 1/r du/dr + d^2u/dz^2) - d/dp(b(p) u) <BR>
 * The initial values and boundary conditions must be set in u. <BR>
 * Uses dimensionless variables, so kappa(p) = p^delta, b(p) = -p^2. <BR>
 * Uses logarithmic p variable log_p = ln(p). <BR>
 * u = u[log_p, z, r]
 * @relatesalso NDVector, NDNumerics
 */
namespace threedim {
    /**
     * Theoretical value of the flux, with an q = p^-gamma injection spectrum, scaled with exp(log_p).
     * @param r, z, p, t step size
     * @param delta delta in kappa = p^delta
     * @param gamma gamma in q = p^-gamma
     * @return psi(r, z, p, t) * exp(log_p)
     */
    NumType
    psi_theory(NumType_I &r, NumType_I &z, NumType_I &log_p, NumType_I &t, NumType_I &delta, NumType_I &gamma);

    /**
     * Theoretical value of the flux, with an delta peak injection, scaled with exp(log_p).
     * @param r, z, p, t step size
     * @param delta delta in kappa = p^delta
     * @param epsilon epsilon in dirac_delta(x, epsilon)
     * @return psi(r, z, p, t) * exp(log_p)
     */
    NumType
    psi_theory_delta(NumType_I &r, NumType_I &z, NumType_I &log_p, NumType_I &t, NumType_I &delta, NumType_I &epsilon);

    /**
     * Calculate the diffusion part of the equation for one time step. <BR>
     * du/dt = kappa(p) * (d^2u/dr^2 + 1/r du/dr + d^2u/dz^2) <BR>
     * kappa(p) = p^delta
     * @param[in, out] un the function u to be calculated. In at time step n, out at time step n+1.
     * @param[in] dr, dz, dt step size
     * @param[in] delta delta in kappa = p^delta
     * @param[in] r, log_p
     */
    void diffusion_step_r_z(VecND &un, NumType_I &dr, NumType_I &dz, NumType_I &dt,
                            NumType_I &delta, VecNum_I &r, VecNum_I &log_p);

    /**
     * Calculate the energy loss part of the equation for one time step. <BR>
     * du/dt = - d/dp(b(p) u) = 2 exp(log_p) u + exp(log_p) du/dlog_p
     * @param[in, out] un the function u to be calculated. In at time step n, out at time step n+1.
     * @param[in] dlog_p, dt step size
     * @param[in] delta delta in kappa = p^delta
     * @param[in] log_p
     */
    void calculate_step_p(VecND &un, NumType_I &dlog_p, NumType_I &dt, VecNum_I &log_p);

    /**
     * Calculate three dim diffusion numerical.
     * @param[in, out] u the function u to be calculated. Must have the initial values at beginning, will be returned with
     *      calculated values after steps*dt
     * @param[in] dr, dz, dlog_p, dt step size
     * @param[in] steps count of steps to calculate
     * @param[in] delta delta in kappa = p^delta
     * @param[in] r, log_p
     */
    void calculate_steps(VecND &u, NumType_I &dr, NumType_I &dz, NumType_I &dlog_p, NumType_I &dt,
                         const int &steps, NumType_I &delta, VecNum_I &r, VecNum_I &log_p);

    /**
     * Generates .csv files for the three dimensional diffusion.
     * @param dr, dz, dlog_p, dt step size
     * @param steps count of steps to calculate
     * @param offset count of steps to begin the calculation
     * @param length_r, length_z length in r/z direction
     * @param log_p_start, log_p_end start/end of log_p
     * @param delta delta in kappa = p^delta
     * @param gamma gamma in q = p^-gamma
     * @param file_appendix the appendix to the file to identify different plots
     */
    void
    plot(NumType_I &dr, NumType_I &dz, NumType_I &dlog_p, NumType_I &dt, const int &steps, const int &offset,
         NumType_I &length_r, NumType_I &length_z, NumType_I &log_p_start, NumType_I &log_p_end,
         NumType_I &delta, NumType_I &gamma, const std::string &file_appendix = "");

    /**
     * Generates .csv files for the three dimensional diffusion.
     * @param steps NumVector of the different step counts, is casted into int
     * @param dr, dz, dlog_p, dt step size
     * @param offset count of steps to begin the calculation
     * @param length_r, length_z length in r/z direction
     * @param log_p_start, log_p_end start/end of log_p
     * @param delta delta in kappa = p^delta
     * @param gamma gamma in q = p^-gamma
     */
    VecNum
    error(VecNum_I &steps, NumType_I &dr, NumType_I &dz, NumType_I &dlog_p, NumType_I &dt, const int &offset,
          NumType_I &length_r, NumType_I &length_z, NumType_I &log_p_start, NumType_I &log_p_end,
          NumType_I &delta, NumType_I &gamma);

    /**
     * Makes the plot for the differences at different parameters.
     */
    void error_plot();

    /**
     * Test only the diffusion part of the equation. Give the same results as two dim diffusion.
     * @param dr, dz, dt step size
     * @param steps count of steps to calculate
     * @param offset count of steps to begin the calculation
     * @param length_r, length_z length in r/z direction
     * @param delta delta in kappa = p^delta
     * @param file_appendix the appendix to the file to identify different plots
     */
    void test_diffusion(NumType_I &dr, NumType_I &dz, NumType_I &dt, const int &steps, const int &offset,
                        NumType_I &length_r, NumType_I &length_z, NumType_I &delta,
                        const std::string &file_appendix = "");

    /**
     * Analytical solution to the diffusion part. <BR>
     * equation: du/dt - e^-x d/dx(e^2x u) = 0 <BR>
     * solution: u = exp(-2 x) * dirac_delta(exp(-x) - (t + 1)) <BR>
     * the function is scaled with exp(x).
     * @param x, t parameter
     * @param delta delta in kappa = p^delta
     * @param epsilon epsilon in dirac_delta(x, epsilon)
     * @return u(x, t) * exp(x)
     */
    NumType p_theory(NumType_I &x, NumType_I &t, NumType_I &epsilon);

    /**
     * Test only the energy loss part of the equation.
     * @param dlog_p, dt step size
     * @param steps count of steps to calculate
     * @param offset count of steps to begin the calculation
     * @param log_p_start, log_p_end start/end of log_p
     * @param epsilon epsilon in dirac_delta(x, epsilon)
     * @param file_appendix the appendix to the file to identify different plots
     */
    void test_p(NumType_I &dlog_p, NumType_I &dt, const int &steps, const int &offset,
                NumType_I &log_p_start, NumType_I &log_p_end, NumType_I &epsilon,
                const std::string &file_appendix = "");
}

#endif //PROGRAMM_THREEDIMDIFFUSION_HPP
