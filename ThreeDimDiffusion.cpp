//
// Created by flosc on 20.05.2020.
//

#include "ThreeDimDiffusion.hpp"
#include "NDNumerics.hpp"
#include "parameters.hpp"
#include <cmath>
#include <iostream>
#include <sstream>
#include "TwoDimDiffusion.hpp"
#include <omp.h>

using namespace std;
using namespace plot;
#define IND(j) (SizeType) (j), subvector_index, index

NumType
threedim::psi_theory(NumType_I &r, NumType_I &z, NumType_I &log_p, NumType_I &t, NumType_I &delta, NumType_I &gamma) {
    NumType p = exp(log_p);
    NumType p0 = 1. / (1. / p - t);
    if (p0 < 0)
        return 0;
    NumType lambda = -1. / (delta - 1.) * (pow(p, delta - 1.) - pow(p0, delta - 1.));
    return exp(-(r * r + z * z) / (4 * lambda)) / pow(4 * M_PI * lambda, 3. / 2.)
           * (p0 * p0) / (p * p) * pow(p0, -gamma)
           * exp(log_p);
}

NumType
threedim::psi_theory_delta(NumType_I &r, NumType_I &z, NumType_I &log_p, NumType_I &t, NumType_I &delta,
                           NumType_I &epsilon) {
    NumType lambda = 1. / (1. - delta) * (pow(t + 1., 1. - delta) - 1.);
    return exp(-(r * r + z * z) / (4 * lambda)) / (pow(4 * M_PI * lambda, 3. / 2.))
           //           * exp(-2. * log_p) * dirac_delta(exp(-log_p) - (t + 1.), epsilon);
           * exp(-log_p) * dirac_delta(exp(-log_p) - (t + 1.), epsilon); ///scale u with exp(log_p)
}

void
threedim::diffusion_step_r_z(VecND &un, NumType_I &dr, NumType_I &dz, NumType_I &dt, NumType_I &delta,
                             VecNum_I &r, VecNum_I &log_p) {
    IndexType index = {0, 0, 0};
#pragma omp parallel for default(none) shared(un, r, log_p, dr, dz, dt, delta) firstprivate(index)
    for (SizeType i1 = 0; i1 < un.get_size(0); i1++) {
        index[0] = i1;
        NumType kappa_2 = exp(delta * log_p[i1]) / 2; ///kappa/2, because needed in 1/2 for crank_nicolson

        ///Step in z
        NumType alpha_z = kappa_2 * dt / (dz * dz); ///For implicit method
        VecNum b_z(un.get_size(1), 1 + 2 * alpha_z);
        VecNum a_z(un.get_size(1), -alpha_z);
        VecNum c_z(un.get_size(1), -alpha_z);
        c_z.front() = 0; ///constant boundary start
        b_z.front() = 1;
        a_z.back() = 0; ///constant boundary end
        b_z.back() = 1;
        for (SizeType i2 = 0; i2 < un.get_size(2); i2++) {
            index[2] = i2;
            NDNumerics::diffusion_ftcs_index(un, 1, index, dz, dt, kappa_2, 0);
            NDNumerics::diffusion_implicit_index(un, 1, index, a_z, b_z, c_z);
        }

        ///Step in r
        NumType alpha_r = kappa_2 * dt / (dr * dr); ///For implicit method
        VecNum b_r(un.get_size(2), 1 + 2 * alpha_r);
        VecNum a_r(un.get_size(2), -alpha_r);
        VecNum c_r(un.get_size(2), -alpha_r);
        c_r.front() = -2 * alpha_r; ///boundary start
        a_r.back() = 0; ///constant boundary end
        b_r.back() = 1;
        ///du/dt = kappa/r du/dr part
        auto kappa_r_func = [&kappa_2](NumType_I &x) {
            return -kappa_2 * 2.0 / x;
        };
        VecNum kappa_r = vec_apply(r, kappa_r_func);
        for (SizeType i2 = 0; i2 < un.get_size(1); i2++) {
            index[1] = i2;
            NDNumerics::diffusion_ftcs_index(un, 2, index, dr, dt, kappa_2, 1);
            NDNumerics::diffusion_implicit_index(un, 2, index, a_r, b_r, c_r);
//            NDNumerics::diffusion_lax_wendroff_index(un, 2, index, dr, dt, kappa_2 * 2, r);
            NDNumerics::semi_lagrangian_index(un, 2, index, dr, dt, kappa_r);
        }
    }
}

void
threedim::calculate_step_p(VecND &un, NumType_I &dlog_p, NumType_I &dt, VecNum_I &log_p) {
    ///v = -exp(log_p)
    auto velocity = [](NumType_I &x) {
        return -exp(x);
    };
    VecNum v = vec_apply(log_p, velocity);
    IndexType index = {0, 0, 0};
#pragma omp parallel for default(none) shared(un, log_p, v, dlog_p, dt) firstprivate(index)
    for (SizeType i_z = 0; i_z < un.get_size(1); i_z++) { ///Energy loss only in disk
        index[1] = i_z;
        for (SizeType i_r = 0; i_r < un.get_size(2); i_r++) {
            index[2] = i_r;

//            NDNumerics::semi_lagrangian_2_index(un, 0, index, dlog_p, dt, v, 2);
            NDNumerics::mpdata_index(un, 0, index, dlog_p, dt, v); ///Use mpdata upwind scheme
        }
    }
}

void
threedim::calculate_steps(VecND &u, NumType_I &dr, NumType_I &dz, NumType_I &dlog_p, NumType_I &dt, const int &steps,
                          NumType_I &delta, VecNum_I &r, VecNum_I &log_p) {
    for (int i = 0; i < steps; i++) {
        diffusion_step_r_z(u, dr, dz, dt, delta, r, log_p);
        calculate_step_p(u, dlog_p, dt, log_p);
    }
}

void threedim::plot(NumType_I &dr, NumType_I &dz, NumType_I &dlog_p, NumType_I &dt, const int &steps, const int &offset,
                    NumType_I &length_r, NumType_I &length_z, NumType_I &log_p_start, NumType_I &log_p_end,
                    NumType_I &delta, NumType_I &gamma, const std::string &file_appendix) {
    VecNum r = arange(0, length_r, dr);
    VecNum z = arange(-length_z, length_z, dz);
    SizeType index_z0 = (z.size() / 2);
    VecNum log_p = arange(log_p_start, log_p_end, dlog_p);
    SizeType index_p0 = 0;
    string label_p = "log_p=" + to_string(log_p[index_p0]) + ")";

    VecND u;
    function<NumType(VecNum_I &)> f0 = [&dt, &offset, &delta, &gamma](VecNum_I &val) {
        return psi_theory(val[2], val[1], val[0], dt * offset, delta, gamma);
    };
    u.apply({log_p, z, r}, f0);
    ///Boundaries
    auto set_boundaries_z = [](VecND &un, SizeType_I &subvector_index, IndexType_I &index) {
        un.at(0, subvector_index, index) = 0;
        un.at(-1, subvector_index, index) = 0;
    };
    u.apply_subvector(set_boundaries_z, 1);
    auto set_boundaries_r = [](VecND &un, SizeType_I &subvector_index, IndexType_I &index) {
        un.at(-1, subvector_index, index) = 0;
    };
    u.apply_subvector(set_boundaries_r, 2);
//    save_as_csv(r, u.subvector(2, {index_p0, index_z0, 0}), "ThreeDimDiff_r_u0" + file_appendix + ".csv",
//                "r", "u(z=0, " + label_p, "u0");
//    save_as_csv(z, u.subvector(1, {index_p0, 0, 0}), "ThreeDimDiff_z_u0" + file_appendix + ".csv",
//                "z", "u(r=0, " + label_p, "u0");
//    save_as_csv(log_p, u.subvector(0, {0, index_z0, 0}), "ThreeDimDiff_logp_u0" + file_appendix + ".csv",
//                "log_p", "u(r=0, z=0)", "u0");

    VecND ut;
    function<NumType(VecNum_I &)> ft = [&dt, &steps, &offset, &delta, &gamma](VecNum_I &val) {
        return psi_theory(val[2], val[1], val[0], dt * (steps + offset), delta, gamma);
    };
    ut.apply({log_p, z, r}, ft);
    save_as_csv(r, ut.subvector(2, {index_p0, index_z0, 0}), "ThreeDimDiff_r_theory" + file_appendix + ".csv",
                "r", "u(z=0, " + label_p, "Theory");
    save_as_csv(z, ut.subvector(1, {index_p0, 0, 0}), "ThreeDimDiff_z_theory" + file_appendix + ".csv",
                "z", "u(r=0, " + label_p, "Theory");
    save_as_csv(log_p, ut.subvector(0, {0, index_z0, 0}), "ThreeDimDiff_logp_theory" + file_appendix + ".csv",
                "log_p", "u(r=0, z=0)", "Theory");

    threedim::calculate_steps(u, dr, dz, dlog_p, dt, steps, delta, r, log_p);
    save_as_csv(r, u.subvector(2, {index_p0, index_z0, 0}), "ThreeDimDiff_r_numerical" + file_appendix + ".csv",
                "r", "u(z=0, " + label_p, "Numerical");
    save_as_csv(z, u.subvector(1, {index_p0, 0, 0}), "ThreeDimDiff_z_numerical" + file_appendix + ".csv",
                "z", "u(r=0, " + label_p, "Numerical");
    save_as_csv(log_p, u.subvector(0, {0, index_z0, 0}), "ThreeDimDiff_logp_numerical" + file_appendix + ".csv",
                "log_p", "u(r=0, z=0)", "Numerical");

    ///Calculate the error
    NumType max_value = abs(vec_max(ut.get_values()));
    function<NumType(NumType_I &, NumType_I &)> func_diff;
    VecND diff;
    ///absolute difference
//    func_diff = [](NumType_I &a, NumType_I &b) { return abs(a - b); };
//    diff = VecND::operate(ut, u, func_diff);
//    save_as_csv(r, diff.subvector(2, {index_p0, index_z0, 0}), "ThreeDimDiff_r_abs" + file_appendix + "_error.csv",
//                "r", "$\\Delta$ u(z=0, " + label_p, "absolute difference");
//    save_as_csv(z, diff.subvector(1, {index_p0, 0, 0}), "ThreeDimDiff_z_abs" + file_appendix + "_error.csv",
//                "z", "$\\Delta$ u(r=0, " + label_p, "absolute difference");
//    save_as_csv(log_p, diff.subvector(0, {0, index_z0, 0}), "ThreeDimDiff_logp_abs" + file_appendix + "_error.csv",
//                "log_p", "$\\Delta$ u(r=0, z=0)", "absolute difference");
    ///relative difference
    func_diff = [](NumType_I &a, NumType_I &b) { return abs(a - b) / abs(a); };
    diff = VecND::operate(ut, u, func_diff);
    save_as_csv(r, diff.subvector(2, {index_p0, index_z0, 0}), "ThreeDimDiff_r_rel" + file_appendix + "_error.csv",
                "r", "$\\Delta$ u(z=0, " + label_p, "relative difference");
    save_as_csv(z, diff.subvector(1, {index_p0, 0, 0}), "ThreeDimDiff_z_rel" + file_appendix + "_error.csv",
                "z", "$\\Delta$ u(r=0, " + label_p, "relative difference");
    save_as_csv(log_p, diff.subvector(0, {0, index_z0, 0}), "ThreeDimDiff_logp_rel" + file_appendix + "_error.csv",
                "log_p", "$\\Delta$ u(r=0, z=0)", "relative difference");
    ///absolute difference / max_value
    func_diff = [& max_value](NumType_I &a, NumType_I &b) { return abs(a - b) / max_value; };
    diff = VecND::operate(ut, u, func_diff);
    save_as_csv(r, diff.subvector(2, {index_p0, index_z0, 0}), "ThreeDimDiff_r_absrel" + file_appendix + "_error.csv",
                "r", "$\\Delta$ u(z=0, " + label_p, "absolute difference / max_value");
    save_as_csv(z, diff.subvector(1, {index_p0, 0, 0}), "ThreeDimDiff_z_absrel" + file_appendix + "_error.csv",
                "z", "$\\Delta$ u(r=0, " + label_p, "absolute difference / max_value");
    save_as_csv(log_p, diff.subvector(0, {0, index_z0, 0}), "ThreeDimDiff_logp_absrel" + file_appendix + "_error.csv",
                "log_p", "$\\Delta$ u(r=0, z=0)", "absolute difference / max_value");
}

VecNum
threedim::error(VecNum_I &steps, NumType_I &dr, NumType_I &dz, NumType_I &dlog_p, NumType_I &dt, const int &offset,
                NumType_I &length_r, NumType_I &length_z, NumType_I &log_p_start, NumType_I &log_p_end,
                NumType_I &delta, NumType_I &gamma) {
    VecNum r = arange(0, length_r, dr);
    VecNum z = arange(-length_z, length_z, dz);
    VecNum log_p = arange(log_p_start, log_p_end, dlog_p);

//    VecND u(3, {log_p.size(), z.size(), r.size()});
    VecND u;
    function<NumType(VecNum_I &)> f0 = [&dt, &offset, &delta, &gamma](VecNum_I &val) {
        return psi_theory(val[2], val[1], val[0], dt * offset, delta, gamma);
    };
    u.apply({log_p, z, r}, f0);
    ///Boundaries
    auto set_boundaries_z = [](VecND &un, SizeType_I &subvector_index, IndexType_I &index) {
        un.at(0, subvector_index, index) = 0;
        un.at(-1, subvector_index, index) = 0;
    };
    u.apply_subvector(set_boundaries_z, 1);
    auto set_boundaries_r = [](VecND &un, SizeType_I &subvector_index, IndexType_I &index) {
        un.at(-1, subvector_index, index) = 0;
    };
    u.apply_subvector(set_boundaries_r, 2);

    VecNum ret_diff;
    int step, last_step = 0;
    for (auto &step_num: steps) {
        step = (int) step_num;
        VecND ut;
        function<NumType(VecNum_I &)> ft = [&dt, &step, &offset, &delta, &gamma](VecNum_I &val) {
            return psi_theory(val[2], val[1], val[0], dt * (step + offset), delta, gamma);
        };
        ut.apply({log_p, z, r}, ft);

        threedim::calculate_steps(u, dr, dz, dlog_p, dt, step - last_step, delta, r, log_p);

//        ret_diff.push_back(vec_max(difference(u.get_values(), ut.get_values()))); ///absolute difference
//        ret_diff.push_back(vec_max(relative_difference(u.get_values(), ut.get_values()))); ///relative difference
        ret_diff.push_back(max_difference_relative(u.get_values(), ut.get_values())); ///max difference relative

        cout << "Calculated " << step << "/" << (int) steps.back() << " steps (last: " << step - last_step << ") \r" << flush;
        last_step = step;
    }
    cout << "\r" << flush;
    return ret_diff;
}

void threedim::error_plot() {
    const int offset = ERROR_OFFSET;
    const NumType length_r = ERROR_LENGTH_R;
    const NumType length_z = ERROR_LENGTH_Z;
    const NumType log_p_start = ERROR_LOG_P_START;
    const NumType log_p_end = ERROR_LOG_P_END;
    const NumType delta = DELTA;
    const NumType gamma = GAMMA;
    VecNum steps, dr_steps, dz_steps, dlog_p_steps, dt_steps;
    ///Plot as function of steps
    steps = ERROR_STEPS;
    dr_steps = ERROR_STEPS_DR;
    dz_steps = ERROR_STEPS_DZ;
    dlog_p_steps = ERROR_STEPS_DLOG_P;
    dt_steps = ERROR_STEPS_DT;
    for (auto &dr: dr_steps) {
        for (auto &dz: dz_steps) {
            for (auto &dlog_p: dlog_p_steps) {
                for (auto &dt: dt_steps) {
                    VecNum diff = error(steps, dr, dz, dlog_p, dt, offset, length_r, length_z,
                                        log_p_start, log_p_end, delta, gamma);
                    ostringstream file, label;
                    file << "ThreeDimDiffError_steps_dr" << dr << "_dz" << dz << "_dlogp" << dlog_p << "_dt" << dt
                         << ".csv";
                    label << "dr = " << dr << ", dz = " << dz << ", dlog_p = " << dlog_p << ", dt = " << dt;
                    save_as_csv(steps, diff, file.str(), "steps", "$\\Delta u$", label.str());
                }
            }
        }
    }
    ///Plot as function of dt
    steps = {ERROR_DT_STEP};
    dr_steps = ERROR_DT_DR;
    dz_steps = ERROR_DT_DZ;
    dlog_p_steps = ERROR_DT_DLOG_P;
    dt_steps = ERROR_DT;
    for (auto &dr: dr_steps) {
        for (auto &dz: dz_steps) {
            for (auto &dlog_p: dlog_p_steps) {
                VecNum diff;
                for (auto &dt: dt_steps) {
                    diff.push_back(error(steps, dr, dz, dlog_p, dt, offset, length_r, length_z,
                                         log_p_start, log_p_end, delta, gamma)[0]);
                }
                ostringstream file, label;
                file << "ThreeDimDiffError_dt_dr" << dr << "_dz" << dz << "_dlogp" << dlog_p << ".csv";
                label << "dr = " << dr << ", dz = " << dz << ", dlog_p = " << dlog_p;
                save_as_csv(dt_steps, diff, file.str(), "dt", "$\\Delta u$", label.str());
            }
        }
    }
}

void threedim::test_diffusion(NumType_I &dr, NumType_I &dz, NumType_I &dt, const int &steps, const int &offset,
                              NumType_I &length_r, NumType_I &length_z, NumType_I &delta, const string &file_appendix) {
    VecNum r = arange(0, length_r, dr);
    VecNum z = arange(-length_z, length_z, dz);
    SizeType index_z0 = (z.size() / 2);
    VecNum log_p = {1};
    NumType kappa = exp(delta * log_p[0]);

    VecND u;
    function<NumType(VecNum_I &)> f0 = [&dt, &offset, &kappa](VecNum_I &val) {
        return twodim::psi_theory(val[1], val[2], dt * offset, kappa);
    };
    u.apply({log_p, z, r}, f0);
    ///Boundaries
    auto set_boundaries_z = [](VecND &un, SizeType_I &subvector_index, IndexType_I &index) {
        un.at(0, subvector_index, index) = 0;
        un.at(-1, subvector_index, index) = 0;
    };
    u.apply_subvector(set_boundaries_z, 1);
    auto set_boundaries_r = [](VecND &un, SizeType_I &subvector_index, IndexType_I &index) {
        un.at(-1, subvector_index, index) = 0;
    };
    u.apply_subvector(set_boundaries_r, 2);

    VecND ut;
    function<NumType(VecNum_I &)> ft = [&dt, &steps, &offset, &kappa](VecNum_I &val) {
        return twodim::psi_theory(val[1], val[2], dt * (steps + offset), kappa);
    };
    ut.apply({log_p, z, r}, ft);
    save_as_csv(r, ut.subvector(2, {0, index_z0, 0}), "ThreeDimTestDiff_r_theory" + file_appendix + ".csv",
                "r", "u(z=0)", "Theory");
    save_as_csv(z, ut.subvector(1, {0, 0, 0}), "ThreeDimTestDiff_z_theory" + file_appendix + ".csv",
                "z", "u(r=0)", "Theory");

//    calculate_steps(u, dr, dz, 1, dt, steps, delta, r, log_p);
    for (int i = 0; i < steps; i++) {
        diffusion_step_r_z(u, dr, dz, dt, delta, r, log_p);
    }
    save_as_csv(r, u.subvector(2, {0, index_z0, 0}), "ThreeDimTestDiff_r_numerical" + file_appendix + ".csv",
                "r", "u(z=0)", "Numerical");
    save_as_csv(z, u.subvector(1, {0, 0, 0}), "ThreeDimTestDiff_z_numerical" + file_appendix + ".csv",
                "z", "u(r=0)", "Numerical");

    ///Calculate the error
    NumType max_value = abs(vec_max(ut.get_values()));
    function<NumType(NumType_I &, NumType_I &)> func_diff;
    VecND diff;
    ///absolute difference
//    func_diff = [](NumType_I &a, NumType_I &b) { return abs(a - b); };
//    diff = VecND::operate(ut, u, func_diff);
//    save_as_csv(r, diff.subvector(2, {0, index_z0, 0}), "ThreeDimTestDiff_r_abs" + file_appendix + "_error.csv",
//                "r", "$\\Delta u(z=0)$", "absolute difference");
//    save_as_csv(z, diff.subvector(1, {0, 0, 0}), "ThreeDimTestDiff_z_abs" + file_appendix + "_error.csv",
//                "z", "$\\Delta u(r=0)$", "absolute difference");
    ///relative difference
//    func_diff = [](NumType_I &a, NumType_I &b) { return abs(a - b) / abs(a); };
//    diff = VecND::operate(ut, u, func_diff);
//    save_as_csv(r, diff.subvector(2, {0, index_z0, 0}), "ThreeDimTestDiff_r_rel" + file_appendix + "_error.csv",
//                "r", "$\\Delta u(z=0)$", "relative difference");
//    save_as_csv(z, diff.subvector(1, {0, 0, 0}), "ThreeDimTestDiff_z_rel" + file_appendix + "_error.csv",
//                "z", "$\\Delta u(r=0)$", "relative difference");
    ///absolute difference / max_value
    func_diff = [&max_value](NumType_I &a, NumType_I &b) { return abs(a - b) / max_value; };
    diff = VecND::operate(ut, u, func_diff);
    save_as_csv(r, diff.subvector(2, {0, index_z0, 0}), "ThreeDimTestDiff_r_absrel" + file_appendix + "_error.csv",
                "r", "$\\Delta u(z=0)$", "absolute difference / max_value");
    save_as_csv(z, diff.subvector(1, {0, 0, 0}), "ThreeDimTestDiff_z_absrel" + file_appendix + "_error.csv",
                "z", "$\\Delta u(r=0)$", "absolute difference / max_value");
}

NumType
threedim::p_theory(NumType_I &x, NumType_I &t, NumType_I &epsilon) {
//    return exp(-2. * x) * dirac_delta(exp(-x) - (t + 1.), epsilon);
    return exp(-x) * dirac_delta(exp(-x) - (t + 1.), epsilon); ///Scale u with exp(x)
}

void threedim::test_p(NumType_I &dlog_p, NumType_I &dt, const int &steps, const int &offset, NumType_I &log_p_start,
                      NumType_I &log_p_end, NumType_I &epsilon, const string &file_appendix) {
    VecNum r = {0};
    VecNum z = {0};
    VecNum log_p = arange(log_p_start, log_p_end, dlog_p);

    VecND u;
    function<NumType(VecNum_I &)> f0 = [&dt, &offset, &epsilon](VecNum_I &val) {
        return p_theory(val[0], dt * offset, epsilon);
    };
    u.apply({log_p, z, r}, f0);
//    save_as_csv(log_p, u.subvector(0, {0, 0, 0}), "ThreeDimTestP_u0" + file_appendix + ".csv",
//                "log_p", "u(log_p)", "u0");

    VecND ut;
    function<NumType(VecNum_I &)> ft = [&dt, &steps, &offset, &epsilon](VecNum_I &val) {
        return p_theory(val[0], dt * (steps + offset), epsilon);
    };
    ut.apply({log_p, z, r}, ft);
    save_as_csv(log_p, ut.subvector(0, {0, 0, 0}), "ThreeDimTestP_theory" + file_appendix + ".csv",
                "log_p", "u(log_p)", "Theory");

//    threedim::calculate_steps(u, dr, dz, dlog_p, dt, steps, 0, r, log_p);
    for (int i = 0; i < steps; i++) {
        calculate_step_p(u, dlog_p, dt, log_p);
    }
    save_as_csv(log_p, u.subvector(0, {0, 0, 0}), "ThreeDimTestP_numerical" + file_appendix + ".csv",
                "log_p", "u(log_p)", "Numerical");

    ///Calculate the error
    NumType max_value = abs(vec_max(ut.get_values()));
    function<NumType(NumType_I &, NumType_I &)> func_diff;
    VecND diff;
    ///absolute difference
//    func_diff = [](NumType_I &a, NumType_I &b) { return abs(a - b); };
//    diff = VecND::operate(ut, u, func_diff);
//    save_as_csv(log_p, diff.subvector(0, {0, 0, 0}), "ThreeDimTestP_abs" + file_appendix + "_error.csv",
//                "log_p", "$\\Delta$ u(log_p)", "absolute difference");
    ///relative difference
//    func_diff = [](NumType_I &a, NumType_I &b) { return abs(a - b) / abs(a); };
//    diff = VecND::operate(ut, u, func_diff);
//    save_as_csv(log_p, diff.subvector(0, {0, 0, 0}), "ThreeDimTestP_rel" + file_appendix + "_error.csv",
//                "log_p", "$\\Delta$ u(log_p)", "relative difference");
    ///absolute difference / max_value
    func_diff = [&max_value](NumType_I &a, NumType_I &b) { return abs(a - b) / max_value; };
    diff = VecND::operate(ut, u, func_diff);
    save_as_csv(log_p, diff.subvector(0, {0, 0, 0}), "ThreeDimTestP_absrel" + file_appendix + "_error.csv",
                "log_p", "$\\Delta$ u(log_p)", "absolute difference / max_value");
}
