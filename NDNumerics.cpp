//
// Created by flosc on 13.05.2020.
//

#include "NDNumerics.hpp"
#include "tridiag.hpp"
#include <cmath>

using namespace std;
using namespace plot;
///Accesses elements in subvector with NDVector.at(IND(j))
#define IND(j) (SizeType) (j), subvector_index, index

void NDNumerics::diffusion_ftcs_index(VecND &u, SizeType_I &subvector_index, IndexType_I &index, NumType_I &dx,
                                      NumType_I &dt, NumType_I &kappa0, const int &boundary_condition) {
    VecNum un1(u.get_size(subvector_index));
    if (boundary_condition == 0 or boundary_condition == 1) {
        if (boundary_condition == 0) {
            un1[0] = u.at(IND(0)); ///Constant boundary at start
        } else {
            ///Boundary for r, u[-dr] = u[dr]
            un1[0] = u.at(IND(0)) + kappa0 * dt / (dx * dx) * (u.at(IND(1)) - 2 * u.at(IND(0)) + u.at(IND(1)));
        }
        un1[un1.size() - 1] = u.at(IND(un1.size() - 1)); ///Constant boundary at end
    } else if (boundary_condition == 2) {
        ///Cyclic boundaries, u[-1] = u[end], u[end+1] = u[0]
        SizeType i_end = un1.size() - 1;
        un1[0] = u.at(IND(0)) + kappa0 * dt / (dx * dx) * (u.at(IND(1)) - 2 * u.at(IND(0)) + u.at(IND(i_end)));
        un1[i_end] = u.at(IND(i_end)) +
                     kappa0 * dt / (dx * dx) * (u.at(IND(0)) - 2 * u.at(IND(i_end)) + u.at(IND(i_end - 1)));
    }
    for (SizeType j = 1; j < u.get_size(subvector_index) - 1; j++) {
        un1[j] = u.at(IND(j)) + kappa0 * dt / (dx * dx) * (u.at(IND(j + 1)) - 2 * u.at(IND(j)) + u.at(IND(j - 1)));
    }
    u.set_subvector(un1, subvector_index, index);
}

void NDNumerics::diffusion_ftcs(VecND &un, NumType_I &dx, NumType_I &dt, NumType_I &kappa0, SizeType_I &dimension_index,
                                const int &boundary_condition) {
    auto func = [&dx, &dt, &kappa0, &boundary_condition](VecND &u, SizeType_I &subvector_index, IndexType_I &index) {
        diffusion_ftcs_index(u, subvector_index, index, dx, dt, kappa0, boundary_condition);
    };
    un.apply_subvector(func, dimension_index);
}

void NDNumerics::diffusion_implicit_index(VecND &u, SizeType_I &subvector_index, IndexType_I &index, VecNum_I &a,
                                          VecNum_I &b, VecNum_I &c) {
    VecNum subvec_u = u.subvector(subvector_index, index);
    VecNum subvec_u1(u.get_size(subvector_index));
    tridag(a, b, c, subvec_u, subvec_u1);
    u.set_subvector(subvec_u1, subvector_index, index);
}

void NDNumerics::diffusion_implicit_cyclic_index(VecND &u, SizeType_I &subvector_index, IndexType_I &index, VecNum_I &a,
                                                 VecNum_I &b, VecNum_I &c) {
    VecNum subvec_u = u.subvector(subvector_index, index);
    VecNum subvec_u1(u.get_size(subvector_index));
    ///Corner elements: alpha = c.back(), beta = a.front()
    cyclic(a, b, c, c.back(), a.front(), subvec_u, subvec_u1);
    u.set_subvector(subvec_u1, subvector_index, index);
}

void
NDNumerics::diffusion_implicit(VecND &un, NumType_I &dx, NumType_I &dt, NumType_I &kappa0, SizeType_I &dimension_index,
                               const int &boundary_condition) {
    if (boundary_condition == 0 or boundary_condition == 1) {
        NumType alpha = kappa0 * dt / (dx * dx);
        VecNum b(un.get_size(dimension_index), 1 + 2 * alpha);
        VecNum a(un.get_size(dimension_index), -alpha);
        VecNum c(un.get_size(dimension_index), -alpha);
        if (boundary_condition == 0) {
            c.front() = 0; ///constant boundary start
            b.front() = 1;
        } else { ///Boundary for r
            c.front() = -2 * alpha;
        }
        a.back() = 0; ///constant boundary end
        b.back() = 1;
        auto func = [&a, &b, &c](VecND &u, SizeType_I &subvector_index, IndexType_I &index) {
            diffusion_implicit_index(u, subvector_index, index, a, b, c);
        };
        un.apply_subvector(func, dimension_index);
    } else if (boundary_condition == 2) {
        ///Cyclic boundaries
        NumType alpha = kappa0 * dt / (dx * dx);
        VecNum b(un.get_size(dimension_index), 1 + 2 * alpha);
        VecNum a(un.get_size(dimension_index), -alpha);
        VecNum c(un.get_size(dimension_index), -alpha);
        ///Corner elements are set in c.back(), a.front()
        auto func = [&a, &b, &c](VecND &u, SizeType_I &subvector_index, IndexType_I &index) {
            diffusion_implicit_cyclic_index(u, subvector_index, index, a, b, c);
        };
        un.apply_subvector(func, dimension_index);
    }
}

void NDNumerics::diffusion_crank_nicolson(VecND &un, NumType_I &dx, NumType_I &dt, NumType_I &kappa0,
                                          SizeType_I &dimension_index, const int &boundary_condition) {
    diffusion_ftcs(un, dx, dt, kappa0 / 2, dimension_index, boundary_condition);
    diffusion_implicit(un, dx, dt, kappa0 / 2, dimension_index, boundary_condition);
}

void NDNumerics::diffusion_lax_wendroff_index(VecND &u, SizeType_I &subvector_index, IndexType_I &index, NumType_I &dx,
                                              NumType_I &dt, NumType_I &kappa0, VecNum_I &x, const int &x_type) {
    VecNum un1(u.get_size(subvector_index));
    un1[x_type] = u.at(IND(x_type)); ///Boundary Conditions
    un1[un1.size() - 1] = u.at(IND(un1.size() - 1));
    for (SizeType j = 1 + x_type; j < u.get_size(subvector_index) - 1; j++) {
        NumType alpha = -kappa0 * dt / (x[j] * dx); ///Negative, because of inverse sign in equation.
        un1[j] = u.at(IND(j)) - alpha * 1.0 / 2.0 *
                                ((u.at(IND(j + 1)) + u.at(IND(j))) - alpha * (u.at(IND(j + 1)) - u.at(IND(j)))
                                 - (u.at(IND(j)) + u.at(IND(j - 1))) + alpha * (u.at(IND(j)) - u.at(IND(j - 1))));
    }
    u.set_subvector(un1, subvector_index, index);
}

void NDNumerics::diffusion_lax_wendroff(VecND &un, NumType_I &dx, NumType_I &dt, NumType_I &kappa0, VecNum_I &x,
                                        SizeType_I &dimension_index) {
    auto func = [&dx, &dt, &kappa0, &x](VecND &u, SizeType_I &subvector_index, IndexType_I &index) {
        diffusion_lax_wendroff_index(u, subvector_index, index, dx, dt, kappa0, x);
    };
    un.apply_subvector(func, dimension_index);
}

void NDNumerics::lax_wendroff(VecND &un, NumType_I &dx, NumType_I &dt, NumType_I &v, SizeType_I &dimension_index) {
    NumType alpha = v * dt / dx;
    auto func = [&dx, &dt, &alpha](VecND &u, SizeType_I &subvector_index, IndexType_I &index) {
        VecNum un1(u.get_size(subvector_index));
        un1[0] = u.at(IND(0)); ///Boundary Conditions
        un1[un1.size() - 1] = u.at(IND(un1.size() - 1));
        for (SizeType j = 1; j < u.get_size(subvector_index) - 1; j++) {
            un1[j] = u.at(IND(j)) - alpha * 1.0 / 2.0 *
                                    ((u.at(IND(j + 1)) + u.at(IND(j))) - alpha * (u.at(IND(j + 1)) - u.at(IND(j)))
                                     - (u.at(IND(j)) + u.at(IND(j - 1))) + alpha * (u.at(IND(j)) - u.at(IND(j - 1))));
        }
        u.set_subvector(un1, subvector_index, index);
    };
    un.apply_subvector(func, dimension_index);
}

void NDNumerics::lax_method(VecND &un, NumType_I &dx, NumType_I &dt, NumType_I &v, SizeType_I &dimension_index) {
    auto func = [&dx, &dt, &v](VecND &u, SizeType_I &subvector_index, IndexType_I &index) {
        VecNum un1(u.get_size(subvector_index));
        un1[0] = u.at(IND(0)); ///Boundary Conditions
        un1[un1.size() - 1] = u.at(IND(un1.size() - 1));
        for (SizeType j = 1; j < u.get_size(subvector_index) - 1; j++) {
            un1[j] = 1.0 / 2.0 * (u.at(IND(j + 1)) + u.at(IND(j - 1))) -
                     v * dt / (2.0 * dx) * (u.at(IND(j + 1)) - u.at(IND(j - 1)));
        }
        u.set_subvector(un1, subvector_index, index);
    };
    un.apply_subvector(func, dimension_index);
}

void NDNumerics::staggered_leapfrog(VecND &un, VecND &um, NumType_I &dx, NumType_I &dt, NumType_I &v,
                                    SizeType_I &dimension_index) {
    NumType alpha = v * dt / dx;
    auto func = [&um, &alpha](VecND &u, SizeType_I &subvector_index, IndexType_I &index) {
        VecNum un1(u.get_size(subvector_index));
        un1[0] = u.at(IND(0)); ///Boundary Conditions
        un1[un1.size() - 1] = u.at(IND(un1.size() - 1));
        for (SizeType j = 1; j < u.get_size(subvector_index) - 1; j++) {
            un1[j] = um.at(IND(j)) - alpha * (u.at(IND(j + 1)) - u.at(IND(j - 1)));
        }
        u.set_subvector(un1, subvector_index, index);
    };
    un.apply_subvector(func, dimension_index);
}

NumType NDNumerics::donor_cell(NumType_I &cjm, NumType_I &cj, NumType_I &v, NumType_I &dx, NumType_I &dt) {
    return ((v + abs(v)) * cjm + (v - abs(v)) * cj) * dt / (2.0 * dx);
}

void NDNumerics::upwind_index(VecND &un, SizeType_I &subvector_index, IndexType_I &index, NumType_I &dx,
                              NumType_I &dt, VecNum_I &v) {
    VecNum un1(un.get_size(subvector_index));
    un1[0] = un.at(IND(0)); ///Boundary Conditions
    un1[un1.size() - 1] = un.at(IND(un1.size() - 1));
    for (SizeType j = 1; j < un.get_size(subvector_index) - 1; j++) {
        NumType vm = (v[j - 1] + v[j]) / 2.;
        NumType vp = (v[j] + v[j + 1]) / 2.;
        un1[j] = un.at(IND(j)) - (donor_cell(un.at(IND(j)), un.at(IND(j + 1)), vp, dx, dt)
                                  - donor_cell(un.at(IND(j - 1)), un.at(IND(j)), vm, dx, dt));
    }
    un.set_subvector(un1, subvector_index, index);
}

void NDNumerics::upwind(VecND &un, NumType_I &dx, NumType_I &dt, VecND_I &v, SizeType_I &dimension_index) {
    auto func = [&dx, &dt, &v](VecND &u, SizeType_I &subvector_index, IndexType_I &index) {
        upwind_index(u, subvector_index, index, dx, dt, v.subvector(subvector_index, index));
    };
    un.apply_subvector(func, dimension_index);
}

void NDNumerics::mpdata_index(VecND &un, SizeType_I &subvector_index, IndexType_I &index, NumType_I &dx,
                              NumType_I &dt, VecNum_I &v) {
    const NumType epsilon = 1e-15;

    upwind_index(un, subvector_index, index, dx, dt, v);

    VecNum vd(v.size()); ///v[i] stand for v[i+1/2]
    vd[0] = 0;
    vd.back() = 0;
    for (SizeType j = 1; j < vd.size() - 1; j++) {
        if (un.at(IND(j)) <= epsilon) { ///Works only for u >= 0. epsilon instead of 0, because problems otherwise.
            vd[j] = 0;
        } else {
            NumType vp = (v[j] + v[j + 1]) / 2.;
            vd[j] = (abs(vp) * dx - dt * vp * vp) / (un.at(IND(j)) + un.at(IND(j + 1)) + epsilon)
                    * (un.at(IND(j + 1)) - un.at(IND(j))) / dx;
        }
    }

    VecNum un1(un.get_size(subvector_index));
    un1[0] = un.at(IND(0)); ///Boundary Conditions
    un1[un1.size() - 1] = un.at(IND(un1.size() - 1));
    for (SizeType j = 1; j < un.get_size(subvector_index) - 1; j++) {
        un1[j] = un.at(IND(j)) - (donor_cell(un.at(IND(j)), un.at(IND(j + 1)), vd[j], dx, dt)
                                  - donor_cell(un.at(IND(j - 1)), un.at(IND(j)), vd[j - 1], dx, dt));
    }
    un.set_subvector(un1, subvector_index, index);
}

void NDNumerics::mpdata(VecND &un, NumType_I &dx, NumType_I &dt, VecND_I &v, SizeType_I &dimension_index) {
    auto func = [&dx, &dt, &v](VecND &u, SizeType_I &subvector_index, IndexType_I &index) {
        mpdata_index(u, subvector_index, index, dx, dt, v.subvector(subvector_index, index));
    };
    un.apply_subvector(func, dimension_index);
}

VecNum NDNumerics::interpolate_linear(VecNum_I &un, VecNum_I &xp) {
    VecNum un1(un.size());
    for (SizeType j = 0; j < un.size(); j++) {
        if (xp[j] <= 0) { ///Constant boundary start
            un1[j] = un[0];
        } else if ((SizeType) xp[j] >= un.size() - 1) {  ///Constant boundary end
            un1[j] = un[un.size() - 1];
        } else {
            SizeType i = xp[j];
            NumType x = xp[j] - i;
            un1[j] = un[i] * (1 - x) + un[i + 1] * x;
        }
    }
    return un1;
}

VecNum NDNumerics::interpolate_cubic(VecNum_I &un, VecNum_I &xp) {
    VecNum un1(un.size());
    for (SizeType j = 0; j < un.size(); j++) {
        if (xp[j] <= 0) {
            un1[j] = un[0];
        } else if ((SizeType) xp[j] >= un.size() - 1) {
            un1[j] = un[un.size() - 1];
        } else {
            SizeType i = xp[j];
            NumType m0, m1; ///Derivations at begin / end.
            if (i == 0) {
                m0 = un[i + 1] - un[i];
            } else {
                m0 = (un[i + 1] - un[i - 1]) / 2.0;
            }
            if (i == un.size() - 2) {
                m1 = un[i + 1] - un[i];
            } else {
                m1 = (un[i + 2] - un[i]) / 2.0;
            }
            NumType x = xp[j] - i;
            un1[j] = (2 * x * x * x - 3 * x * x + 1) * un[i] + (x * x * x - 2 * x * x + x) * m0 +
                     (-2 * x * x * x + 3 * x * x) * un[i + 1] + (x * x * x - x * x) * m1;
        }
    }
    return un1;
}

void NDNumerics::semi_lagrangian_index(VecND &un, SizeType_I &subvector_index, IndexType_I &index, NumType_I &dx,
                                       NumType_I &dt, VecNum_I &v, const int &boundary_condition,
                                       const int &iterations) {
    ///Assuming velocity v is constant in time -> vm = v, vi = vm = v;
    VecNum vi = v;
    VecNum xp(un.get_size(subvector_index));
    for (int i = 0; i < iterations; i++) {
        for (SizeType j = 0; j < un.get_size(subvector_index); j++) {
            xp[j] = j - dt / (2. * dx) * vi[j];
        }
        vi = interpolate_linear(v, xp);
    }
    for (SizeType j = 0; j < un.get_size(subvector_index); j++) {
        xp[j] = j - dt / dx * vi[j];
    }
    VecNum un1 = interpolate_cubic(un.subvector(subvector_index, index), xp);
    if (boundary_condition == 0) {
        un1[0] = un.at(IND(0));
        un1[un1.size() - 1] = un.at(IND(un1.size() - 1));
    } else if (boundary_condition == 1) {
        un1[un1.size() - 1] = un.at(IND(un1.size() - 1));
    }
    un.set_subvector(un1, subvector_index, index);
}

void NDNumerics::semi_lagrangian(VecND &un, NumType_I &dx, NumType_I &dt, VecND_I &v, SizeType_I &dimension_index,
                                 const int &boundary_condition, const int &iterations) {
    auto func = [&dt, &dx, &v, &boundary_condition, &iterations](VecND &u, SizeType_I &subvector_index,
                                                                 IndexType_I &index) {
        semi_lagrangian_index(u, subvector_index, index, dx, dt, v.subvector(subvector_index, index),
                              boundary_condition, iterations);
    };
    un.apply_subvector(func, dimension_index);
}

void NDNumerics::semi_lagrangian_2_index(VecND &un, SizeType_I &subvector_index, IndexType_I &index, NumType_I &dx,
                                         NumType_I &dt, VecNum_I &v, const int &boundary_condition,
                                         const int &iterations) {
    ///Assuming velocity v is constant in time -> vm = v, vi = vm;
    VecNum vi = v;
    VecNum xp(un.get_size(subvector_index));
    for (int i = 0; i < iterations; i++) {
        for (SizeType j = 0; j < un.get_size(subvector_index); j++) {
            xp[j] = j - dt / (2. * dx) * vi[j];
        }
        vi = interpolate_linear(v, xp);
//        vi = interpolate_cubic(v, xp);
    }
    for (SizeType j = 0; j < un.get_size(subvector_index); j++) {
        xp[j] = j - dt / dx * vi[j];
    }
    VecNum un1 = interpolate_cubic(un.subvector(subvector_index, index), xp);
    VecNum dv(un1.size()); ///dv/dx
    dv[0] = (v[1] - v[0]) / dx;
    dv[un1.size() - 1] = (v[un1.size() - 1] - v[un1.size() - 2]) / dx;
    for (SizeType j = 1; j < un1.size() - 1; j++) {
        dv[j] = (v[j + 1] - v[j - 1]) / (2.0 * dx);
    }
    VecNum dv1 = interpolate_cubic(dv, xp); ///dv at t=n-1!
    ///un+1 = un * (1 + dt/2 gn[j2]) / (1 - dt/2 gn+1[j]); gn[j] = gn+1[j] = -dv, j2 = xp
    for (SizeType j = 0; j < un.get_size(subvector_index); j++) {
        un1[j] = (1.0 - 0.5 * dt * dv1[j]) / (1.0 + 0.5 * dt * dv[j]) * un1[j];
    }
    if (boundary_condition == 0) {
        un1[0] = un.at(IND(0));
        un1[un1.size() - 1] = un.at(IND(un1.size() - 1));
    } else if (boundary_condition == 1) {
        un1[un1.size() - 1] = un.at(IND(un1.size() - 1));
    }
    un.set_subvector(un1, subvector_index, index);
}

void NDNumerics::semi_lagrangian_2(VecND &un, NumType_I &dx, NumType_I &dt, VecND_I &v, SizeType_I &dimension_index,
                                   const int &boundary_condition, const int &iterations) {
    auto func = [&dt, &dx, &v, &boundary_condition, &iterations](VecND &u, SizeType_I &subvector_index,
                                                                 IndexType_I &index) {
        semi_lagrangian_2_index(u, subvector_index, index, dx, dt, v.subvector(subvector_index, index),
                                boundary_condition, iterations);
    };
    un.apply_subvector(func, dimension_index);
}