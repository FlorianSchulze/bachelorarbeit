//
// Created by Flo on 01.05.2020.
//

#ifndef PROGRAM_PLOT_HPP
#define PROGRAM_PLOT_HPP

#include "own_error.hpp"
#include "NDVector.hpp"
#include <vector>
#include <functional>
#include "hdf5.h"

///Defines the type of number used.
typedef double NumType;
///Numbers used for input.
typedef const NumType NumType_I;
///Defines a vector of numbers.
typedef std::vector<NumType> VecNum;
///A vector of numbers for input to functions.
typedef const std::vector<NumType> VecNum_I;
///A NDVector of numbers.
typedef NDVector<NumType> VecND;
///A NDVector of numbers for input.
typedef const NDVector<NumType> VecND_I;
///Type for Sizes.
typedef std::size_t SizeType;
///Type for Sizes for input.
typedef const std::size_t SizeType_I;
///Type for multidimensional indices.
typedef std::vector<SizeType> IndexType;
///Type for multidimensional indices for input.
typedef const std::vector<SizeType> IndexType_I;

///Some useful functions.
namespace plot {
    /**
     * Saves two vectors as a .csv file.
     * It then can be plotted with plot.py, for details see README.md.
     * @param x, y the vectors to be saved
     * @param file the .csv file to save the vectors
     * @param x_label, y_label the labels of the axis
     * @param label label of the graph
     * @return 0 if everything worked, -1 if the file was not saved (because it was emtpy)
     */
    int save_as_csv(VecNum_I &x, VecNum_I &y, const std::string &file, const std::string &x_label = "None",
                     const std::string &y_label = "None", const std::string &label = "None");

    /**
     * Saves a NDvector as a .csv file.
     * It then can be load in python with plot.py, for details see README.md.
     * @param x the vector to be saved
     * @param file the .csv file to save the vectors
     * @return 0 if everything worked
     */
    int save_as_csv(VecND_I &x, const std::string &file);

    /**
     * Saves in NDvector as a .hdf5 file.
     * @param[in] x the vector to be saved
     * @param[out] dataset_id the dataset created with the vector saved in it
     * @param[in] file_id the file id
     * @param[in] name the name of the dataset
     */
    void save_in_hdf5(VecND_I &x, hid_t &dataset_id, const hid_t &file_id, const std::string &name);

    /**
     * Prints a vector in the console.
     * @param x vector to be printed
     * @param max_print_size the maximal count of elements to be printed, 0 for inf
     */
    void vec_print(VecNum_I &x, VecNum::size_type max_print_size = 15);

    /**
     * Creates a vector with values from start to stop with distance step.
     * @param start start value
     * @param stop stop value
     * @param step distance between values
     * @return created vector
     * @see linspace
     * @see [numpy.arrange]
     */
    VecNum arange(NumType_I &start, NumType_I &stop, NumType_I &step);

    /**
     * Creates a vector with num values between start and stop.
     * @param start start value
     * @param stop stop value
     * @param num number of values
     * @return created vector
     * @see arange
     * @see [numpy.linspace]
     */
    VecNum linspace(NumType_I &start, NumType_I &stop, const int &num);

    /**
     * Creates an vector with the function func applied to x.
     * @param[in] x input vector for the function
     * @param func function that should be applied
     * @return vector f(x)
     */
    VecNum vec_apply(VecNum_I &x, NumType (*func)(NumType));

    VecNum vec_apply(VecNum_I &x, const std::function<NumType(NumType)> &func);

    /**
     * Creates an vector with the operator func applied to x and y.
     * @param[in] x, y input vectors for the operator
     * @param func operator that should be applied
     * @return vector f(x, y)
     */
    VecNum vec_operator(VecNum_I &x, VecNum_I &y, NumType (*func)(NumType, NumType));

    VecNum vec_operator(VecNum_I &x, VecNum_I &y, const std::function<NumType(NumType, NumType)> &func);

    /**
     * Gives the sum of the elements in the vector.
     * @param vec input vector
     * @return summ of vector elements
     */
    NumType vec_sum(VecNum_I &vec);

    /**
     * Gives the maximum element in the vector.
     * @param vec input vector
     * @return maximal value
     */
    NumType vec_max(VecNum_I &vec);

    /**
     * Sorts the vector vec.
     * @param[in, out] vec vector to be sorted.
     */
    void vec_sort(VecNum &vec);

    /**
     * An approximation of the dirac delta with the normal distribution.
     * @param x x-value
     * @param epsilon small parameter for approximation
     * @return exp(-x^2 / (2 epsilon)) / sqrt(2 pi epsilon);
     * @note Returns 0 if x^2 &ge 10*epsilon
     */
    NumType dirac_delta(NumType_I &x, NumType_I &epsilon = 1e-2);

    /**
     * Calculates the absolute difference between two vectors.
     * @param x, y two vectors
     * @return vector abs(x-y)
     */
    VecNum difference(VecNum_I &x, VecNum_I &y);

    /**
     * Calculates the relative difference between two vectors.
     * @param x, y two vectors
     * @return vector abs(x-y) / abs(x)
     */
    VecNum relative_difference(VecNum_I &x, VecNum_I &y);

    /**
     * Calculates the maximal difference between two vectors and return its relative value.
     * @param x, y two vectors
     * @return max(abs(x-y)) / abs(max(x))
     */
    NumType max_difference_relative(VecNum_I &x, VecNum_I &y);

    /**
     * Fills the vector vec with uniform distributed numbers between min and max.
     * @param[out] vec vector to be filled with random numbers
     * @param[in] min, max min/max value
     */
    void random_uniform(VecNum &vec, NumType_I &min, NumType_I &max);

    /**
     * Fills the vector vec with normal distributed numbers with mean and stddev.
     * @param[out] vec vector to be filled with random numbers
     * @param[in] mean mean of the distribution
     * @param[in] stddev standard derivation sigma
     */
    void random_normal(VecNum &vec, NumType_I &mean, NumType_I &stddev);

    /**
     * Prints out the histogram of an vector of random numbers.
     * @param random_numbers vector with random numbers
     * @param step_size step size of the histogram
     * @param min, max minimal and maximal value. If min = max = 0 they are set automatic.
     * @param max_stars maximal number of starts
     */
    void print_distribution(VecNum_I &random_numbers, NumType_I &step_size = 1, NumType min = 0, NumType max = 0,
                            int max_stars = 20);
}

#endif //PROGRAM_PLOT_HPP
