//
// Created by Flo on 01.05.2020.
//

#include "plot.hpp"
#include <iostream>
#include <fstream>
#include <cmath>
#include <algorithm>
#include <random>
#include <chrono>

using namespace std;

int plot::save_as_csv(VecNum_I &x, VecNum_I &y, const string &file, const string &x_label, const string &y_label,
                       const string &label) {
    if (x.empty() or y.empty()) {
        return -1;
    }
    ofstream out(file.c_str());
    const string delimiter = "; ";
    out << label << endl << x_label << delimiter << y_label << endl;
    for (VecNum::size_type i = 0; i != x.size() && i != y.size(); i++) {
        out << x[i] << delimiter << y[i] << endl;
    }
    out.close();
    return 0;
}

int plot::save_as_csv(VecND_I &x, const string &file) {
    ofstream out(file.c_str());
    for (SizeType i = 0; i < x.get_dimension(); i++) {
        out << x.get_size(i) << ";";
    }
    out << endl;
    //TD: set precision
    for (auto const &value: x.get_values()) {
        out << value << endl;
    }
    out.close();
    return 0;
}

void plot::save_in_hdf5(VecND_I &x, hid_t &dataset_id, const hid_t &file_id, const string &name) {
    SizeType rank = x.get_dimension();
    hsize_t *dims;
    dims = new hsize_t[rank];
    for (SizeType i = 0; i < rank; i++) dims[i] = x.get_size(i);
    hid_t dataspace_id = H5Screate_simple(rank, dims, nullptr);
    delete[] dims;
    dataset_id = H5Dcreate2(file_id, name.c_str(), H5T_IEEE_F64BE, dataspace_id, H5P_DEFAULT, H5P_DEFAULT, H5P_DEFAULT);
    H5Sclose(dataspace_id);
    H5Dwrite(dataset_id, H5T_NATIVE_DOUBLE, H5S_ALL, H5S_ALL, H5P_DEFAULT, x.get_values().data());
}

void plot::vec_print(VecNum_I &x, VecNum::size_type max_print_size) {
    if (x.size() <= max_print_size or max_print_size == 0) {
        for (auto const &value: x) cout << value << "; ";
    } else {
        for (VecNum::size_type i = 0; i < 3; i++) cout << x[i] << "; ";
        cout << "[" << x.size() - 6 << "]; ";
        for (VecNum::size_type i = x.size() - 3; i < x.size(); i++) cout << x[i] << "; ";
    }
    cout << endl;
}

VecNum plot::arange(NumType_I &start, NumType_I &stop, NumType_I &step) {
    VecNum vec;
    for (NumType value = start; value <= stop; value += step) {
        vec.push_back(value);
    }
    return vec;
}

VecNum plot::linspace(NumType_I &start, NumType_I &stop, const int &num) {
    VecNum vec(num, 0);
    for (VecNum::size_type i = 0; i != vec.size(); i++) {
        vec[i] = start + (stop - start) * i / (num - 1);
    }
    return vec;
}

VecNum plot::vec_apply(VecNum_I &x, NumType (*func)(NumType)) {
    VecNum ret(x.size());
    transform(x.begin(), x.end(), ret.begin(), func);
    return ret;
}

VecNum plot::vec_apply(VecNum_I &x, const function<NumType(NumType)> &func) {
    VecNum ret(x.size());
    transform(x.begin(), x.end(), ret.begin(), func);
    return ret;
}

VecNum plot::vec_operator(VecNum_I &x, VecNum_I &y, NumType (*func)(NumType, NumType)) {
    VecNum ret(x.size());
    transform(x.begin(), x.end(), y.begin(), ret.begin(), func);
    return ret;
}

VecNum plot::vec_operator(VecNum_I &x, VecNum_I &y, const function<NumType(NumType, NumType)> &func) {
    VecNum ret(x.size());
    transform(x.begin(), x.end(), y.begin(), ret.begin(), func);
    return ret;
}

NumType plot::vec_sum(VecNum_I &vec) {
    NumType sum_of_elems = 0;
    for (auto &n: vec)
        sum_of_elems += n;
    return sum_of_elems;
}

NumType plot::vec_max(VecNum_I &vec) {
    return *max_element(vec.begin(), vec.end());
}

void plot::vec_sort(VecNum &vec) {
    sort(vec.begin(), vec.end());
}

NumType plot::dirac_delta(NumType_I &x, NumType_I &epsilon) {
//    if (x * x > 10 * epsilon) return 0;
    return exp(-x * x / (2 * epsilon)) / sqrt(2 * M_PI * epsilon);
}

VecNum plot::difference(VecNum_I &x, VecNum_I &y) {
    function<NumType(NumType_I &, NumType_I &)> func = [](NumType_I &a, NumType_I &b) { return abs(a - b); };
    return plot::vec_operator(x, y, func);
}

VecNum plot::relative_difference(VecNum_I &x, VecNum_I &y) {
    function<NumType(NumType_I &, NumType_I &)> func = [](NumType_I &a, NumType_I &b) { return abs(a - b) / abs(a); };
    return plot::vec_operator(x, y, func);
}

NumType plot::max_difference_relative(VecNum_I &x, VecNum_I &y) {
    return vec_max(difference(x, y)) / abs(vec_max(x));
}

void plot::random_uniform(VecNum &vec, NumType_I &min, NumType_I &max) {
    unsigned seed = chrono::system_clock::now().time_since_epoch().count();
    default_random_engine generator(seed);
    uniform_real_distribution<NumType> distribution(min, max);
    for (NumType &x : vec) {
        x = distribution(generator);
    }
}

void plot::random_normal(VecNum &vec, NumType_I &mean, NumType_I &stddev) {
    unsigned seed = chrono::system_clock::now().time_since_epoch().count();
    default_random_engine generator(seed);
    normal_distribution<NumType> distribution(mean, stddev);
    for (NumType &x : vec) {
        x = distribution(generator);
    }
}

void
plot::print_distribution(VecNum_I &random_numbers, NumType_I &step_size, NumType min, NumType max, int max_starts) {
    if (min == 0 and max == 0) {
        min = (int) *min_element(random_numbers.begin(), random_numbers.end());
        max = *max_element(random_numbers.begin(), random_numbers.end()) + step_size;
    }
    VecNum axis = arange(min, max, step_size);
    vector<int> prop(axis.size(), 0);

    for (NumType_I &x : random_numbers) {
        for (SizeType i = 0; i < axis.size() - 1; i++) {
            if (x >= axis[i] and x < axis[i + 1]) prop[i] += 1;
        }
    }

    int max_prop = *max_element(prop.begin(), prop.end());
    int number_starts = max_starts * random_numbers.size() / max_prop;

    for (SizeType i = 0; i < axis.size() - 1; i++) {
        cout << axis[i] << "-" << axis[i + 1] << ": ";
        cout << string((prop[i] * number_starts) / random_numbers.size(), '*') << endl;
    }
}
